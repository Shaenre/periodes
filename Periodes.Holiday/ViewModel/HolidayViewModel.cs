﻿using ID3iHoliday.Models;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Holiday.ViewModel
{
    public class HolidayViewModel : BindableBase
    {
        [Dependency]
        public IUnityContainer Container { get; set; }
        [Dependency]
        public IHolidaySystem HolidaySystem { get; set; }

        public HolidayViewModel() { }

        private DateTime selectedDate = DateTime.Now;
        public DateTime SelectedDate
        {
            get => selectedDate;
            set
            {
                if (selectedDate != value)
                {
                    selectedDate = value;
                    RaisePropertyChanged(nameof(SelectedDate));
                    RaisePropertyChanged(nameof(Holidays));
                }
            }
        }

        public ObservableCollection<Country> Countries => new ObservableCollection<Country>(HolidaySystem.CountriesAvailable);
        private Country selectedCountry;
        public Country SelectedCountry
        {
            get => selectedCountry;
            set
            {
                if (selectedCountry != value)
                {
                    selectedCountry = value;
                    RaisePropertyChanged(nameof(SelectedCountry));
                    SelectedState = null;
                    holidays = null;
                    RaisePropertyChanged(nameof(Holidays));
                }
            }
        }

        private State selectedState;
        public State SelectedState
        {
            get => selectedState;
            set
            {
                if (selectedState != value)
                {
                    selectedState = value;
                    RaisePropertyChanged(nameof(SelectedState));
                    SelectedRegion = null;
                    holidays = null;
                    RaisePropertyChanged(nameof(Holidays));
                }
            }
        }

        private Region selectedRegion;
        public Region SelectedRegion
        {
            get => selectedRegion;
            set
            {
                if (selectedRegion != value)
                {
                    selectedRegion = value;
                    RaisePropertyChanged(nameof(SelectedRegion));
                    holidays = null;
                    RaisePropertyChanged(nameof(Holidays));
                }
            }
        }

        private ObservableCollection<SpecificDay> holidays = null;
        public ObservableCollection<SpecificDay> Holidays
        {
            get
            {
                if (holidays == null && SelectedCountry != null)
                    holidays = new ObservableCollection<SpecificDay>(HolidaySystem.All(SelectedDate.Year, SelectedCountry.Code, SelectedState?.Code, SelectedRegion?.Code));
                return holidays;
            }
        }

        private SpecificDay selectedHoliday;
        public SpecificDay SelectedHoliday
        {
            get
            {
                return selectedHoliday;
            }
            set
            {
                if (selectedHoliday != value)
                {
                    selectedHoliday = value;
                    RaisePropertyChanged(nameof(SelectedHoliday));
                }
            }
        }
    }
}
