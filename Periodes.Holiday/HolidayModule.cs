﻿using Periodes.Global;
using Periodes.Holiday.View;
using ID3iShared.Module;
using ID3iShared.Regions;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Periodes.Holiday
{
    public class HolidayModule : Module
    {
        public HolidayModule() { }
        public override void Initialize()
        {
            Container.RegisterTypeForNavigation<HolidayView>();

            RegionManager.RegisterViewWithRegion(RegionNames.MenuRegion, () => new ViewWithName(new RadRadioButton()
            {
                Content = "Jours particuliers",
                Command = Container.Resolve<IApplicationCommands>().NavigateCommand,
                CommandParameter = nameof(HolidayView)
            }, $"RadRadioButton_{nameof(HolidayView)}_{new Random().Next()}"));
        }
    }
}
