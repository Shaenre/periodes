﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Periodes.Gestion.Standard.Data;
using Periodes.Gestion.Standard.View;
using Periodes.Global;
using Periodes.Preferences;
using ID3iShared.Module;
using ID3iShared.Regions;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Periodes.Gestion.Standard
{
    public class StandardModule : Module
    {
        public static string Name => "STD";
        public override void Initialize()
        {
            Configure();

            Container.RegisterInstance(Container.Resolve<StandardController>(), new ContainerControlledLifetimeManager());

            Container.RegisterInstance<IPreferences>($"{Name}_{nameof(Preferences)}", Container.Resolve<Preferences>().Initialize(x => x.Charger()), new ContainerControlledLifetimeManager());

            Container.RegisterTypeForNavigation<LayoutTPSTDView>();
            Container.RegisterTypeForNavigation<PreferencesSTDView>();

            RegionManager.RegisterViewWithRegion(RegionNames.GestionRegion, () => new ViewWithName(new RadButton()
            {
                Content = "Standard",
                Command = Container.Resolve<StandardController>().ShowInterfaceCommand
            }, $"RadButton_ShowStandardInterface_{new Random().Next()}"));
        }

        private void Configure()
        {
            var factory = Container.Resolve<IFactoryVM>();

            CheckTypePeriode(factory, TypePeriodeReference.YEAR, "Année");
            CheckTypePeriode(factory, TypePeriodeReference.MONTH, "Mois");
            CheckTypePeriode(factory, TypePeriodeReference.WEEK, "Semaine");
            CheckTypePeriode(factory, TypePeriodeReference.DAY, "Jour");
            CheckTypePeriode(factory, TypePeriodeReference.HALFDAY, "Semi-jour");
            factory.SaveTypesPeriodes();
        }

        private void CheckTypePeriode(IFactoryVM factory, TypePeriodeReference type, string libelle)
        {
            if (factory.TypesPeriodes.FirstOrDefault(x => x.Code == type.ToString()) == null)
            {
                var typePeriode = new VMTypePeriode()
                {
                    Designation = libelle,
                    Index = (int)type,
                    Code = type.ToString()
                };
                factory.TypesPeriodes.Add(typePeriode);
            }
        }
    }
}
