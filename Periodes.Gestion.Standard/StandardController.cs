﻿using Periodes.Gestion.Standard.View;
using Periodes.Global;
using Periodes.Policy;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.Standard
{
    public class StandardController
    {
        [Dependency]
        public IUnityContainer Container { get; set; }
        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public DelegateCommand ShowInterfaceCommand { get; set; }

        private DefaultPolicy DefaultPolicy { get; } = new DefaultPolicy();
        public StandardController()
        {
            ShowInterfaceCommand = new DelegateCommand(ShowInterfaceExecute);
        }
        private void ShowInterfaceExecute()
        {
            RegionManager.RequestNavigate(RegionNames.TypePeriodeRegion, nameof(LayoutTPSTDView));
            RegionManager.RequestNavigate(RegionNames.SettingsRegion, nameof(PreferencesSTDView));
            Container.Resolve<GestionController>().Try(x =>
            {
                x.ModuleActif = StandardModule.Name;
                x.Policy = DefaultPolicy;
            });
        }
    }
}
