﻿using Periodes.Preferences;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.Standard.ViewModel
{
    public class PreferencesSTDViewModel
    {
        public Preferences Preferences { get; set; }
        public PreferencesSTDViewModel(IUnityContainer container)
        {
            Preferences = container.Resolve<IPreferences>($"{StandardModule.Name}_{nameof(Preferences)}") as Preferences;
        }
    }
}
