﻿using Periodes.Configuration;
using Periodes.Preferences;
using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.Standard
{
    public class Preferences : BindableBase, IPreferences
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [JsonIgnore()]
        public string FilePath
        {
            get
            {
                string directoryPath = Path.Combine(ConfigFile.Conf.ApplicationDirectory, "Modules", nameof(StandardModule));
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                return Path.Combine(directoryPath, "Preferences.json");
            }
        }

        public void Enregistrer()
        {
            JsonConvert.SerializeObject(this).SaveAs(FilePath);
        }

        public void Charger()
        {
            if (File.Exists(FilePath))
                using (TextReader reader = new StreamReader(FilePath))
                {
                    var pref = JsonConvert.DeserializeObject<Preferences>(reader.ReadToEnd());
                    StartDate = pref.StartDate;
                    EndDate = pref.EndDate;
                }
            else
            {
                StartDate = new DateTime(DateTime.Today.Year, 01, 01);
                EndDate = new DateTime(DateTime.Today.Year + 1, 01, 01);
            }
        }
    }
}
