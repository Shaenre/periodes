﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Periodes.Controls
{
    public class ThemeAndPaletteSelector : Control
    {
        public IEnumerable Themes
        {
            get { return (IEnumerable)GetValue(ThemesProperty); }
            set { SetValue(ThemesProperty, value); }
        }
        public static readonly DependencyProperty ThemesProperty =
            DependencyProperty.Register("Themes", typeof(IEnumerable), typeof(ThemeAndPaletteSelector), new PropertyMetadata(null));

        public object SelectedTheme
        {
            get { return (object)GetValue(SelectedThemeProperty); }
            set { SetValue(SelectedThemeProperty, value); }
        }
        public static readonly DependencyProperty SelectedThemeProperty =
            DependencyProperty.Register("SelectedTheme", typeof(object), typeof(ThemeAndPaletteSelector), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string ThemeDisplayPath
        {
            get { return (string)GetValue(ThemeDisplayPathProperty); }
            set { SetValue(ThemeDisplayPathProperty, value); }
        }
        public static readonly DependencyProperty ThemeDisplayPathProperty =
            DependencyProperty.Register("ThemeDisplayPath", typeof(string), typeof(ThemeAndPaletteSelector), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable Palettes
        {
            get { return (IEnumerable)GetValue(PalettesProperty); }
            set { SetValue(PalettesProperty, value); }
        }
        public static readonly DependencyProperty PalettesProperty =
            DependencyProperty.Register("Palettes", typeof(IEnumerable), typeof(ThemeAndPaletteSelector), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object SelectedPalette
        {
            get { return (object)GetValue(SelectedPaletteProperty); }
            set { SetValue(SelectedPaletteProperty, value); }
        }
        public static readonly DependencyProperty SelectedPaletteProperty =
            DependencyProperty.Register("SelectedPalette", typeof(object), typeof(ThemeAndPaletteSelector), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string PaletteDisplayPath
        {
            get { return (string)GetValue(PaletteDisplayPathProperty); }
            set { SetValue(PaletteDisplayPathProperty, value); }
        }
        public static readonly DependencyProperty PaletteDisplayPathProperty =
            DependencyProperty.Register("PaletteDisplayPath", typeof(string), typeof(ThemeAndPaletteSelector), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
    }
}
