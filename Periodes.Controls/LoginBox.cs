﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Periodes.Controls
{
    [TemplatePart(Name = "PART_LOGIN", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_PASSWORD", Type = typeof(PasswordBox))]
    [TemplatePart(Name = "PART_BUTTON_AUTH", Type = typeof(Button))]
    public class LoginBox : Control
    {
        private TextBox LoginTextBox { get; set; }
        private PasswordBox PasswordBox { get; set; }
        private Button ButtonAuth { get; set; }
        public LoginBox()
        {
            DefaultStyleKey = typeof(LoginBox);
        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            LoginTextBox = GetTemplateChild("PART_LOGIN") as TextBox;
            PasswordBox = GetTemplateChild("PART_PASSWORD") as PasswordBox;
            ButtonAuth = GetTemplateChild("PART_BUTTON_AUTH") as Button;
        }

        public string Login
        {
            get { return (string)GetValue(LoginProperty); }
            set { SetValue(LoginProperty, value); }
        }
        public static readonly DependencyProperty LoginProperty =
            DependencyProperty.Register("Login", typeof(string), typeof(LoginBox), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(LoginBox), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ICommand AuthCommand
        {
            get { return (ICommand)GetValue(AuthCommandProperty); }
            set { SetValue(AuthCommandProperty, value); }
        }
        public static readonly DependencyProperty AuthCommandProperty =
            DependencyProperty.Register("AuthCommand", typeof(ICommand), typeof(LoginBox), new PropertyMetadata(null));
    }
}
