﻿using MahApps.Metro.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Periodes.Controls
{
    [TemplatePart(Name = "PART_DATE", Type = typeof(DateTimePicker))]
    [TemplatePart(Name = "PART_TYPE_PERIODE", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_TYPE_PERIODE_SOUHAITE", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_TYPE_PERIODE_OFFSET", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_OFFSET", Type = typeof(ComboBox))]
    public class PeriodeApiView : Control
    {
        private DateTimePicker DatePicker { get; set; }
        private ComboBox ComboTypePeriode { get; set; }
        private ComboBox ComboTypePeriodeSouhaite { get; set; }
        private ComboBox ComboTypePeriodeOffset { get; set; }
        private TextBox TextBoxOffset { get; set; }

        public DateTime Date
        {
            get { return (DateTime)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }
        public static readonly DependencyProperty DateProperty = DependencyProperty.Register(nameof(Date), typeof(DateTime), typeof(PeriodeApiView), new FrameworkPropertyMetadata(DateTime.Today, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable TypesPeriode
        {
            get { return (IEnumerable)GetValue(TypesPeriodeProperty); }
            set { SetValue(TypesPeriodeProperty, value); }
        }
        public static readonly DependencyProperty TypesPeriodeProperty = DependencyProperty.Register(nameof(TypesPeriode), typeof(IEnumerable), typeof(PeriodeApiView), new PropertyMetadata(null));

        public string TypePeriode
        {
            get { return (string)GetValue(TypePeriodeProperty); }
            set { SetValue(TypePeriodeProperty, value); }
        }
        public static readonly DependencyProperty TypePeriodeProperty = DependencyProperty.Register(nameof(TypePeriode), typeof(string), typeof(PeriodeApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string TypePeriodeSouhaite
        {
            get { return (string)GetValue(TypePeriodeSouhaiteProperty); }
            set { SetValue(TypePeriodeSouhaiteProperty, value); }
        }
        public static readonly DependencyProperty TypePeriodeSouhaiteProperty = DependencyProperty.Register(nameof(TypePeriodeSouhaite), typeof(string), typeof(PeriodeApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string TypePeriodeOffset
        {
            get { return (string)GetValue(TypePeriodeOffsetProperty); }
            set { SetValue(TypePeriodeOffsetProperty, value); }
        }
        public static readonly DependencyProperty TypePeriodeOffsetProperty = DependencyProperty.Register(nameof(TypePeriodeOffset), typeof(string), typeof(PeriodeApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public int Offset
        {
            get { return (int)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }
        public static readonly DependencyProperty OffsetProperty = DependencyProperty.Register(nameof(Offset), typeof(int), typeof(PeriodeApiView), new FrameworkPropertyMetadata(0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ICommand TryItCommand
        {
            get { return (ICommand)GetValue(TryItCommandProperty); }
            set { SetValue(TryItCommandProperty, value); }
        }
        public static readonly DependencyProperty TryItCommandProperty = DependencyProperty.Register(nameof(TryItCommand), typeof(ICommand), typeof(PeriodeApiView), new PropertyMetadata(null));

        public string Reponse
        {
            get { return (string)GetValue(ReponseProperty); }
            set { SetValue(ReponseProperty, value); }
        }
        public static readonly DependencyProperty ReponseProperty = DependencyProperty.Register(nameof(Reponse), typeof(string), typeof(PeriodeApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string RequestURI
        {
            get { return (string)GetValue(RequestURIProperty); }
            set { SetValue(RequestURIProperty, value); }
        }
        public static readonly DependencyProperty RequestURIProperty = DependencyProperty.Register(nameof(RequestURI), typeof(string), typeof(PeriodeApiView), new PropertyMetadata(null));
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(nameof(Description), typeof(string), typeof(PeriodeApiView), new PropertyMetadata(null));

        public string HttpMethod
        {
            get { return (string)GetValue(HttpMethodProperty); }
            set { SetValue(HttpMethodProperty, value); }
        }
        public static readonly DependencyProperty HttpMethodProperty = DependencyProperty.Register(nameof(HttpMethod), typeof(string), typeof(PeriodeApiView), new PropertyMetadata(null));

        public PeriodeApiView()
        {
            DefaultStyleKey = typeof(PeriodeApiView);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            Reponse = null;
            base.OnGotFocus(e);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            DatePicker = GetTemplateChild("PART_DATE") as DateTimePicker;
            ComboTypePeriode = GetTemplateChild("PART_TYPE_PERIODE") as ComboBox;
            ComboTypePeriodeSouhaite = GetTemplateChild("PART_TYPE_PERIODE_SOUHAITE") as ComboBox;
            ComboTypePeriodeOffset = GetTemplateChild("PART_TYPE_PERIODE_OFFSET") as ComboBox;
            TextBoxOffset = GetTemplateChild("PART_OFFSET") as TextBox;
        }
    }
}
