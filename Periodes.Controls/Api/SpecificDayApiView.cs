﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Periodes.Controls
{
    [TemplatePart(Name = "PART_YEAR", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_COUNTRY_CODE", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_STATE_CODE", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_REGION_CODE", Type = typeof(ComboBox))]
    [TemplatePart(Name = "PART_RULE_TYPE", Type = typeof(ComboBox))]
    public class SpecificDayApiView : Control
    {
        private ComboBox ComboBoxYear { get; set; }

        private ComboBox ComboBoxCountryCode { get; set; }
        private ComboBox ComboBoxStateCode { get; set; }
        private ComboBox ComboBoxRegionCode { get; set; }
        private ComboBox ComboBoxRuleType { get; set; }
        
        public IEnumerable Years
        {
            get { return (IEnumerable)GetValue(YearsProperty); }
            set { SetValue(YearsProperty, value); }
        }
        public static readonly DependencyProperty YearsProperty = DependencyProperty.Register(nameof(Years), typeof(IEnumerable), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public int Year
        {
            get { return (int)GetValue(YearProperty); }
            set { SetValue(YearProperty, value); }
        }
        public static readonly DependencyProperty YearProperty = DependencyProperty.Register(nameof(Year), typeof(int), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(DateTime.Today.Year, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable Countries
        {
            get { return (IEnumerable)GetValue(CountriesProperty); }
            set { SetValue(CountriesProperty, value); }
        }
        public static readonly DependencyProperty CountriesProperty = DependencyProperty.Register(nameof(Countries), typeof(IEnumerable), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public object Country
        {
            get { return (object)GetValue(CountryProperty); }
            set { SetValue(CountryProperty, value); }
        }
        public static readonly DependencyProperty CountryProperty = DependencyProperty.Register(nameof(Country), typeof(object), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string CountryCode
        {
            get { return (string)GetValue(CountryCodeProperty); }
            set { SetValue(CountryCodeProperty, value); }
        }
        public static readonly DependencyProperty CountryCodeProperty = DependencyProperty.Register(nameof(CountryCode), typeof(string), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable States
        {
            get { return (IEnumerable)GetValue(StatesProperty); }
            set { SetValue(StatesProperty, value); }
        }
        public static readonly DependencyProperty StatesProperty = DependencyProperty.Register(nameof(States), typeof(IEnumerable), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public object State
        {
            get { return (object)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }
        public static readonly DependencyProperty StateProperty = DependencyProperty.Register(nameof(State), typeof(object), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string StateCode
        {
            get { return (string)GetValue(StateCodeProperty); }
            set { SetValue(StateCodeProperty, value); }
        }
        public static readonly DependencyProperty StateCodeProperty = DependencyProperty.Register(nameof(StateCode), typeof(string), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable Regions
        {
            get { return (IEnumerable)GetValue(RegionsProperty); }
            set { SetValue(RegionsProperty, value); }
        }
        public static readonly DependencyProperty RegionsProperty = DependencyProperty.Register(nameof(Regions), typeof(IEnumerable), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public object Region
        {
            get { return (object)GetValue(RegionProperty); }
            set { SetValue(RegionProperty, value); }
        }
        public static readonly DependencyProperty RegionProperty = DependencyProperty.Register(nameof(Region), typeof(object), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string RegionCode
        {
            get { return (string)GetValue(RegionCodeProperty); }
            set { SetValue(RegionCodeProperty, value); }
        }
        public static readonly DependencyProperty RegionCodeProperty = DependencyProperty.Register(nameof(RegionCode), typeof(string), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IEnumerable RuleTypes
        {
            get { return (IEnumerable)GetValue(RuleTypesProperty); }
            set { SetValue(RuleTypesProperty, value); }
        }
        public static readonly DependencyProperty RuleTypesProperty = DependencyProperty.Register(nameof(RuleTypes), typeof(IEnumerable), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string RuleType
        {
            get { return (string)GetValue(RuleTypeProperty); }
            set { SetValue(RuleTypeProperty, value); }
        }
        public static readonly DependencyProperty RuleTypeProperty = DependencyProperty.Register(nameof(RuleType), typeof(string), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ICommand TryItCommand
        {
            get { return (ICommand)GetValue(TryItCommandProperty); }
            set { SetValue(TryItCommandProperty, value); }
        }
        public static readonly DependencyProperty TryItCommandProperty = DependencyProperty.Register(nameof(TryItCommand), typeof(ICommand), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string Reponse
        {
            get { return (string)GetValue(ReponseProperty); }
            set { SetValue(ReponseProperty, value); }
        }
        public static readonly DependencyProperty ReponseProperty = DependencyProperty.Register(nameof(Reponse), typeof(string), typeof(SpecificDayApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string RequestURI
        {
            get { return (string)GetValue(RequestURIProperty); }
            set { SetValue(RequestURIProperty, value); }
        }
        public static readonly DependencyProperty RequestURIProperty = DependencyProperty.Register(nameof(RequestURI), typeof(string), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(nameof(Description), typeof(string), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public string HttpMethod
        {
            get { return (string)GetValue(HttpMethodProperty); }
            set { SetValue(HttpMethodProperty, value); }
        }
        public static readonly DependencyProperty HttpMethodProperty = DependencyProperty.Register(nameof(HttpMethod), typeof(string), typeof(SpecificDayApiView), new PropertyMetadata(null));

        public SpecificDayApiView()
        {
            DefaultStyleKey = typeof(SpecificDayApiView);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            Reponse = null;
            base.OnGotFocus(e);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            ComboBoxYear = GetTemplateChild("PART_YEAR") as ComboBox;
            ComboBoxCountryCode = GetTemplateChild("PART_COUNTRY_CODE") as ComboBox;
            ComboBoxStateCode = GetTemplateChild("PART_STATE_CODE") as ComboBox;
            ComboBoxRegionCode = GetTemplateChild("PART_REGION_CODE") as ComboBox;
            ComboBoxRuleType = GetTemplateChild("PART_RULE_TYPE") as ComboBox;
        }
    }
}
