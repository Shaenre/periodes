﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Periodes.Controls
{
    public class HelloApiView : Control
    {
        public ICommand TryItCommand
        {
            get { return (ICommand)GetValue(TryItCommandProperty); }
            set { SetValue(TryItCommandProperty, value); }
        }
        public static readonly DependencyProperty TryItCommandProperty = DependencyProperty.Register(nameof(TryItCommand), typeof(ICommand), typeof(HelloApiView), new PropertyMetadata(null));

        public string Reponse
        {
            get { return (string)GetValue(ReponseProperty); }
            set { SetValue(ReponseProperty, value); }
        }
        public static readonly DependencyProperty ReponseProperty = DependencyProperty.Register(nameof(Reponse), typeof(string), typeof(HelloApiView), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string RequestURI
        {
            get { return (string)GetValue(RequestURIProperty); }
            set { SetValue(RequestURIProperty, value); }
        }
        public static readonly DependencyProperty RequestURIProperty = DependencyProperty.Register(nameof(RequestURI), typeof(string), typeof(HelloApiView), new PropertyMetadata(null));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(nameof(Description), typeof(string), typeof(HelloApiView), new PropertyMetadata(null));

        public string HttpMethod
        {
            get { return (string)GetValue(HttpMethodProperty); }
            set { SetValue(HttpMethodProperty, value); }
        }
        public static readonly DependencyProperty HttpMethodProperty = DependencyProperty.Register(nameof(HttpMethod), typeof(string), typeof(HelloApiView), new PropertyMetadata(null));

        public HelloApiView()
        {
            DefaultStyleKey = typeof(HelloApiView);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            Reponse = null;
            base.OnGotFocus(e);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }
    }
}
