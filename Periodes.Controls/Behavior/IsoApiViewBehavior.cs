﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Controls.Behavior
{
    public class IsoApiViewBehavior
    {
        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(ISOApiView))]
        public static Visibility GetShowCountryCode(DependencyObject obj) => (Visibility)obj.GetValue(ShowCountryCodeProperty);
        public static void SetShowCountryCode(DependencyObject obj, Visibility value) => obj.SetValue(ShowCountryCodeProperty, value);
        public static readonly DependencyProperty ShowCountryCodeProperty = DependencyProperty.RegisterAttached("ShowCountryCode", typeof(Visibility), typeof(IsoApiViewBehavior), new PropertyMetadata(Visibility.Visible));

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(ISOApiView))]
        public static Visibility GetShowStateCode(DependencyObject obj) => (Visibility)obj.GetValue(ShowStateCodeProperty);
        public static void SetShowStateCode(DependencyObject obj, Visibility value) => obj.SetValue(ShowStateCodeProperty, value);
        public static readonly DependencyProperty ShowStateCodeProperty = DependencyProperty.RegisterAttached("ShowStateCode", typeof(Visibility), typeof(IsoApiViewBehavior), new PropertyMetadata(Visibility.Visible));

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(ISOApiView))]
        public static Visibility GetShowRegionCode(DependencyObject obj) => (Visibility)obj.GetValue(ShowRegionCodeProperty);
        public static void SetShowRegionCode(DependencyObject obj, Visibility value) => obj.SetValue(ShowRegionCodeProperty, value);
        public static readonly DependencyProperty ShowRegionCodeProperty = DependencyProperty.RegisterAttached("ShowRegionCode", typeof(Visibility), typeof(IsoApiViewBehavior), new PropertyMetadata(Visibility.Visible));

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(ISOApiView))]
        public static Visibility GetHideAll(DependencyObject obj) => (Visibility)obj.GetValue(HideAllProperty);
        public static void SetHideAll(DependencyObject obj, Visibility value) => obj.SetValue(HideAllProperty, value);
        public static readonly DependencyProperty HideAllProperty = DependencyProperty.RegisterAttached("HideAll", typeof(Visibility), typeof(IsoApiViewBehavior), new PropertyMetadata(Visibility.Visible, OnHideAllChanged));
        private static void OnHideAllChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            obj.SetValue(ShowStateCodeProperty, Visibility.Collapsed);
            obj.SetValue(ShowCountryCodeProperty, Visibility.Collapsed);
            obj.SetValue(ShowRegionCodeProperty, Visibility.Collapsed);
        }
    }
}
