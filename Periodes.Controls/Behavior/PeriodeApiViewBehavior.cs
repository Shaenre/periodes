﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Controls.Behavior
{
    public class PeriodeApiViewBehavior
    {
        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(PeriodeApiView))]
        public static Visibility GetShowTypePeriodeSouhaite(DependencyObject obj) => (Visibility)obj.GetValue(ShowTypePeriodeSouhaiteProperty);
        public static void SetShowTypePeriodeSouhaite(DependencyObject obj, Visibility value) => obj.SetValue(ShowTypePeriodeSouhaiteProperty, value);
        public static readonly DependencyProperty ShowTypePeriodeSouhaiteProperty = DependencyProperty.RegisterAttached("ShowTypePeriodeSouhaite", typeof(Visibility), typeof(PeriodeApiViewBehavior), new PropertyMetadata(Visibility.Collapsed, OnShowTypePeriodeSouhaiteChanged));
        private static void OnShowTypePeriodeSouhaiteChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) => sender.SetValue(ShowTypePeriodeSouhaiteProperty, e.NewValue);

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(PeriodeApiView))]
        public static Visibility GetShowTypePeriodeOffset(DependencyObject obj) => (Visibility)obj.GetValue(ShowTypePeriodeOffsetProperty);
        public static void SetShowTypePeriodeOffset(DependencyObject obj, Visibility value) => obj.SetValue(ShowTypePeriodeOffsetProperty, value);
        public static readonly DependencyProperty ShowTypePeriodeOffsetProperty = DependencyProperty.RegisterAttached("ShowTypePeriodeOffset", typeof(Visibility), typeof(PeriodeApiViewBehavior), new PropertyMetadata(Visibility.Collapsed, OnShowTypePeriodeOffsetChanged));
        private static void OnShowTypePeriodeOffsetChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) => sender.SetValue(ShowTypePeriodeOffsetProperty, e.NewValue);

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(PeriodeApiView))]
        public static Visibility GetAll(DependencyObject obj) => (Visibility)obj.GetValue(AllProperty);
        public static void SetAll(DependencyObject obj, Visibility value) => obj.SetValue(AllProperty, value);
        public static readonly DependencyProperty AllProperty = DependencyProperty.RegisterAttached("All", typeof(Visibility), typeof(PeriodeApiViewBehavior), new PropertyMetadata(Visibility.Visible, OnAllChanged));
        private static void OnAllChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            obj.SetValue(ShowTypePeriodeSouhaiteProperty, e.NewValue);
            obj.SetValue(ShowTypePeriodeOffsetProperty, e.NewValue);
        }
    }
}
