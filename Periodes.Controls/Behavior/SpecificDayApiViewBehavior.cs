﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Controls.Behavior
{
    public class SpecificDayApiViewBehavior
    {
        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(SpecificDayApiView))]
        public static Visibility GetHideState(DependencyObject obj) => (Visibility)obj.GetValue(HideStateProperty);
        public static void SetHideState(DependencyObject obj, Visibility value) => obj.SetValue(HideStateProperty, value);
        public static readonly DependencyProperty HideStateProperty = DependencyProperty.RegisterAttached("HideState", typeof(Visibility), typeof(SpecificDayApiViewBehavior), new PropertyMetadata(Visibility.Collapsed));

        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(SpecificDayApiView))]
        public static Visibility GetHideRegion(DependencyObject obj) => (Visibility)obj.GetValue(HideRegionProperty);
        public static void SetHideRegion(DependencyObject obj, Visibility value) =>obj.SetValue(HideRegionProperty, value);
        public static readonly DependencyProperty HideRegionProperty = DependencyProperty.RegisterAttached("HideRegion", typeof(Visibility), typeof(SpecificDayApiViewBehavior), new PropertyMetadata(Visibility.Collapsed));


        [Category("Attached property")]
        [AttachedPropertyBrowsableForType(typeof(SpecificDayApiView))]
        public static Visibility GetHideTypeRule(DependencyObject obj) => (Visibility)obj.GetValue(HideTypeRuleProperty);
        public static void SetHideTypeRule(DependencyObject obj, Visibility value) => obj.SetValue(HideTypeRuleProperty, value);
        public static readonly DependencyProperty HideTypeRuleProperty = DependencyProperty.RegisterAttached("HideTypeRule", typeof(Visibility), typeof(SpecificDayApiViewBehavior), new PropertyMetadata(Visibility.Visible));
    }
}
