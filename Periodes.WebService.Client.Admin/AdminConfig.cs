﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core = ID3iHttpClient;

namespace Periodes.WebService.Client
{
    public class AdminConfig : Core::ClientConfiguration
    {
        public static AdminConfig Conf { get; private set; }
        
        static AdminConfig()
        {
            Conf = (AdminConfig)ConfigurationManager.GetSection(nameof(AdminConfig));
            if (!Conf.ElementInformation.IsPresent)
                throw new Exception($"La section de configuration {nameof(AdminConfig)} n'a pas été trouvée.");
        }
        public static void CheckConsistency() { }
    }
}
