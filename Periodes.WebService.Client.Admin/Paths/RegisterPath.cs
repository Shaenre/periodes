﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class RegisterPath : SegmentPath
    {
        protected override string Segment => "Register";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal RegisterPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de créer un type de période.
        /// </summary>
        /// <param name="model">
        ///     Type de période à créer.
        /// </param>
        /// <returns>
        ///     Le type de période créé.
        /// </returns>
        public Either<string, TypePeriodeModel> Call(TypePeriodeModel model) => DoJob<TypePeriodeModel>(model);
    }

    public class AllTPPath : SegmentPath
    {
        protected override string Segment => "All";
        public override HttpMethod HttpMethod => HttpMethod.Get;
        internal AllTPPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de créer un type de période.
        /// </summary>
        /// <param name="model">
        ///     Type de période à créer.
        /// </param>
        /// <returns>
        ///     Le type de période créé.
        /// </returns>
        public Either<string, List<TypePeriodeModel>> Call() => DoJob<List<TypePeriodeModel>>();
    }
}
