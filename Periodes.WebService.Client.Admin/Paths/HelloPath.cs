﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class HelloPath : SegmentPath
    {
        protected override string Segment => "Hello";
        internal HelloPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de tester si l'Api est accessible.
        /// </summary>
        public Either<string, RetourApi<CodeApi, string>> Call() => DoJob<RetourApi<CodeApi, string>>();
    }
}
