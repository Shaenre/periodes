﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class PeriodePath : SegmentPath
    {
        protected override string Segment => "Periodes";
        internal PeriodePath(SegmentPath parent) : base(parent) { }
        public SomePath Some => new SomePath(this);
        public SavePath Save => new SavePath(this);
        public MassSavePath MassSave => new MassSavePath(this);
    }  
}
