﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class TypePeriodePath : SegmentPath
    {
        protected override string Segment => "TypePeriode";
        internal TypePeriodePath(SegmentPath parent) : base(parent) { }
        public TypePath Type => new TypePath(this);
        public RegisterPath Register => new RegisterPath(this);
        public AllTPPath All => new AllTPPath(this);
    }
}
