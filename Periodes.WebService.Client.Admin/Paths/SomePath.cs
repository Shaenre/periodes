﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class SomePath : SegmentPath
    {
        protected override string Segment => "Some";
        internal SomePath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de récupérer la liste des périodes pour un type en particulier.
        /// </summary>
        /// <param name="value">
        ///     Code du type de période.
        /// </param>
        /// <returns>
        ///     La liste des périodes du type souhaité.
        /// </returns>
        public Either<string, List<PeriodeModelFull>> Call(string value)
        {
            this.Use(value);
            return DoJob<List<PeriodeModelFull>>();
        }
    }
}
