﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class SavePath : SegmentPath
    {
        protected override string Segment => "Save";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal SavePath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de créer ou de mettre à jour une période.
        /// </summary>
        /// <param name="periode">
        ///     Période à créer ou à mettre à jour.
        /// </param>
        /// <returns>
        ///     La période créée.
        /// </returns>
        public Either<string, RetourApi<CodeApi, PeriodeModelFull>> Call(PeriodeModelFull periode) => DoJob<RetourApi<CodeApi, PeriodeModelFull>>(periode);
    }

    public class MassSavePath : SegmentPath
    {
        protected override string Segment => "MassSave";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal MassSavePath(SegmentPath parent) : base(parent) { }
        public Either<string, RetourApiBase<CodeApi>> Call(List<PeriodeModelFull> lst) => DoJob<RetourApiBase<CodeApi>>(lst);
    }
}
