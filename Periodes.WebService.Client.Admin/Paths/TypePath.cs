﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class TypePath : SegmentPath
    {
        protected override string Segment => "Type";
        internal TypePath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de récupérer le type de période par son code.
        /// </summary>
        /// <param name="value">
        ///     Code du type de période.
        /// </param>
        /// <returns>
        ///     Le type de période correspondant.
        /// </returns>
        public Either<string, TypePeriodeModel> Call(string value)
        {
            this.Use(value);
            return DoJob<TypePeriodeModel>();
        }
    }
}
