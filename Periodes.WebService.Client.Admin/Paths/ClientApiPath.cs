﻿using ID3iHttpClient.Config;
using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class ClientApiPath : SegmentPath
    {
        protected override string Segment => "";
        internal ClientApiPath(SegmentPath parent) : base(parent) { }
        public override string EndPoint => Parent.EndPoint;
        public IndexPath Index => new IndexPath(this);
        public TypePeriodePath TypePeriode => new TypePeriodePath(this);
        public PeriodePath Periode => new PeriodePath(this);

        internal NonGenericHeader<ClientApiPath> NonGenericHeader => new NonGenericHeader<ClientApiPath>(this);
        internal AcceptHeader<ClientApiPath> Accept => new AcceptHeader<ClientApiPath>(this);
        internal UserAgentHeader<ClientApiPath> UserAgent => new UserAgentHeader<ClientApiPath>(this);
        internal AuthorizationHeader<ClientApiPath> Authorize => new AuthorizationHeader<ClientApiPath>(this);
    }
}
