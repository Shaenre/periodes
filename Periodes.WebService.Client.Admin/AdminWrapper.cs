﻿using Core = ID3iHttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.WebService.Client.Paths;
using ID3iHttpClient.Config;

namespace Periodes.WebService.Client
{
    public class AdminWrapper : Core::ClientWrapper
    {
        public ClientApiPath Client { get; set; }
        public AdminWrapper() : base() { }
        protected override void OnConfigure()
        {
            AdminConfig.CheckConsistency();
            Client = new ClientApiPath(new Core::Paths.RootPath(this, AdminConfig.Conf.BaseApiUrl));
            Client
                .NonGenericHeader
                    .Add("ID3iPeriodesApp", AdminConfig.Conf.AppCode)
                    .Configure()
                .Accept
                    .Add(DefaultAcceptHeader.Json)
                    .Configure()
                .UserAgent
                    .Add(new UserAgentHeader<ClientApiPath>.UserAgentParameter("Client-ID3i"))
                    .Configure()
                .Authorize
                    .Set(new AuthorizationHeader<ClientApiPath>.AuthorizationParameter("ID3i-Auth", AdminConfig.Conf.ApiKey))
                    .Configure();
        }
    }
}
