﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class PeriodeModel
    {
        public string Alias { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public PeriodeModel() { }
    }
}
