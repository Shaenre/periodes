﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class PeriodeModelFull
    {
        public int Id { get; set; }
        public int IdTypePeriode { get; set; }
        public string Reference { get; set; }
        public string Designation { get; set; }
        public int Quantite { get; set; }
        public string Alias { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public int Index { get; set; }
        public bool Active { get; set; }
        public int Year { get; set; }
    }
}
