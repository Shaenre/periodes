﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class SettingsNumberFactory
    {
        private SettingsNumber Settings { get; set; } = new SettingsNumber();
        public static SettingsNumberFactory Make => new SettingsNumberFactory();
        protected SettingsNumberFactory() { }

        public SettingsNumberDate Date => new SettingsNumberDate(Settings);

        public class SettingsNumberDate
        {
            private SettingsNumber Settings { get; set; }
            public SettingsNumberDate(SettingsNumber settings) => Settings = settings;
            public SettingsNumberDate Accept(DateTime date)
            {
                Settings.Date = date;
                return this;
            }
            public SettingsNumberTypePeriode TypePeriode => new SettingsNumberTypePeriode(Settings);
        }
        public class SettingsNumberTypePeriode
        {
            private SettingsNumber Settings { get; set; }
            public SettingsNumberTypePeriode(SettingsNumber settings) => Settings = settings;
            public SettingsNumberTypePeriode Accept<T>(T typePeriode)
            {
                Settings.TypePeriode = typePeriode.ToString();
                return this;
            }
            public SettingsNumberTypePeriodeSouhaite TypePeriodeSouhaite => new SettingsNumberTypePeriodeSouhaite(Settings);
        }
        public class SettingsNumberTypePeriodeSouhaite
        {
            private SettingsNumber Settings { get; set; }
            public SettingsNumberTypePeriodeSouhaite(SettingsNumber settings) => Settings = settings;
            public SettingsNumberTypePeriodeSouhaite Accept<T>(T typePeriodeSouhaite)
            {
                Settings.TypePeriodeSouhaite = typePeriodeSouhaite.ToString();
                return this;
            }
            public SettingsNumber Finish() => Settings;
        }
    }
    public class SettingsNumber
    {
        /// <summary>
        /// Date.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Type de la période de référence.
        /// </summary>
        public string TypePeriode { get; set; }
        /// <summary>
        /// Type de la période souhaitée.
        /// </summary>
        public string TypePeriodeSouhaite { get; set; }
    }
}
