﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Periodes.WebService.Models.PeriodePosition;
using static Periodes.WebService.Models.PeriodePolicy;

namespace Periodes.WebService.Models
{
    public class SettingsCountFactory
    {
        private SettingsCount Settings { get; set; } = new SettingsCount();
        public static SettingsCountFactory Make => new SettingsCountFactory();
        protected SettingsCountFactory() { }

        public SettingsCountStartDate Start => new SettingsCountStartDate(Settings);

        public class SettingsCountStartDate
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountStartDate(SettingsCount settings) => Settings = settings;
            public SettingsCountStartDate Accept(DateTime startDate)
            {
                Settings.StartDate = startDate;
                return this;
            }
            public SettingsCountEndDate End => new SettingsCountEndDate(Settings);
        }
        public class SettingsCountEndDate
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountEndDate(SettingsCount settings) => Settings = settings;
            public SettingsCountEndDate Accept(DateTime endDate)
            {
                Settings.EndDate = endDate;
                return this;
            }
            public SettingsCountTypePeriode TypePeriode => new SettingsCountTypePeriode(Settings);
        }
        public class SettingsCountTypePeriode
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountTypePeriode(SettingsCount settings) => Settings = settings;
            public SettingsCountTypePeriode Accept<T>(T typePeriode)
            {
                Settings.TypePeriode = typePeriode.ToString();
                return this;
            }
            public SettingsCountTypePeriodeCounted TypePeriodeCounted => new SettingsCountTypePeriodeCounted(Settings);
        }
        public class SettingsCountTypePeriodeCounted
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountTypePeriodeCounted(SettingsCount settings) => Settings = settings;
            public SettingsCountTypePeriodeCounted Accept<T>(T typePeriodeCounted)
            {
                Settings.TypePeriodeCounted = typePeriodeCounted.ToString();
                return this;
            }
            public SettingsCountPosition Position => new SettingsCountPosition(Settings);
            public SettingsCount Finish() => Settings;
        }
        public class SettingsCountPosition
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountPosition(SettingsCount settings) => Settings = settings;
            public SettingsCountPosition Accept(PeriodePosition position)
            {
                Settings.Position = position;
                return this;
            }
            public SettingsCountPolicy Policy => new SettingsCountPolicy(Settings);
        }
        public class SettingsCountPolicy
        {
            private SettingsCount Settings { get; set; }
            public SettingsCountPolicy(SettingsCount settings) => Settings = settings;
            public SettingsCountPolicy Accept(PeriodePolicy policy)
            {
                Settings.Policy = policy;
                return this;
            }
            public SettingsCount Finish() => Settings;
        }
    }
    public class SettingsCount
    {
        /// <summary>
        /// Date de début.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Date de fin.
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Type des périodes.
        /// </summary>
        public string TypePeriode { get; set; }
        /// <summary>
        /// Type des périodes à compter.
        /// </summary>
        public string TypePeriodeCounted { get; set; }
        /// <summary>
        /// Positions des périodes de début et de fin. Par défaut <see cref="FTL"/>
        /// </summary>
        public PeriodePosition Position { get; set; } = FTL;
        /// <summary>
        /// Politique de gestion des extrémités. Par défaut <see cref="IncludeEndPeriode"/>
        /// </summary>
        public PeriodePolicy Policy { get; set; } = IncludeEndPeriode;

        internal static SettingsCount Get => new SettingsCount();
        public SettingsCount() { }
    }
}
