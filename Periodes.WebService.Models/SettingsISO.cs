﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class SettingsISOFactory
    {
        private SettingsISO Settings { get; set; } = new SettingsISO();
        public static SettingsISOFactory Make => new SettingsISOFactory();
        protected SettingsISOFactory() { }
        public SettingsISOCountry Country => new SettingsISOCountry(Settings);

        public class SettingsISOCountry
        {
            private SettingsISO Settings { get; set; }
            public SettingsISOCountry(SettingsISO settings) => Settings = settings;
            public SettingsISOCountry Accept(string countryCode)
            {
                Settings.CountryCode = countryCode;
                return this;
            }
            public SettingsISO Finish() => Settings;

            public SettingsISOState State => new SettingsISOState(Settings);
        }
        public class SettingsISOState
        {
            private SettingsISO Settings { get; set; }
            public SettingsISOState(SettingsISO settings) => Settings = settings;
            public SettingsISOState Accept(string stateCode)
            {
                Settings.StateCode = stateCode;
                return this;
            }
            public SettingsISO Finish() => Settings;

            public SettingsISORegion Region => new SettingsISORegion(Settings);
        }
        public class SettingsISORegion
        {
            private SettingsISO Settings { get; set; }
            public SettingsISORegion(SettingsISO settings) => Settings = settings;
            public SettingsISORegion Accept(string regionCode)
            {
                Settings.RegionCode = regionCode;
                return this;
            }
            public SettingsISO Finish() => Settings;
        }
    }
    public class SettingsISO
    {
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string RegionCode { get; set; }
    }
}
