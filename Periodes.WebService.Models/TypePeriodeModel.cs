﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class TypePeriodeModel
    {
        public int Id { get; set; }
        public TypePeriodeModel TypeReference { get; set; }
        public string Designation { get; set; }
        public int Index { get; set; }
        public bool EstReference { get; set; }
        public string Code { get; set; }
    }
}
