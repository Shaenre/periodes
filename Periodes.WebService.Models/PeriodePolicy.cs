﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    [Flags]
    public enum PeriodePolicy
    {
        None = 0,
        ExcludeStartPeriode = 1 << 0,
        IncludeEndPeriode = ExcludeStartPeriode << 1,

        All = ExcludeStartPeriode | IncludeEndPeriode
    }
}
