﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class RetourApi<T, U> : RetourApiBase<T>
        where T : struct, IConvertible
    {
        public U Object { get; set; }
    }
}
