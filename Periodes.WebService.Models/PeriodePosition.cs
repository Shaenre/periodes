﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    [Flags]
    public enum PeriodePosition
    {
        None = 0,
        FirstInStart = 1,
        LastInStart = FirstInStart << 1,
        FirstInEnd = LastInStart << 1,
        LastInEnd = FirstInEnd << 1,

        FTF = FirstInStart | FirstInEnd,
        FTL = FirstInStart | LastInEnd,
        LTF = LastInStart | FirstInEnd,
        LTL = LastInStart | LastInEnd
    }
}
