﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class SettingsHolidayFactory
    {
        private SettingsHoliday Settings { get; set; } = new SettingsHoliday();
        public static SettingsHolidayFactory Make => new SettingsHolidayFactory();
        protected SettingsHolidayFactory() { }
        public SettingsHolidayYear Year => new SettingsHolidayYear(Settings);

        public class SettingsHolidayYear
        {
            private SettingsHoliday Settings { get; set; }
            public SettingsHolidayYear(SettingsHoliday settings) => Settings = settings;
            public SettingsHolidayYear Accept(int year)
            {
                Settings.Year = year;
                return this;
            }
            public SettingsHolidayType Type => new SettingsHolidayType(Settings);
        }
        public class SettingsHolidayType
        {
            private SettingsHoliday Settings { get; set; }
            public SettingsHolidayType(SettingsHoliday settings) => Settings = settings;
            public SettingsHolidayType Accept<T>(T value)
            {
                Settings.Type = value.ToString();
                return this;
            }
            public SettingsHolidayCountry Country => new SettingsHolidayCountry(Settings);
        }
        public class SettingsHolidayCountry
        {
            private SettingsHoliday Settings { get; set; }
            public SettingsHolidayCountry(SettingsHoliday settings) => Settings = settings;
            public SettingsHolidayCountry Accept(string countryCode)
            {
                Settings.CountryCode = countryCode;
                return this;
            }
            public SettingsHoliday Finish() => Settings;
            public SettingsHolidayState State => new SettingsHolidayState(Settings);
        }
        public class SettingsHolidayState
        {
            private SettingsHoliday Settings { get; set; }
            public SettingsHolidayState(SettingsHoliday settings) => Settings = settings;
            public SettingsHolidayState Accept(string stateCode)
            {
                Settings.StateCode = stateCode;
                return this;
            }
            public SettingsHoliday Finish() => Settings;
            public SettingsHolidayRegion Region => new SettingsHolidayRegion(Settings);
        }
        public class SettingsHolidayRegion
        {
            private SettingsHoliday Settings { get; set; }
            public SettingsHolidayRegion(SettingsHoliday settings) => Settings = settings;
            public SettingsHolidayRegion Accept(string regionCode)
            {
                Settings.RegionCode = regionCode;
                return this;
            }
            public SettingsHoliday Finish() => Settings;
        }
    }
    public class SettingsHoliday
    {
        public int Year { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string RegionCode { get; set; }
        public string Type { get; set; }
    }
}
