﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class RetourApiBase<T>
        where T : struct, IConvertible
    {
        public T Statut { get; set; }
        public string Message { get; set; }
    }
}
