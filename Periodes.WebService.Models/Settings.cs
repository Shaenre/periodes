﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Models
{
    public class SettingsFactory
    {
        private Settings Settings { get; set; } = new Settings();
        public static SettingsFactory Make => new SettingsFactory();
        protected SettingsFactory() { }
        public SettingsDate Date => new SettingsDate(Settings);
        public class SettingsDate
        {
            private Settings Settings { get; set; }
            public SettingsDate(Settings settings) => Settings = settings;
            public SettingsDate Accept(DateTime date)
            {
                Settings.Date = date;
                return this;
            }
            public SettingsTypePeriode TypePeriode => new SettingsTypePeriode(Settings);
        }
        public class SettingsTypePeriode
        {
            private Settings Settings { get; set; }
            public SettingsTypePeriode(Settings settings) => Settings = settings;
            public SettingsTypePeriode Accept<T>(T typePeriode)
            {
                Settings.TypePeriode = typePeriode.ToString();
                return this;
            }
            public SettingsTypePeriodeSouhaite TypePeriodeSouhaite => new SettingsTypePeriodeSouhaite(Settings);
            public SettingsTypePeriodeOffset TypePeriodeOffset => new SettingsTypePeriodeOffset(Settings);
            public Settings Finish() => Settings;
        }
        public class SettingsTypePeriodeSouhaite
        {
            private Settings Settings { get; set; }
            public SettingsTypePeriodeSouhaite(Settings settings) => Settings = settings;
            public SettingsTypePeriodeSouhaite Accept<T>(T typePeriodeSouhaite)
            {
                Settings.TypePeriodeSouhaite = typePeriodeSouhaite.ToString();
                return this;
            }
            public Settings Finish() => Settings;
        }
        public class SettingsTypePeriodeOffset
        {
            private Settings Settings { get; set; }
            public SettingsTypePeriodeOffset(Settings settings) => Settings = settings;
            public SettingsTypePeriodeOffset Accept<T>(T typePeriodeOffset)
            {
                Settings.TypePeriodeOffset = typePeriodeOffset.ToString();
                return this;
            }
            public SettingsOffset Offset => new SettingsOffset(Settings);
        }
        public class SettingsOffset
        {
            private Settings Settings { get; set; }
            public SettingsOffset(Settings settings) => Settings = settings;
            public SettingsOffset Accept(int offset)
            {
                Settings.Offset = offset;
                return this;
            }
            public Settings Finish() => Settings;
            public SettingsTypePeriodeSouhaite TypePeriodeSouhaite => new SettingsTypePeriodeSouhaite(Settings);
        }
    }
    public class Settings
    {
        /// <summary>
        /// Date dont on souhaite récupérer la période.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Type de la période de référence.
        /// </summary>
        public string TypePeriode { get; set; }
        /// <summary>
        /// Type de période souhaité.
        /// </summary>
        public string TypePeriodeSouhaite { get; set; }
        /// <summary>
        /// Nombre de periodes à décaller à partir de la période de référence.
        /// </summary>
        public int Offset { get; set; }
        /// <summary>
        /// Type des periodes à décaller à partir de la période de référence.
        /// </summary>
        public string TypePeriodeOffset { get; set; }

        internal static Settings Get => new Settings();
        public Settings() { }
    }
}
