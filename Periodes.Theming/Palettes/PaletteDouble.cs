﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Theming.Palettes
{
    public class PaletteDouble : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name { get; set; }
        private Action<string, double> DoubleChangedAction { get; set; }
        private double value;
        public double Value
        {
            get => value;
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    OnPropertyChanged(nameof(Value));
                    DoubleChangedAction(Name, Value);
                }
            }
        }
        public PaletteDouble(string name, double value, Action<string, double> doubleChangedAction)
        {
            Name = name;
            DoubleChangedAction = doubleChangedAction;
            Value = value;
        }
    }
}
