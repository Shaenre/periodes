﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using System.IO;
using ID3iCore;

namespace Periodes.Theming.Palettes
{
    internal static class PaletteManager
    {
        internal static IEnumerable<IPalette> GetMaterialPalettes()
        {
            var telerikPalettes = new List<IPalette>();
            foreach (var item in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Palettes", "Material"), "*.json"))
                PaletteMaterial.FromJson(item).IfNotNull(x => telerikPalettes.Add(x));
            return telerikPalettes;
        }
        internal static IEnumerable<IPalette> GetOffice2016Palettes()
        {
            var telerikPalettes = new List<IPalette>();
            foreach (var item in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Palettes", "Office2016"), "*.json"))
                PaletteOffice2016.FromJson(item).IfNotNull(x => telerikPalettes.Add(x));
            return telerikPalettes;
        }
        internal static IEnumerable<IPalette> GetApplicationPalettes()
        {
            var applicationPalettes = new List<IPalette>();
            foreach (var item in Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Palettes", "Application"), "*.json"))
                PaletteApplication.FromJson(item).IfNotNull(x => applicationPalettes.Add(x));
            return applicationPalettes;
        }
    }
}
