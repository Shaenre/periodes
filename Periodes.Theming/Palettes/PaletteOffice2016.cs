﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using System.Windows.Media;
using System.IO;

namespace Periodes.Theming.Palettes
{
    internal class PaletteOffice2016 : Palette
    {
        public PaletteOffice2016(PaletteSettings settings, params string[] colors) : base(settings)
        {
            InitializeColors(colors);
        }
        private void InitializeColors(string[] colors)
        {
            var paletteColors = new List<PaletteColor>
            {
                new PaletteColor("AccentColor", (Color)ColorConverter.ConvertFromString(colors[0]), PaletteColorChangedAction),
                new PaletteColor("AccentFocusedColor", (Color)ColorConverter.ConvertFromString(colors[1]), PaletteColorChangedAction),
                new PaletteColor("AccentMouseOverColor", (Color)ColorConverter.ConvertFromString(colors[2]), PaletteColorChangedAction),
                new PaletteColor("AccentPressedColor", (Color)ColorConverter.ConvertFromString(colors[3]), PaletteColorChangedAction),
                new PaletteColor("AlternativeColor", (Color)ColorConverter.ConvertFromString(colors[4]), PaletteColorChangedAction),
                new PaletteColor("BasicColor", (Color)ColorConverter.ConvertFromString(colors[5]), PaletteColorChangedAction),
                new PaletteColor("ComplementaryColor", (Color)ColorConverter.ConvertFromString(colors[6]), PaletteColorChangedAction),
                new PaletteColor("IconColor", (Color)ColorConverter.ConvertFromString(colors[7]), PaletteColorChangedAction),
                new PaletteColor("MainColor", (Color)ColorConverter.ConvertFromString(colors[8]), PaletteColorChangedAction),
                new PaletteColor("MarkerColor", (Color)ColorConverter.ConvertFromString(colors[9]), PaletteColorChangedAction),
                new PaletteColor("MarkerInvertedColor", (Color)ColorConverter.ConvertFromString(colors[10]), PaletteColorChangedAction),
                new PaletteColor("MouseOverColor", (Color)ColorConverter.ConvertFromString(colors[11]), PaletteColorChangedAction),
                new PaletteColor("PressedColor", (Color)ColorConverter.ConvertFromString(colors[12]), PaletteColorChangedAction),
                new PaletteColor("PrimaryColor", (Color)ColorConverter.ConvertFromString(colors[13]), PaletteColorChangedAction),
                new PaletteColor("SelectedColor", (Color)ColorConverter.ConvertFromString(colors[14]), PaletteColorChangedAction),
                new PaletteColor("ValidationColor", (Color)ColorConverter.ConvertFromString(colors[15]), PaletteColorChangedAction)
            };
            Colors = new List<PaletteColor>(paletteColors);
        }
        protected override void OnColorChanged(string name, Color color)
        {
            switch (name)
            {
                case "AccentColor":
                    Office2016Palette.Palette.AccentColor = color;
                    break;
                case "AccentFocusedColor":
                    Office2016Palette.Palette.AccentFocusedColor = color;
                    break;
                case "AccentMouseOverColor":
                    Office2016Palette.Palette.AccentMouseOverColor = color;
                    break;
                case "AccentPressedColor":
                    Office2016Palette.Palette.AccentPressedColor = color;
                    break;
                case "AlternativeColor":
                    Office2016Palette.Palette.AlternativeColor = color;
                    break;
                case "BasicColor":
                    Office2016Palette.Palette.BasicColor = color;
                    break;
                case "ComplementaryColor":
                    Office2016Palette.Palette.ComplementaryColor = color;
                    break;
                case "IconColor":
                    Office2016Palette.Palette.IconColor = color;
                    break;
                case "MainColor":
                    Office2016Palette.Palette.MainColor = color;
                    break;
                case "MarkerColor":
                    Office2016Palette.Palette.MarkerColor = color;
                    break;
                case "MarkerInvertedColor":
                    Office2016Palette.Palette.MarkerInvertedColor = color;
                    break;
                case "MouseOverColor":
                    Office2016Palette.Palette.MouseOverColor = color;
                    break;
                case "PressedColor":
                    Office2016Palette.Palette.PressedColor = color;
                    break;
                case "PrimaryColor":
                    Office2016Palette.Palette.PrimaryColor = color;
                    break;
                case "SelectedColor":
                    Office2016Palette.Palette.SelectedColor = color;
                    break;
                case "ValidationColor":
                    Office2016Palette.Palette.ValidationColor = color;
                    break;
                default:
                    break;
            }
        }

        public static PaletteOffice2016 FromJson(string pathFile)
        {
            PaletteSerializable paletteSerializable = null;
            using (TextReader reader = new StreamReader(pathFile))
                paletteSerializable = Newtonsoft.Json.JsonConvert.DeserializeObject<PaletteSerializable>(reader.ReadToEnd());
            if (paletteSerializable != null)
                return new PaletteOffice2016(paletteSerializable.Settings, paletteSerializable.Colors);
            return null;
        }
    }
}
