﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Periodes.Theming.Palettes
{
    public interface IPalette
    {
        PaletteSettings Settings { get; set; }
        IEnumerable<PaletteColor> Colors { get; set; }
        Action<string, Color> PaletteColorChangedAction { get; set; }
        IEnumerable<PaletteGridLength> GridLength { get; set; }
        Action<string, GridLength> PaletteGridLengthChangedAction { get; set; }
        IEnumerable<PaletteDouble> Doubles { get; set; }
        Action<string, double> PaletteDoubleChangedAction { get; set; }

        void RefreshPalette();
    }
}
