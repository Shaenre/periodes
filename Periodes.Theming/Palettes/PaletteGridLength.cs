﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Theming.Palettes
{
    public class PaletteGridLength : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name { get; set; }
        private Action<string, GridLength> GridLengthChangedAction { get; set; }
        private GridLength gridLength;
        public GridLength GridLength
        {
            get => gridLength;
            set
            {
                if (gridLength != value)
                {
                    gridLength = value;
                    OnPropertyChanged(nameof(GridLength));
                    GridLengthChangedAction(Name, GridLength);
                }
            }
        }
        public PaletteGridLength(string name, GridLength gridLength, Action<string, GridLength> gridLengthChangedAction)
        {
            Name = name;
            GridLengthChangedAction = gridLengthChangedAction;
            GridLength = gridLength;
        }
    }
}
