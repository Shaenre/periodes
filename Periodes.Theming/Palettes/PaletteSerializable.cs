﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Theming.Palettes
{
    public class PaletteSerializable
    {
        public PaletteSettings Settings { get; set; }
        public string[] Colors { get; set; }
        public GridLength[] GridLength { get; set; }
        public double[] Doubles { get; set; }
    }
}
