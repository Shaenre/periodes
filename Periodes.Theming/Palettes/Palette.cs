﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Periodes.Theming.Palettes
{
    public class Palette : INotifyPropertyChanged, IPalette
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public PaletteSettings Settings { get; set; }
        public IEnumerable<PaletteColor> Colors { get; set; }
        public Action<string, Color> PaletteColorChangedAction { get; set; }
        public IEnumerable<PaletteGridLength> GridLength { get; set; }
        public Action<string, GridLength> PaletteGridLengthChangedAction { get; set; }
        public IEnumerable<PaletteDouble> Doubles { get; set; }
        public Action<string, double> PaletteDoubleChangedAction { get; set; }
        public Palette(PaletteSettings settings)
        {
            PaletteColorChangedAction = new Action<string, Color>((string name, Color color) => OnColorChanged(name, color));
            PaletteGridLengthChangedAction = new Action<string, GridLength>((string name, GridLength length) => OnGridLengthChanged(name, length));
            PaletteDoubleChangedAction = new Action<string, double>((string name, double value) => OnDoubleChanged(name, value));
            Settings = settings;
        }
        public void RefreshPalette()
        {
            if (Colors != null)
                foreach (var item in Colors)
                    OnColorChanged(item.Name, item.Color);
            if (GridLength != null)
                foreach (var item in GridLength)
                    OnGridLengthChanged(item.Name, item.GridLength);
            if (Doubles != null)
                foreach (var item in Doubles)
                    OnDoubleChanged(item.Name, item.Value);
        }
        protected virtual void OnColorChanged(string name, Color color) { }
        protected virtual void OnGridLengthChanged(string name, GridLength length) { }
        protected virtual void OnDoubleChanged(string name, double value) { }
    }
}
