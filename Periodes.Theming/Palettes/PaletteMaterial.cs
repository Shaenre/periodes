﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using System.Windows.Media;
using System.IO;

namespace Periodes.Theming.Palettes
{
    internal class PaletteMaterial : Palette
    {
        public PaletteMaterial(PaletteSettings settings, params string[] colors) : base(settings)
        {
            InitializeColors(colors);
        }
        private void InitializeColors(params string[] colors)
        {
            var paletteColors = new List<PaletteColor>
            {
                new PaletteColor("AccentNormalColor", (Color)ColorConverter.ConvertFromString(colors[0]), PaletteColorChangedAction),
                new PaletteColor("AccentHoverColor", (Color)ColorConverter.ConvertFromString(colors[1]), PaletteColorChangedAction),
                new PaletteColor("AccentPressedColor", (Color)ColorConverter.ConvertFromString(colors[2]), PaletteColorChangedAction),
                new PaletteColor("DividerColor", (Color)ColorConverter.ConvertFromString(colors[3]), PaletteColorChangedAction),
                new PaletteColor("IconColor", (Color)ColorConverter.ConvertFromString(colors[4]), PaletteColorChangedAction),
                new PaletteColor("MainColor", (Color)ColorConverter.ConvertFromString(colors[5]), PaletteColorChangedAction),
                new PaletteColor("MarkerColor", (Color)ColorConverter.ConvertFromString(colors[6]), PaletteColorChangedAction),
                new PaletteColor("ValidationColor", (Color)ColorConverter.ConvertFromString(colors[7]), PaletteColorChangedAction),
                new PaletteColor("ComplementaryColor", (Color)ColorConverter.ConvertFromString(colors[8]), PaletteColorChangedAction),
                new PaletteColor("AlternativeColor", (Color)ColorConverter.ConvertFromString(colors[9]), PaletteColorChangedAction),
                new PaletteColor("MarkerInvertedColor", (Color)ColorConverter.ConvertFromString(colors[10]), PaletteColorChangedAction),
                new PaletteColor("PrimaryColor", (Color)ColorConverter.ConvertFromString(colors[11]), PaletteColorChangedAction),
                new PaletteColor("PrimaryNormalColor", (Color)ColorConverter.ConvertFromString(colors[12]), PaletteColorChangedAction),
                new PaletteColor("PrimaryFocusColor", (Color)ColorConverter.ConvertFromString(colors[13]), PaletteColorChangedAction),
                new PaletteColor("PrimaryHoverColor", (Color)ColorConverter.ConvertFromString(colors[14]), PaletteColorChangedAction),
                new PaletteColor("PrimaryPressedColor", (Color)ColorConverter.ConvertFromString(colors[15]), PaletteColorChangedAction),
                new PaletteColor("RippleColor", (Color)ColorConverter.ConvertFromString(colors[16]), PaletteColorChangedAction),
                new PaletteColor("ReadOnlyBackgroundColor", (Color)ColorConverter.ConvertFromString(colors[17]), PaletteColorChangedAction),
                new PaletteColor("ReadOnlyBorderColor", (Color)ColorConverter.ConvertFromString(colors[18]), PaletteColorChangedAction)
            };
            Colors = new List<PaletteColor>(paletteColors);
        }
        protected override void OnColorChanged(string name, Color color)
        {
            switch (name)
            {
                case "AccentNormalColor":
                    MaterialPalette.Palette.AccentNormalColor = color;
                    break;
                case "AccentHoverColor":
                    MaterialPalette.Palette.AccentHoverColor = color;
                    break;
                case "AccentPressedColor":
                    MaterialPalette.Palette.AccentPressedColor = color;
                    break;
                case "DividerColor":
                    MaterialPalette.Palette.DividerColor = color;
                    break;
                case "IconColor":
                    MaterialPalette.Palette.IconColor = color;
                    break;
                case "MainColor":
                    MaterialPalette.Palette.MainColor = color;
                    break;
                case "MarkerColor":
                    MaterialPalette.Palette.MarkerColor = color;
                    break;
                case "ValidationColor":
                    MaterialPalette.Palette.ValidationColor = color;
                    break;
                case "ComplementaryColor":
                    MaterialPalette.Palette.ComplementaryColor = color;
                    break;
                case "AlternativeColor":
                    MaterialPalette.Palette.AlternativeColor = color;
                    break;
                case "MarkerInvertedColor":
                    MaterialPalette.Palette.MarkerInvertedColor = color;
                    break;
                case "PrimaryColor":
                    MaterialPalette.Palette.PrimaryColor = color;
                    break;
                case "PrimaryNormalColor":
                    MaterialPalette.Palette.PrimaryNormalColor = color;
                    break;
                case "PrimaryFocusColor":
                    MaterialPalette.Palette.PrimaryFocusColor = color;
                    break;
                case "PrimaryHoverColor":
                    MaterialPalette.Palette.PrimaryHoverColor = color;
                    break;
                case "PrimaryPressedColor":
                    MaterialPalette.Palette.PrimaryPressedColor = color;
                    break;
                case "RippleColor":
                    MaterialPalette.Palette.RippleColor = color;
                    break;
                case "ReadOnlyBackgroundColor":
                    MaterialPalette.Palette.ReadOnlyBackgroundColor = color;
                    break;
                case "ReadOnlyBorderColor":
                    MaterialPalette.Palette.ReadOnlyBorderColor = color;
                    break;
                default:
                    break;
            }
        }

        public static PaletteMaterial FromJson(string pathFile)
        {
            PaletteSerializable paletteSerializable = null;
            using (TextReader reader = new StreamReader(pathFile))
                paletteSerializable = Newtonsoft.Json.JsonConvert.DeserializeObject<PaletteSerializable>(reader.ReadToEnd());
            if (paletteSerializable != null)
                return new PaletteMaterial(paletteSerializable.Settings, paletteSerializable.Colors);
            return null;
        }
    }
}
