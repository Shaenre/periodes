﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Periodes.Theming.Palettes
{
    public class PaletteColor : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name { get; set; }
        private Action<string, Color> ColorChangedAction { get; set; }
        private Color color;
        public Color Color
        {
            get => color;
            set
            {
                if (color != value)
                {
                    color = value;
                    OnPropertyChanged(nameof(Color));
                    ColorChangedAction(Name, Color);
                }
            }
        }
        public PaletteColor(string name, Color color, Action<string, Color> colorChangedAction)
        {
            Name = name;
            ColorChangedAction = colorChangedAction;
            Color = color;
        }
    }
}
