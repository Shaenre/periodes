﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Periodes.Theming.Resources;
using System.IO;

namespace Periodes.Theming.Palettes
{
    public class PaletteApplication : Palette
    {
        public PaletteApplication(PaletteSettings settings, string[] colors, GridLength[] gridLength, double[] doubles) : base(settings)
        {
            Initialize(colors, gridLength, doubles);
        }
        private void Initialize(string[] colors, GridLength[] gridLength, double[] doubles)
        {
            var paletteColors = new List<PaletteColor>
            {
                new PaletteColor(nameof(ThemeResource.CustomColor), (Color)ColorConverter.ConvertFromString(colors[0]), PaletteColorChangedAction),
            };
            Colors = new List<PaletteColor>(paletteColors);
        }
        protected override void OnColorChanged(string name, Color color)
        {
            switch (name)
            {
                case nameof(ThemeResource.CustomColor):
                    ThemeResource.Palette.CustomColor = color;
                    break;
            }
        }

        protected override void OnGridLengthChanged(string name, GridLength length)
        {
            switch (name)
            {
                default:
                    break;
            }
        }

        protected override void OnDoubleChanged(string name, double value)
        {
            switch (name)
            {
                default:
                    break;
            }
        }

        public static PaletteApplication FromJson(string pathFile)
        {
            PaletteSerializable paletteSerializable = null;
            using (TextReader reader = new StreamReader(pathFile))
                paletteSerializable = Newtonsoft.Json.JsonConvert.DeserializeObject<PaletteSerializable>(reader.ReadToEnd());
            if (paletteSerializable != null)
                return new PaletteApplication(paletteSerializable.Settings, paletteSerializable.Colors, paletteSerializable.GridLength, paletteSerializable.Doubles);
            return null;
        }
    }
}
