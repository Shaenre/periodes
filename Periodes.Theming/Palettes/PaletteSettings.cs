﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Theming.Palettes
{
    public class PaletteSettings
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public PaletteSettings(string name, string displayName)
        {
            Name = name;
            DisplayName = displayName;
        }
    }
}
