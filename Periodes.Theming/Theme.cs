﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Theming.Palettes;
using ID3iCore;

namespace Periodes.Theming
{
    public class Theme : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name { get; set; }
        private IPalette telerikPalette;
        public IPalette TelerikPalette
        {
            get => telerikPalette;
            set
            {
                if (telerikPalette != value && value != null)
                {
                    telerikPalette = value;
                    OnPropertyChanged(nameof(TelerikPalette));
                    TelerikPalette.RefreshPalette();
                }
            }
        }

        private IEnumerable<IPalette> telerikPalettes;
        public IEnumerable<IPalette> TelerikPalettes
        {
            get => telerikPalettes;
            set
            {
                if (telerikPalettes != value)
                {
                    telerikPalettes = value;
                    OnPropertyChanged(nameof(TelerikPalettes));
                    if (string.IsNullOrEmpty(Preferences.Instance.Palette) || Preferences.Instance.Theme != Name)
                        TelerikPalette = TelerikPalettes.First();
                    else
                        TelerikPalette = TelerikPalettes.FirstOrDefault(x => x.Settings.DisplayName == Preferences.Instance.Palette).ValueOrDefault(x => x, TelerikPalettes.First());
                }
            }
        }

        private IPalette applicationPalette;
        public IPalette ApplicationPalette
        {
            get => applicationPalette;
            set
            {
                if (applicationPalette != value && value != null)
                {
                    applicationPalette = value;
                    OnPropertyChanged(nameof(ApplicationPalette));
                    ApplicationPalette.RefreshPalette();
                }
            }
        }

        private IEnumerable<IPalette> applicationPalettes;
        public IEnumerable<IPalette> ApplicationPalettes
        {
            get => applicationPalettes;
            set
            {
                if (applicationPalettes != value)
                {
                    applicationPalettes = value;
                    OnPropertyChanged(nameof(ApplicationPalettes));
                    ApplicationPalette = ApplicationPalettes.First();
                }
            }
        }
    }
}
