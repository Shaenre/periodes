﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3iShared.Module;
using Prism.Unity;
using Periodes.Theming.View;
using Periodes.Global;
using ID3iShared.Regions;
using Telerik.Windows.Controls;
using Microsoft.Practices.Unity;

namespace Periodes.Theming
{
    public class ThemeModule : Module
    {
        public override void Initialize()
        {
            ThemeManager.Initialize();
            Container.RegisterTypeForNavigation<ThemeAndPaletteView>();

            RegionManager.RegisterViewWithRegion(RegionNames.ThemeRegion, () => new ViewWithName(new RadButton()
            {
                Content = "Paramètres",
                Command = Container.Resolve<IApplicationCommands>().NavigateCommand,
                CommandParameter = nameof(ThemeAndPaletteView)
            }, $"RadRadioButton_{nameof(ThemeAndPaletteView)}_{new Random().Next()}"));
        }
    }
}
