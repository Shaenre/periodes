﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ID3iCore;
using ID3iCore.WPF.Dictionaries;

namespace Periodes.Theming
{
    internal static class ResourcesManager
    {
        private static List<string> XamlFiles { get; } = new List<string>()
        {
            "System.Windows.xaml",
            "Telerik.Windows.Controls.xaml",
            //"Telerik.Windows.Cloud.Controls.xaml",
            "Telerik.Windows.Controls.Input.xaml",
            "Telerik.Windows.Controls.Navigation.xaml",
            //"Telerik.Windows.Controls.RibbonView.xaml",
            //"Telerik.Windows.Controls.Diagrams.xaml",
            //"Telerik.Windows.Controls.Diagrams.Extensions.xaml",
            //"Telerik.Windows.Controls.Docking.xaml",
            //"Telerik.Windows.Controls.ScheduleView.xaml",
            //"Telerik.Windows.Controls.GanttView.xaml",
            "Telerik.Windows.Controls.GridView.xaml",
            //"Telerik.Windows.Controls.DataVisualization.xaml",
            //"Telerik.Windows.Controls.Pivot.xaml",
            //"Telerik.Windows.Controls.PivotFieldList.xaml",
            //"Telerik.Windows.Controls.ImageEditor.xaml",
            //"Telerik.Windows.Controls.Spreadsheet.xaml"
        };
        private static List<string> Styles { get; } = new List<string>()
        {
            "Material.xaml",
            "Office2016.xaml"
        };
        public static void LoadResourceDictionaries(Theme theme)
        {
            Application.Current.Resources.MergedDictionaries.RemoveWhere(x => x is ThemeResourceDictionary);
            for (int i = 0; i <= XamlFiles.Count - 1; i++)
            {
                string path = $"/Telerik.Windows.Themes.{theme.Name};component/Themes/{XamlFiles[i]}";
                Application.Current.Resources.MergedDictionaries.Insert(i, new ThemeResourceDictionary() { Source = new Uri(path, UriKind.RelativeOrAbsolute) });
            }
            if (Styles.Contains($"{theme.Name}.xaml"))
                Application.Current.Resources.MergedDictionaries.Insert(XamlFiles.Count, (new ThemeResourceDictionary() { Source = new Uri($"/Periodes.Theming;component/Themes/{theme.Name}.xaml", UriKind.RelativeOrAbsolute) }));
            Application.Current.Resources.MergedDictionaries.Insert(XamlFiles.Count + 1, (new ThemeResourceDictionary() { Source = new Uri("/Periodes.Theming;component/Themes/Controls.xaml", UriKind.RelativeOrAbsolute) }));
        }
    }
}
