﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace Periodes.Theming
{
    public class Preferences
    {
        internal static string FilePath
        {
            get
            {
                return Path.Combine(ConfigFile.Conf.ApplicationDirectory, "Preferences.Themes.json");
            }
        }

        public string Theme { get; set; }
        public string Palette { get; set; }

        public static Preferences Instance { get; private set; }

        static Preferences()
        {
            if (File.Exists(FilePath))
                using (TextReader reader = new StreamReader(FilePath))
                    Instance = JsonConvert.DeserializeObject<Preferences>(reader.ReadToEnd());
            else
                Instance = new Preferences();
        }

        public void Enregistrer()
        {
            using (FileStream fs = new FileStream(FilePath, FileMode.Create))
            using (StreamWriter sw = new StreamWriter(fs))
                sw.WriteLine(JsonConvert.SerializeObject(this));
        }
    }
}
