﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Periodes.Theming.ViewModel;

namespace Periodes.Theming.View
{
    /// <summary>
    /// Logique d'interaction pour ThemeAndPaletteView.xaml
    /// </summary>
    public partial class ThemeAndPaletteView : UserControl
    {
        public ThemeAndPaletteView(ThemeAndPaletteViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
        }
    }
}
