﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Theming.Resources
{
    [TypeConverter(typeof(ThemeResourceKeyTypeConverter))]
    public sealed class ThemeResourceKey : ResourceKey
    {
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly ResourceKey CustomBrush = new ThemeResourceKey(ThemeResourceKeyID.CustomBrush);
        internal ThemeResourceKeyID Key { get; private set; }
        internal ThemeResourceKey(ThemeResourceKeyID key) => Key = key;

        public override Assembly Assembly => typeof(ThemeResourceKey).Assembly;
        public override string ToString() => Key.ToString();
    }
}
