﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace Periodes.Theming.Resources
{
    public class ThemeResourceKeyTypeConverter : TypeConverter
    {
        private static Dictionary<string, ResourceKey> keys;
        private static Lazy<StandardValuesCollection> standardValues = new Lazy<StandardValuesCollection>(() => new StandardValuesCollection(keys.Keys.ToList()));
        static ThemeResourceKeyTypeConverter()
        {
            keys = new Dictionary<string, ResourceKey>
            {
                { ThemeResourceKeyID.CustomBrush.ToString(), ThemeResourceKeyProxy.CustomBrush }
            };
        }
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context) => standardValues.Value;
        public override bool GetPropertiesSupported(ITypeDescriptorContext context) => true;
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return ((destinationType == typeof(MarkupExtension)) && (context is IValueSerializerContext)) || base.CanConvertTo(context, destinationType);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string stringValue)
            {
                if (keys.TryGetValue(stringValue, out ResourceKey resourceKey))
                    return resourceKey;
                else if (stringValue.StartsWith("{x:Static Resources:") && stringValue.EndsWith("}"))
                {
                    var tokens = stringValue.Substring(20, stringValue.Length - 21).Split('.');
                    if (tokens.Length == 2 && keys.TryGetValue(tokens[1], out resourceKey))
                        return resourceKey;
                }
            }

            return base.ConvertFrom(context, culture, value);
        }
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
                return $"{{x:Static Resources:{nameof(ThemeResourceKeyProxy)}.{Convert.ToString(value)}}}";

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
