﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Periodes.Theming.Resources
{
    public sealed class ThemeResource : Freezable
    {
        internal static DependencyProperty RegisterColor(string propertyName, long color)
        {
            var a = (byte)(color >> 24);
            var r = (byte)((color & 0x00FF0000) >> 16);
            var g = (byte)((color & 0x0000FF00) >> 8);
            var b = (byte)(color & 0x000000FF);

            return DependencyProperty.Register(propertyName, typeof(Color), typeof(ThemeResource), new PropertyMetadata(Color.FromArgb(a, r, g, b), OnResourcePropertyChanged));
        }
        internal static DependencyProperty RegisterDouble(string propertyName, double value)
        {
            return DependencyProperty.Register(propertyName, typeof(double), typeof(ThemeResource), new PropertyMetadata(value, OnResourcePropertyChanged));
        }
        internal static DependencyProperty RegisterGridLength(string propertyName, GridLength length)
        {
            return DependencyProperty.Register(propertyName, typeof(GridLength), typeof(ThemeResource), new PropertyMetadata(length, OnResourcePropertyChanged));
        }
        private static void OnResourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ThemeResource themePalette)
                themePalette.OnResourcePropertyChanged(e);
        }
        private static DependencyProperty GetBindingProperty(DependencyObject freezable)
        {
            if (freezable is SolidColorBrush)
                return SolidColorBrush.ColorProperty;
            else if (freezable is GradientStop)
                return GradientStop.ColorProperty;
            return null;
        }
        protected override sealed Freezable CreateInstanceCore()
        {
            throw new NotImplementedException();
        }

        #region Freezable
        private static void TryFreeze(Freezable freezable)
        {
            DependencyProperty dp = GetBindingProperty(freezable);
            if (dp != null)
            {
                BindingExpression bindingExpression = BindingOperations.GetBindingExpression(freezable, dp);
                if (bindingExpression == null)
                    freezable.Changed += new EventHandler(OnFreezableChanged);
                else
                {
                    bindingExpression.UpdateTarget();
                    freezable.SetValue(dp, freezable.GetValue(dp));
                    BindingOperations.ClearAllBindings(freezable);
                    freezable.Freeze();
                }
            }
        }

        private static void OnFreezableChanged(object sender, EventArgs e)
        {
            Freezable freezable = sender as Freezable;
            DependencyProperty dependencyProperty = GetBindingProperty(freezable);
            BindingExpression bindingExpression = BindingOperations.GetBindingExpression(freezable, dependencyProperty);
            if (bindingExpression != null)
            {
                freezable.Changed -= OnFreezableChanged;
                bindingExpression.UpdateTarget();
                freezable.SetValue(dependencyProperty, freezable.GetValue(dependencyProperty));
                BindingOperations.ClearAllBindings(freezable);
                freezable.Freeze();
            }
        }
        internal static void OnIsFreezableChanged(DependencyObject d, DependencyPropertyChangedEventArgs args, DependencyProperty isFrozenProperty, DependencyObject freezeSource)
        {
            bool newValue = (bool)args.NewValue;
            if (!newValue)
                throw new InvalidOperationException("ThemeResources.IsFreezable attached property can not be unset");
            if (d is Freezable freezable)
            {
                bool isFrozen = (bool)freezeSource.GetValue(isFrozenProperty);
                if (isFrozen)
                    TryFreeze(freezable);
                else
                    if (freezable.ReadLocalValue(isFrozenProperty) == DependencyProperty.UnsetValue)
                    BindingOperations.SetBinding(freezable, isFrozenProperty, new Binding() { Path = new PropertyPath(isFrozenProperty), Source = freezeSource });
            }
        }
        internal static void OnIsFrozenChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            bool isFreezing = (bool)args.NewValue;
            if (isFreezing)
                if (d is Freezable freezable)
                    TryFreeze(freezable);
        }
        internal static void FreezeAndSetResource(ResourceDictionary resourceDictioanry, object key, object resource)
        {
            if (resource is Freezable freezable)
                freezable.Freeze();

            resourceDictioanry[key] = resource;
        }
        public static readonly DependencyProperty IsFreezableProperty =
            DependencyProperty.RegisterAttached("IsFreezable", typeof(bool), typeof(ThemeResource), new PropertyMetadata(false, OnIsFreezablePropertyChanged));
        private static readonly DependencyProperty IsFrozenProperty =
            DependencyProperty.RegisterAttached("IsFrozen", typeof(bool), typeof(ThemeResource), new PropertyMetadata(false, OnIsFrozenChanged));
        public static bool GetIsFreezable(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(IsFreezableProperty);
        }
        public static void SetIsFreezable(DependencyObject dependencyObject, bool value)
        {
            dependencyObject.SetValue(IsFreezableProperty, value);
        }
        private static void OnIsFreezablePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            OnIsFreezableChanged(d, e, IsFrozenProperty, ThemeResource.Palette);
        }
        protected override sealed bool FreezeCore(bool isChecking)
        {
            if (!isChecking)
                FreezeOverride();
            return base.FreezeCore(isChecking);
        }
        private static ThemeResource frozenPalette;
        internal void FreezeOverride()
        {
            frozenPalette = this;
            SetValue(IsFrozenProperty, true);
        }
        #endregion

        [ThreadStatic]
        private static ThemeResource palette;
        public static ThemeResource Palette
        {
            get
            {
                if (frozenPalette != null)
                    return frozenPalette;
                if (palette == null)
                    palette = new ThemeResource();
                return palette;
            }
        }
        internal ThemeResource() { }

        #region PropertyDependency

        public static readonly DependencyProperty CustomColorProperty = RegisterColor("CustomColor", 0x555444);
        public Color CustomColor
        {
            get { return (Color)GetValue(CustomColorProperty); }
            set { SetValue(CustomColorProperty, value); }
        }
        #endregion

        #region ResourceDictionary
        [ThreadStatic]
        private static ResourceDictionary resourceDictionary;
        internal static ResourceDictionary ResourceDictionary
        {
            get
            {
                if (resourceDictionary == null)
                {
                    resourceDictionary = new ResourceDictionary();

                }
                return resourceDictionary;
            }
        }
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        internal void OnResourcePropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == CustomColorProperty)
                FreezeAndSetResource(ResourceDictionary, ThemeResourceKey.CustomBrush, new SolidColorBrush(Palette.CustomColor));
        }
        private static void InitializeThemeDictionary()
        {
            FreezeAndSetResource(ResourceDictionary, ThemeResourceKey.CustomBrush, new SolidColorBrush(Palette.CustomColor));
        }
        #endregion
    }
}
