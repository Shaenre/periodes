﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Theming.Resources
{
    public static class ThemeResourceKeyProxy
    {
        public static ResourceKey CustomBrush => ThemeResourceKey.CustomBrush;
    }
}
