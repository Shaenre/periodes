﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Periodes.Theming.Resources
{
    public sealed class ThemeResourceExtension : DynamicResourceExtension
    {
        public new ThemeResourceKey ResourceKey
        {
            get => base.ResourceKey as ThemeResourceKey;
            set => base.ResourceKey = value;
        }
        public ThemeResourceExtension() { }
    }
}
