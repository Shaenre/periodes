﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Periodes.Theming.Palettes;
using ID3iCore;

namespace Periodes.Theming
{
    public class ThemeManager : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private Theme theme;
        public Theme Theme
        {
            get => theme;
            set
            {
                if (theme != value)
                {
                    theme = value;
                    OnPropertyChanged(nameof(Theme));
                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => ResourcesManager.LoadResourceDictionaries(Theme))).Wait();
                }
            }
        }
        public static ObservableCollection<Theme> Themes { get; set; } = new ObservableCollection<Theme>();

        static ThemeManager()
        {
            if (Themes.Count == 0)
            {
                Themes.Add(new Theme { Name = "Material", TelerikPalettes = PaletteManager.GetMaterialPalettes(), ApplicationPalettes = PaletteManager.GetApplicationPalettes() });
                Themes.Add(new Theme { Name = "Office2016", TelerikPalettes = PaletteManager.GetOffice2016Palettes(), ApplicationPalettes = PaletteManager.GetApplicationPalettes() });
            }
            Instance = new ThemeManager();
        }
        public static void Initialize() { }
        public static ThemeManager Instance { get; private set; }
        public ThemeManager()
        {
            if (string.IsNullOrEmpty(Preferences.Instance.Theme))
                Theme = Themes.First();
            else
                Theme = Themes.FirstOrDefault(x => x.Name == Preferences.Instance.Theme).ValueOrDefault(x => x, Themes.First());
        }
        ~ThemeManager()
        {
            Preferences.Instance.Theme = Theme.Name;
            Preferences.Instance.Palette = Theme.TelerikPalette.Settings.DisplayName;
            Preferences.Instance.Enregistrer();
        }
    }
}
