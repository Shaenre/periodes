﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class NumPath : SegmentPath
    {
        protected override string Segment => "Num";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal NumPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir le numéro d'une période dans un type en particulier.
        /// </summary>
        /// <param name="settings">Settings créé à partir de
        ///     <see cref="SettingsNumberFactory{T, U}/>
        /// </param>
        /// <returns>
        ///     Le numéro de la période.
        /// </returns>
        public Either<string, RetourApi<CodeApi, int>> Call(SettingsNumber settings) => DoJob<RetourApi<CodeApi, int>>(settings);
    }
}
