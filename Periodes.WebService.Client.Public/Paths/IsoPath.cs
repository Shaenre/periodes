﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class IsoPath : SegmentPath
    {
        protected override string Segment => "ISO";
        internal IsoPath(SegmentPath parent) : base(parent) { }
        public CountryPath Country => new CountryPath(this);
        public StatePath State => new StatePath(this);
        public RegionPath Region => new RegionPath(this);
        public SupportedCountryPath SupportedCountry => new SupportedCountryPath(this);
    }
}
