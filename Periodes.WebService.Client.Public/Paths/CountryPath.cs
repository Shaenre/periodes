﻿using System.Net.Http;
using ID3iCore;
using Periodes.WebService.Models;
using ID3iHttpClient.Paths;

namespace Periodes.WebService.Client.Paths
{
    public class CountryPath : SegmentPath
    {
        protected override string Segment => "Get";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal CountryPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir les informations sur un pays.
        /// </summary>
        /// <param name="value">
        ///     Settings créé à partir de <see cref="SettingsISOFactory"/>
        /// </param>
        /// <returns>
        ///     Le pays et quelques informations.
        /// </returns>
        public Either<string, RetourApi<CodeApi, CountryModel>> Call(SettingsISO value) => DoJob<RetourApi<CodeApi, CountryModel>>(value);
    }
}
