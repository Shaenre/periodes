﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class LongWeekEndPath : SegmentPath
    {
        protected override string Segment => "LongWeekEnds";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal LongWeekEndPath(SegmentPath parent) : base(parent) { }
        public Either<string, RetourApi<CodeApi, List<LongWeekEndModel>>> Call(SettingsHoliday settings) => DoJob<RetourApi<CodeApi, List<LongWeekEndModel>>>(settings);
    }
}
