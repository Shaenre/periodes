﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class SupportedCountryPath : SegmentPath
    {
        protected override string Segment => "All";
        internal SupportedCountryPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir la liste des pays supportés par l'Api.
        /// </summary>
        /// <returns>
        ///     La liste des pays supportés par l'Api.
        /// </returns>
        public Either<string, RetourApi<CodeApi, List<CountryModel>>> Call() => DoJob<RetourApi<CodeApi, List<CountryModel>>>();
    }
}
