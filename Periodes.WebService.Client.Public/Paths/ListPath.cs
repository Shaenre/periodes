﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class ListPath : SegmentPath
    {
        protected override string Segment => "Get";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal ListPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet de récupérer tous les jours fériés pour un pays et d'une division administrative pour une année.
        /// </summary>
        /// <param name="settings">Settings créé à partir de
        ///     <see cref="SettingsPHFactory"/>
        /// </param>
        /// <returns>
        ///     La liste des jours fériés pour un pays en particulier.
        /// </returns>
        public Either<string, RetourApi<CodeApi, List<SpecificDayModel>>> Call(SettingsHoliday settings) => DoJob<RetourApi<CodeApi, List<SpecificDayModel>>>(settings);
    }    
}
