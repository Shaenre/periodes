﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class AvgPath : SegmentPath
    {
        protected override string Segment => "Avg";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal AvgPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir une période moyenne.
        /// </summary>
        /// <param name="settings">Settings créé à partir de
        ///     <see cref="SettingsFactory{T, U, V}/>
        /// </param>
        /// <returns>
        ///     L'objet <see cref="PeriodeModel"/> correspondant.
        /// </returns>
        public Either<string, RetourApi<CodeApi, PeriodeModel>> Call(Settings settings) => DoJob<RetourApi<CodeApi, PeriodeModel>>(settings);
    }
}
