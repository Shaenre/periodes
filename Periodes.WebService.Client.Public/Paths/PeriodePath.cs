﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class PeriodePath : SegmentPath
    {
        protected override string Segment => "Periodes";
        internal PeriodePath(SegmentPath parent) : base(parent) { }
        public CountPath Count => new CountPath(this);
        public EnumeratePath Enumerate => new EnumeratePath(this);
        public NumPath Num => new NumPath(this);
        public GetPath Get => new GetPath(this);
        public AddPath Add => new AddPath(this);
        public AvgPath Avg => new AvgPath(this);
    }
}
