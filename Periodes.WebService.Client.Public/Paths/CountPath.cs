﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class CountPath : SegmentPath
    {
        protected override string Segment => "Count";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal CountPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir le nombre de périodes entre deux dates.
        /// </summary>
        /// <param name="settings">Settings créé à partir de 
        ///     <see cref="SettingsCountFactory{T, U}"/>
        /// </param>
        /// <returns>
        ///     Le nombre de périodes entre les dates.
        /// </returns>
        public Either<string, RetourApi<CodeApi, int>> Call(SettingsCount settings) => DoJob<RetourApi<CodeApi, int>>(settings);
    }
}
