﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class RegionPath : SegmentPath
    {
        protected override string Segment => "Get";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal RegionPath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir les informations sur une région.
        /// </summary>
        /// <param name="value">
        ///     Settings créé à partir de <see cref="SettingsISOFactory"/>
        /// </param>
        /// <returns>
        ///     La région et quelques informations.
        /// </returns>
        public Either<string, RetourApi<CodeApi, RegionModel>> Call(SettingsISO value) => DoJob<RetourApi<CodeApi, RegionModel>>(value);
    }
}
