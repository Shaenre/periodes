﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class SpecificDayPath : SegmentPath
    {
        protected override string Segment => "SpecificDay";
        internal SpecificDayPath(SegmentPath parent) : base(parent) { }
        public ListPath List => new ListPath(this);
        public LongWeekEndPath LongWeekEnds => new LongWeekEndPath(this);
    }
}
