﻿using ID3iHttpClient.Paths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class IndexPath : SegmentPath
    {
        protected override string Segment => "Index";
        public IndexPath(SegmentPath parent) : base(parent) { }
        public HelloPath Hello => new HelloPath(this);
    }
}
