﻿using ID3iCore;
using ID3iHttpClient.Paths;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Client.Paths
{
    public class EnumeratePath : SegmentPath
    {
        protected override string Segment => "Enumerate";
        public override HttpMethod HttpMethod => HttpMethod.Post;
        internal EnumeratePath(SegmentPath parent) : base(parent) { }
        /// <summary>
        /// Méthode qui permet d'obtenir les périodes entre deux dates.
        /// </summary>
        /// <param name="settings">Settings créé à partir de 
        ///     <see cref="SettingsCountFactory{T, U}"/>
        /// </param>
        /// <returns>
        ///     Les périodes entre les dates.
        /// </returns>
        public Either<string, RetourApi<CodeApi, List<PeriodeModel>>> Call(SettingsCount settings) => DoJob<RetourApi<CodeApi, List<PeriodeModel>>>(settings);
    }
}
