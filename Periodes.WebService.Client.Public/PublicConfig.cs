﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core = ID3iHttpClient;

namespace Periodes.WebService.Client
{
    public class PublicConfig : Core::ClientConfiguration
    {
        public static PublicConfig Conf { get; private set; }

        static PublicConfig()
        {
            Conf = (PublicConfig)ConfigurationManager.GetSection(nameof(PublicConfig));
            if (Conf == null || !Conf.ElementInformation.IsPresent)
                throw new Exception($"La section de configuration {nameof(PublicConfig)} n'a pas été trouvée.");
        }
        public static void CheckConsistency() { }
    }
}
