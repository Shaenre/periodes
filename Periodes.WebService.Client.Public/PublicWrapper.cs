﻿using Core = ID3iHttpClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Periodes.WebService.Client.Paths;
using ID3iHttpClient.Config;

namespace Periodes.WebService.Client
{
    public class PublicWrapper : Core::ClientWrapper
    {
        public ClientApiPath Client { get; set; }
        public PublicWrapper() : base() { }

        protected override void OnConfigure()
        {
            PublicConfig.CheckConsistency();
            Client = new ClientApiPath(new Core::Paths.RootPath(this, PublicConfig.Conf.BaseApiUrl));
            Client
                .NonGenericHeader
                    .Add("ID3iPeriodesApp", PublicConfig.Conf.AppCode)
                    .Configure()
                .Accept
                    .Add(DefaultAcceptHeader.Json)
                    .Configure()
                .UserAgent
                    .Add(new UserAgentHeader<ClientApiPath>.UserAgentParameter("Client-ID3i"))
                    .Configure()
                .Authorize
                    .Set(new AuthorizationHeader<ClientApiPath>.AuthorizationParameter("ID3i-Auth", PublicConfig.Conf.ApiKey))
                    .Configure();
        }
    }
}
