﻿using Microsoft.Practices.Unity;
using Prism.Unity;
using ID3iShared.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Authorisation.Sans.ViewModel;
using Periodes.Authorisation.Sans.View;

namespace Periodes.Authorisation.Sans
{
    public class AuthorisationSansModule : Module, IAuthorisationModule
    {
        public AuthorisationSansModule() { }
        public override void Initialize()
        {
            Container.RegisterInstance<IAuthorisationViewModel>(Container.Resolve<AuthorisationSansViewModel>(), new ContainerControlledLifetimeManager());

            Container.RegisterTypeForNavigation<AuthorisationSansView>(Names.ViewName);
        }
    }
}
