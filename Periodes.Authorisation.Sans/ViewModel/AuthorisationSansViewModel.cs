﻿using ID3iCore;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Data.ViewModel;
using Periodes.Authorisation.Data;

namespace Periodes.Authorisation.Sans.ViewModel
{
    public class AuthorisationSansViewModel : AuthorisationViewModelBase
    {
        public override bool IsAutomatic { get; set; } = true;
        public AuthorisationSansViewModel()
        {
            AuthorisationInitializeCommand = new DelegateCommand(() => OnAuthorisationEnded((new UtilisateurProfil() { Utilisateur = new VMUtilisateur() { Login = AuthorisationKey } }).Initialize(x => x.ModulesNames.Add("*"))));
        }
    }
}
