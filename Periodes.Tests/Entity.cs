﻿using Periodes.WebService.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

using static Periodes.WebService.Models.PeriodePosition;
using static Periodes.WebService.Models.PeriodePolicy;
using static Periodes.Gestion.Standard.Data.TypePeriodeReference;
using static Periodes.Gestion.WorkingDays.Data.TypePeriode;

namespace Periodes.Tests
{
    [TestClass]
    public class Entity
    {
        /// <summary>
        /// Récupération de la période de référence (date et type de référence).
        /// </summary>
        [TestMethod]
        public void Entity_GetTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference);
                Assert.AreEqual(new DateTime(2019, 01, 17, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 01, 17, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceGetTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2017, 03, 25), idTypePeriodeReference);
                Assert.AreEqual(new DateTime(2017, 03, 25, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 03, 25, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceGetTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2017, 03, 25), idTypePeriodeReference);
                Assert.AreEqual(null, result);
            }
        }

        /// <summary>
        /// Récupération de la période du type souhaité par rapport à une période de référence (date et type de référence).
        /// </summary>
        [TestMethod]
        public void Entity_GetAnotherTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 01, 01, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 01, 31, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceGetAnotherTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2017, 03, 25), idTypePeriodeReference, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 03, 01, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 03, 31, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceGetAnotherTypeFromType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Get(new DateTime(2017, 03, 25), idTypePeriodeReference, idTypePeriodeSouhaite);
                Assert.AreEqual(null, result);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type de référence.
        /// Dans ce cas le type de décallage est un type plus large que celui de référence.
        /// La période renvoyée est au même index.
        /// </summary>
        [TestMethod]
        public void Entity_AddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2019, 08, 17, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 08, 17, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2017, 10, 25, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 25, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 11, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2017, 10, 11, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 11, 23, 59, 59), result.DATE_FIN);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallé d'un nombre de type de période (offset)
        /// exprimée en type de période souhaité.
        /// Dans ce cas le type de décallage et le type souhaité sont plus large que celui de référence.
        /// La période renvoyé est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Entity_AddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 08, 12, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 08, 18, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 10, 9, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 15, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 10, 9, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 15, 23, 59, 59), result.DATE_FIN);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée dans le type de référence.
        /// Dans ce cas le type de décallage est plus large que celui de référence.
        /// La période renvoyée est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Entity_AddHighAnotherTypeFromTypeResultInTypeAvgIndex()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 31, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2019, 02, 14, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 02, 14, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddHighAnotherTypeFromTypeResultInTypeAvgIndex()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2018, 03, 31, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2018, 04, 15, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2018, 04, 15, 23, 59, 59), result.DATE_FIN);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallé d'un nombre de type de période (offset)
        /// exprimée dans le type souhaité.
        /// Dans ce cas le type de décallage est plus large que celui de référence et le type souhaité est plus fin que celui de référence.
        /// La période renvoyée est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Entity_AddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 08, 16, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 08, 16, 11, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 10, 16, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 16, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 7, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 10, 16, 12, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 10, 16, 23, 59, 59), result.DATE_FIN);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type de référence.
        /// Dans ce cas le type de décallage est plus fin que celui de référence.
        /// La période renvoyée est calculée à partir de la dernière période du type de décallage.
        /// </summary>
        [TestMethod]
        public void Entity_AddLowAnotherTypeFromTypeResultInType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2019, 02, 1, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 02, 28, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddLowAnotherTypeFromTypeResultInType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 04, 30, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceAddLowAnotherTypeFromTypeResultInType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset);
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 04, 30, 23, 59, 59), result.DATE_FIN);
            }
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type souhaité.
        /// Dans ce cas le type de décallage et le type souhaité sont plus fins que le type de référence.
        /// La période renvoyée est calculée à partir de la dernière période du type de décallage exprimée en type souhaité.
        /// </summary>
        [TestMethod]
        public void Entity_AddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2019, 01, 17, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 02, 1, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 02, 1, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayReferenceAddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 04, 1, 23, 59, 59), result.DATE_FIN);
            }
        }
        [TestMethod]
        public void Entity_SaturdayNotReferenceAddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeOffset = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Add(new DateTime(2017, 03, 25, 14, 00, 00), idTypePeriodeReference, 1, idTypePeriodeOffset, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2017, 04, 3, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2017, 04, 3, 23, 59, 59), result.DATE_FIN);
            }
        }

        [TestMethod]
        public void Entity_Avg()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Avg(new DateTime(2019, 08, 17), idTypePeriodeReference, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 08, 15, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 08, 15, 23, 59, 59), result.DATE_FIN);

                idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == YEAR.ToString()).ID_TYPE_PERIODE;
                idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                result = db.Avg(DateTime.Today, idTypePeriodeReference, idTypePeriodeSouhaite);
                Assert.AreEqual(new DateTime(2019, 06, 01, 0, 0, 0), result.DATE_DEBUT);
                Assert.AreEqual(new DateTime(2019, 06, 30, 23, 59, 59), result.DATE_FIN);
            }
        }

        [TestMethod]
        public void Entity_Count_ReferenceType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeCounted = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Count(new DateTime(2017, 03, 25, 14, 00, 00), new DateTime(2017, 10, 25, 12, 0, 0), idTypePeriodeReference, idTypePeriodeCounted);
                Assert.AreEqual(8, result);
            }

            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeCounted = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Count(new DateTime(2017, 06, 12, 00, 00, 00), 
                    new DateTime(2017, 08, 12, 0, 00, 00), 
                    idTypePeriodeReference, idTypePeriodeCounted, FTL, IncludeEndPeriode);
                Assert.AreEqual(92, result);
            }
        }

        [TestMethod]
        public void Entity_Count_SameType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeCounted = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Count(new DateTime(2017, 03, 20, 00, 00, 00), new DateTime(2017, 03, 27, 00, 0, 0), idTypePeriodeReference, idTypePeriodeCounted);
                Assert.AreEqual(8, result);
            }
        }

        [TestMethod]
        public void Entity_Count_SpecType()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeCounted = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Count(new DateTime(2017, 03, 25, 00, 00, 00), new DateTime(2017, 04, 01, 00, 0, 0), idTypePeriodeReference, idTypePeriodeCounted);
                Assert.AreEqual(5, result);
            }
        }

        [TestMethod]
        public void Entity_Num1()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == YEAR.ToString()).ID_TYPE_PERIODE;
                var idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(6, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(23, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(160, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(320, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(110, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(220, result);
            }
        }
        [TestMethod]
        public void Entity_Num2()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(2, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(9, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(18, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(6, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(12, result);
            }
        }
        [TestMethod]
        public void Entity_Num3()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WEEK.ToString()).ID_TYPE_PERIODE;
                var idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(5, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(10, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == WORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(4, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(8, result);
            }
        }
        [TestMethod]
        public void Entity_Num4()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeSouhaite = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFDAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(2, result);

                idTypePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == HALFWORKINGDAY.ToString()).ID_TYPE_PERIODE;
                result = db.Num(new DateTime(2017, 06, 09, 15, 00, 00), idTypePeriode, idTypePeriodeSouhaite);
                Assert.AreEqual(2, result);
            }
        }

        [TestMethod]
        public void Entity_Enumerate()
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                var idTypePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == MONTH.ToString()).ID_TYPE_PERIODE;
                var idTypePeriodeCounted = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == DAY.ToString()).ID_TYPE_PERIODE;
                var result = db.Enumerate(new DateTime(2017, 06, 12, 00, 00, 00),
                    new DateTime(2017, 08, 12, 0, 00, 00),
                    idTypePeriodeReference, idTypePeriodeCounted, FTL, IncludeEndPeriode);
                foreach (var item in result)
                    Console.WriteLine(item.ALIAS);
                Assert.AreEqual(92, result.Count());
            }
        }
    }
}
