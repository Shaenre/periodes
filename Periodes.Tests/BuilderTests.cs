﻿using Periodes.BuilderConfig.Islamic;
using Periodes.BuilderConfig.NorthAmerican;
using Periodes.BuilderConfig.Standard;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Tests
{
    public class BuilderTests
    {
        //https://www.calendar-12.com/week_number
        //https://www.timeanddate.com/date/week-numbers.html
        //https://weekcalendar.zendesk.com/hc/en-us/articles/204826391-Week-Numbering
        //https://en.wikipedia.org/wiki/Week
        private static IEnumerable NorthAmerican_DateValues()
        {
            yield return new TestCaseData(2000, new DateTime(1999, 12, 26), new DateTime(2000, 12, 30));
            yield return new TestCaseData(2001, new DateTime(2000, 12, 31), new DateTime(2001, 12, 29));
            yield return new TestCaseData(2002, new DateTime(2001, 12, 30), new DateTime(2002, 12, 28));
            yield return new TestCaseData(2003, new DateTime(2002, 12, 29), new DateTime(2003, 12, 27));
            yield return new TestCaseData(2004, new DateTime(2003, 12, 28), new DateTime(2004, 12, 25));
            yield return new TestCaseData(2005, new DateTime(2004, 12, 26), new DateTime(2005, 12, 31));
            yield return new TestCaseData(2006, new DateTime(2006, 01, 01), new DateTime(2006, 12, 30));
            yield return new TestCaseData(2007, new DateTime(2006, 12, 31), new DateTime(2007, 12, 29));
            yield return new TestCaseData(2008, new DateTime(2007, 12, 30), new DateTime(2008, 12, 27));
            yield return new TestCaseData(2009, new DateTime(2008, 12, 28), new DateTime(2009, 12, 26));
            yield return new TestCaseData(2010, new DateTime(2009, 12, 27), new DateTime(2010, 12, 25));
            yield return new TestCaseData(2011, new DateTime(2010, 12, 26), new DateTime(2011, 12, 31));
            yield return new TestCaseData(2012, new DateTime(2012, 01, 01), new DateTime(2012, 12, 29));
            yield return new TestCaseData(2013, new DateTime(2012, 12, 30), new DateTime(2013, 12, 28));
            yield return new TestCaseData(2014, new DateTime(2013, 12, 29), new DateTime(2014, 12, 27));
            yield return new TestCaseData(2015, new DateTime(2014, 12, 28), new DateTime(2015, 12, 26));
            yield return new TestCaseData(2016, new DateTime(2015, 12, 27), new DateTime(2016, 12, 31));
            yield return new TestCaseData(2017, new DateTime(2017, 01, 01), new DateTime(2017, 12, 30));
            yield return new TestCaseData(2018, new DateTime(2017, 12, 31), new DateTime(2018, 12, 29));
            yield return new TestCaseData(2019, new DateTime(2018, 12, 30), new DateTime(2019, 12, 28));
            yield return new TestCaseData(2020, new DateTime(2019, 12, 29), new DateTime(2020, 12, 26));
            yield return new TestCaseData(2021, new DateTime(2020, 12, 27), new DateTime(2021, 12, 25));
            yield return new TestCaseData(2022, new DateTime(2021, 12, 26), new DateTime(2022, 12, 31));
            yield return new TestCaseData(2023, new DateTime(2023, 01, 01), new DateTime(2023, 12, 30));
            yield return new TestCaseData(2024, new DateTime(2023, 12, 31), new DateTime(2024, 12, 28));
            yield return new TestCaseData(2025, new DateTime(2024, 12, 29), new DateTime(2025, 12, 27));
            yield return new TestCaseData(2026, new DateTime(2025, 12, 28), new DateTime(2026, 12, 26));
            yield return new TestCaseData(2027, new DateTime(2026, 12, 27), new DateTime(2027, 12, 25));
            yield return new TestCaseData(2028, new DateTime(2027, 12, 26), new DateTime(2028, 12, 30));
            yield return new TestCaseData(2029, new DateTime(2028, 12, 31), new DateTime(2029, 12, 29));
            yield return new TestCaseData(2030, new DateTime(2029, 12, 30), new DateTime(2030, 12, 28));
        }
        private static IEnumerable ISO_DateValues()
        {
            yield return new TestCaseData(2000, new DateTime(2000, 01, 03), new DateTime(2000, 12, 31));
            yield return new TestCaseData(2001, new DateTime(2001, 01, 01), new DateTime(2001, 12, 30));
            yield return new TestCaseData(2002, new DateTime(2001, 12, 31), new DateTime(2002, 12, 29));
            yield return new TestCaseData(2003, new DateTime(2002, 12, 30), new DateTime(2003, 12, 28));
            yield return new TestCaseData(2004, new DateTime(2003, 12, 29), new DateTime(2005, 01, 02));
            yield return new TestCaseData(2005, new DateTime(2005, 01, 03), new DateTime(2006, 01, 01));
            yield return new TestCaseData(2006, new DateTime(2006, 01, 02), new DateTime(2006, 12, 31));
            yield return new TestCaseData(2007, new DateTime(2007, 01, 01), new DateTime(2007, 12, 30));
            yield return new TestCaseData(2008, new DateTime(2007, 12, 31), new DateTime(2008, 12, 28));
            yield return new TestCaseData(2009, new DateTime(2008, 12, 29), new DateTime(2010, 01, 03));
            yield return new TestCaseData(2010, new DateTime(2010, 01, 04), new DateTime(2011, 01, 02));
            yield return new TestCaseData(2011, new DateTime(2011, 01, 03), new DateTime(2012, 01, 01));
            yield return new TestCaseData(2012, new DateTime(2012, 01, 02), new DateTime(2012, 12, 30));
            yield return new TestCaseData(2013, new DateTime(2012, 12, 31), new DateTime(2013, 12, 29));
            yield return new TestCaseData(2014, new DateTime(2013, 12, 30), new DateTime(2014, 12, 28));
            yield return new TestCaseData(2015, new DateTime(2014, 12, 29), new DateTime(2016, 01, 03));
            yield return new TestCaseData(2016, new DateTime(2016, 01, 04), new DateTime(2017, 01, 01));
            yield return new TestCaseData(2017, new DateTime(2017, 01, 02), new DateTime(2017, 12, 31));
            yield return new TestCaseData(2018, new DateTime(2018, 01, 01), new DateTime(2018, 12, 30));
            yield return new TestCaseData(2019, new DateTime(2018, 12, 31), new DateTime(2019, 12, 29));
            yield return new TestCaseData(2020, new DateTime(2019, 12, 30), new DateTime(2021, 01, 03));
            yield return new TestCaseData(2021, new DateTime(2021, 01, 04), new DateTime(2022, 01, 02));
            yield return new TestCaseData(2022, new DateTime(2022, 01, 03), new DateTime(2023, 01, 01));
            yield return new TestCaseData(2023, new DateTime(2023, 01, 02), new DateTime(2023, 12, 31));
            yield return new TestCaseData(2024, new DateTime(2024, 01, 01), new DateTime(2024, 12, 29));
            yield return new TestCaseData(2025, new DateTime(2024, 12, 30), new DateTime(2025, 12, 28));
            yield return new TestCaseData(2026, new DateTime(2025, 12, 29), new DateTime(2027, 01, 03));
            yield return new TestCaseData(2027, new DateTime(2027, 01, 04), new DateTime(2028, 01, 02));
            yield return new TestCaseData(2028, new DateTime(2028, 01, 03), new DateTime(2028, 12, 31));
            yield return new TestCaseData(2029, new DateTime(2029, 01, 01), new DateTime(2029, 12, 30));
            yield return new TestCaseData(2030, new DateTime(2029, 12, 31), new DateTime(2030, 12, 29));
        }
        private static IEnumerable Islamic_DateValues()
        {
            yield return new TestCaseData(2000, new DateTime(2000, 01, 01), new DateTime(2000, 12, 29));
            yield return new TestCaseData(2001, new DateTime(2000, 12, 30), new DateTime(2001, 12, 28));
            yield return new TestCaseData(2002, new DateTime(2001, 12, 29), new DateTime(2002, 12, 27));
            yield return new TestCaseData(2003, new DateTime(2002, 12, 28), new DateTime(2003, 12, 26));
            yield return new TestCaseData(2004, new DateTime(2003, 12, 27), new DateTime(2004, 12, 31));
            yield return new TestCaseData(2005, new DateTime(2005, 01, 01), new DateTime(2005, 12, 30));
            yield return new TestCaseData(2006, new DateTime(2005, 12, 31), new DateTime(2006, 12, 29));
            yield return new TestCaseData(2007, new DateTime(2006, 12, 30), new DateTime(2007, 12, 28));
            yield return new TestCaseData(2008, new DateTime(2007, 12, 29), new DateTime(2008, 12, 26));
            yield return new TestCaseData(2009, new DateTime(2008, 12, 27), new DateTime(2009, 12, 25));
            yield return new TestCaseData(2010, new DateTime(2009, 12, 26), new DateTime(2010, 12, 31));
            yield return new TestCaseData(2011, new DateTime(2011, 01, 01), new DateTime(2011, 12, 30));
            yield return new TestCaseData(2012, new DateTime(2011, 12, 31), new DateTime(2012, 12, 28));
            yield return new TestCaseData(2013, new DateTime(2012, 12, 29), new DateTime(2013, 12, 27));
            yield return new TestCaseData(2014, new DateTime(2013, 12, 28), new DateTime(2014, 12, 26));
            yield return new TestCaseData(2015, new DateTime(2014, 12, 27), new DateTime(2015, 12, 25));
            yield return new TestCaseData(2016, new DateTime(2015, 12, 26), new DateTime(2016, 12, 30));
            yield return new TestCaseData(2017, new DateTime(2016, 12, 31), new DateTime(2017, 12, 29));
            yield return new TestCaseData(2018, new DateTime(2017, 12, 30), new DateTime(2018, 12, 28));
            yield return new TestCaseData(2019, new DateTime(2018, 12, 29), new DateTime(2019, 12, 27));
            yield return new TestCaseData(2020, new DateTime(2019, 12, 28), new DateTime(2020, 12, 25));
            yield return new TestCaseData(2021, new DateTime(2020, 12, 26), new DateTime(2021, 12, 31));
            yield return new TestCaseData(2022, new DateTime(2022, 01, 01), new DateTime(2022, 12, 30));
            yield return new TestCaseData(2023, new DateTime(2022, 12, 31), new DateTime(2023, 12, 29));
            yield return new TestCaseData(2024, new DateTime(2023, 12, 30), new DateTime(2024, 12, 27));
            yield return new TestCaseData(2025, new DateTime(2024, 12, 28), new DateTime(2025, 12, 26));
            yield return new TestCaseData(2026, new DateTime(2025, 12, 27), new DateTime(2026, 12, 25));
            yield return new TestCaseData(2027, new DateTime(2026, 12, 26), new DateTime(2027, 12, 31));
            yield return new TestCaseData(2028, new DateTime(2028, 01, 01), new DateTime(2028, 12, 29));
            yield return new TestCaseData(2029, new DateTime(2028, 12, 30), new DateTime(2029, 12, 28));
            yield return new TestCaseData(2030, new DateTime(2029, 12, 29), new DateTime(2030, 12, 27));
        }

        [TestCaseSource(typeof(BuilderTests), nameof(NorthAmerican_DateValues))]
        public void NorthAmerican_FirstAndLastDayOfYear(int year, DateTime firstDayOfYear, DateTime lastDayOfYear)
        {
            var builder = new BuilderConfigNorthAmerican();

            var result = builder.FirstDayOfYear(year);
            Assert.AreEqual(firstDayOfYear, result);
            result = builder.LastDayOfYear(year);
            Assert.AreEqual(lastDayOfYear, result);
        }

        [TestCaseSource(typeof(BuilderTests), nameof(ISO_DateValues))]
        public void ISO_FirstAndLastDayOfYear(int year, DateTime firstDayOfYear, DateTime lastDayOfYear)
        {
            var builder = new BuilderConfigStandard();

            var result = builder.FirstDayOfYear(year);
            Assert.AreEqual(firstDayOfYear, result);
            result = builder.LastDayOfYear(year);
            Assert.AreEqual(lastDayOfYear, result);
        }

        [TestCaseSource(typeof(BuilderTests), nameof(Islamic_DateValues))]
        public void Islamic_FirstAndLastDayOfYear(int year, DateTime firstDayOfYear, DateTime lastDayOfYear)
        {
            var builder = new BuilderConfigIslamic();

            var result = builder.FirstDayOfYear(year);
            Assert.AreEqual(firstDayOfYear, result);
            result = builder.LastDayOfYear(year);
            Assert.AreEqual(lastDayOfYear, result);
        }
    }
}
