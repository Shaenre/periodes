﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace Periodes.Tests
{
    [TestClass]
    public class DateTimeTests
    {
        [TestMethod]
        public void DateTime_Ago_FromFixeDateTime()
        {
            var originalPointInTime = new DateTime(2016, 12, 31, 17, 0, 0, 0, DateTimeKind.Local);
            foreach (var agoValue in new List<int>() { 1, 32, 40, 100, 1000, -1, -100, 0 })
            {
                Assert.AreEqual(agoValue.Years().Before(originalPointInTime), originalPointInTime.AddYears(-agoValue));
                Assert.AreEqual(agoValue.Months().Before(originalPointInTime), originalPointInTime.AddMonths(-agoValue));
                Assert.AreEqual(agoValue.Weeks().Before(originalPointInTime), originalPointInTime.AddDays(-agoValue * 7));
                Assert.AreEqual(agoValue.Days().Before(originalPointInTime), originalPointInTime.AddDays(-agoValue));

                Assert.AreEqual(agoValue.Hours().Before(originalPointInTime), originalPointInTime.AddHours(-agoValue));
                Assert.AreEqual(agoValue.Minutes().Before(originalPointInTime), originalPointInTime.AddMinutes(-agoValue));
                Assert.AreEqual(agoValue.Seconds().Before(originalPointInTime), originalPointInTime.AddSeconds(-agoValue));
                Assert.AreEqual(agoValue.Milliseconds().Before(originalPointInTime), originalPointInTime.AddMilliseconds(-agoValue));
                Assert.AreEqual(agoValue.Ticks().Before(originalPointInTime), originalPointInTime.AddTicks(-agoValue));
            }
        }
        [TestMethod]
        public void DateTime_Ago_FromMonth()
        {
            var originalPointInTime = new DateTime(2016, 4, 30, 0, 0, 0, DateTimeKind.Local);

            Assert.AreEqual(1.Months().Before(originalPointInTime), new DateTime(2016, 3, 30, 0, 0, 0, DateTimeKind.Local));
            Assert.AreEqual(1.Months().After(originalPointInTime), new DateTime(2016, 5, 30, 0, 0, 0, DateTimeKind.Local));
        }
        [TestMethod]
        public void DateTime_Ago_FromLeapYear()
        {
            var originalPointInTime = new DateTime(2016, 2, 29, 0, 0, 0, DateTimeKind.Local);

            Assert.AreEqual(1.Years().Before(originalPointInTime), new DateTime(2015, 2, 28, 0, 0, 0, DateTimeKind.Local));
            Assert.AreEqual(1.Years().After(originalPointInTime), new DateTime(2017, 2, 28, 0, 0, 0, DateTimeKind.Local));
        }
        [TestMethod]
        public void DateTime_From_FixeDateTime()
        {
            var originalPointInTime = new DateTime(2016, 12, 31, 17, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 1, 32, 40, 100, 1000, -1, -100, 0 })
            {
                Assert.AreEqual(value.Years().After(originalPointInTime), originalPointInTime.AddYears(value));
                Assert.AreEqual(value.Months().After(originalPointInTime), originalPointInTime.AddMonths(value));
                Assert.AreEqual(value.Weeks().After(originalPointInTime), originalPointInTime.AddDays(value * 7));
                Assert.AreEqual(value.Days().After(originalPointInTime), originalPointInTime.AddDays(value));

                Assert.AreEqual(value.Hours().After(originalPointInTime), originalPointInTime.AddHours(value));
                Assert.AreEqual(value.Minutes().After(originalPointInTime), originalPointInTime.AddMinutes(value));
                Assert.AreEqual(value.Seconds().After(originalPointInTime), originalPointInTime.AddSeconds(value));
                Assert.AreEqual(value.Milliseconds().After(originalPointInTime), originalPointInTime.AddMilliseconds(value));
                Assert.AreEqual(value.Ticks().After(originalPointInTime), originalPointInTime.AddTicks(value));
            }
        }
        [TestMethod]
        public void DateTime_ChangeTime_Hour()
        {
            var toChange = new DateTime(2016, 10, 25, 0, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 24, -1, 0, 1, 23 })
            {
                try
                {
                    var expected = new DateTime(2016, 10, 25, value, 0, 0, 0, DateTimeKind.Local);
                    Assert.AreEqual(expected, toChange.SetTime(value));
                }
                catch (Exception ex) when (ex is ArgumentOutOfRangeException && (value == 24 || value == -1))
                {
                    Assert.IsTrue(true);
                }
            }
        }
        [TestMethod]
        public void DateTime_ChangeTime_Minutes()
        {
            var toChange = new DateTime(2016, 10, 25, 0, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 0, 16, 59, -1, 60 })
            {
                try
                {
                    var expected = new DateTime(2016, 10, 25, 0, value, 0, 0, DateTimeKind.Local);
                    Assert.AreEqual(expected, toChange.SetTime(0, value));
                }
                catch (Exception ex) when (ex is ArgumentOutOfRangeException && (value == 60 || value == -1))
                {
                    Assert.IsTrue(true);
                }
            }
        }
        [TestMethod]
        public void DateTime_ChangeTime_Secondes()
        {
            var toChange = new DateTime(2016, 10, 25, 0, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 0, 16, 59, -1, 60 })
            {
                try
                {
                    var expected = new DateTime(2016, 10, 25, 0, 0, value, 0, DateTimeKind.Local);
                    Assert.AreEqual(expected, toChange.SetTime(0, 0, value));
                }
                catch (Exception ex) when (ex is ArgumentOutOfRangeException && (value == 60 || value == -1))
                {
                    Assert.IsTrue(true);
                }
            }
        }
        [TestMethod]
        public void DateTime_ChangeTime_Millisecondes()
        {
            var toChange = new DateTime(2016, 10, 25, 0, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 0, 100, 999, -1, 1000 })
            {
                try
                {
                    var expected = new DateTime(2016, 10, 25, 0, 0, 0, value, DateTimeKind.Local);
                    Assert.AreEqual(expected, toChange.SetTime(0, 0, 0, value));
                }
                catch (Exception ex) when (ex is ArgumentOutOfRangeException && (value == 1000 || value == -1))
                {
                    Assert.IsTrue(true);
                }
            }
        }
        [TestMethod]
        public void DateTime_TimeZoneTests()
        {
            for (var i = -11; i <= 12; i++)
            {
                var beginningOfDayUtc = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var actualDayStart = beginningOfDayUtc.BeginningOfDay(i);
                var actualDayEnd = beginningOfDayUtc.EndOfDay(i);
                var expectedDayStart = beginningOfDayUtc.AddHours(i);
                var expectedDayEnd = beginningOfDayUtc.SetHour(23).SetMinute(59).SetSecond(59).SetMillisecond(999).AddHours(i);
                Assert.AreEqual(expectedDayStart, actualDayStart);
                Assert.AreEqual(expectedDayEnd, actualDayEnd);
            }
        }
        [TestMethod]
        public void DateTime_Divers()
        {
            var now = DateTime.Now;
            Assert.AreEqual(new DateTime(now.Year, now.Month, now.Day, 23, 59, 59, 999, DateTimeKind.Local), DateTime.Now.EndOfDay());
            Assert.AreEqual(new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, 0, DateTimeKind.Local), DateTime.Now.BeginningOfDay());

            var firstBirthDay = new DateTime(1988, 7, 8, 17, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(firstBirthDay + new TimeSpan(1, 0, 5, 0, 0), firstBirthDay + 1.Days() + 5.Minutes());

            Assert.AreEqual(now + TimeSpan.FromDays(1), now.NextDay());
            Assert.AreEqual(now - TimeSpan.FromDays(1), now.PreviousDay());

            Assert.AreEqual(now + TimeSpan.FromDays(7), now.WeekAfter());
            Assert.AreEqual(now - TimeSpan.FromDays(7), now.WeekEarlier());

            Assert.AreEqual(new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2015, 12, 31, 0, 0, 0, DateTimeKind.Local).Add(1.Days()));
            Assert.AreEqual(new DateTime(2016, 1, 2, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Local).Add(1.Days()));

            var sampleDate = new DateTime(2016, 1, 1, 13, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(new DateTime(2016, 1, 1, 12, 0, 0, 0, DateTimeKind.Local), sampleDate.Noon());
            Assert.AreEqual(new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Local), sampleDate.Midnight());

            Assert.AreEqual(3.Days() + 3.Days(), 6.Days());
            Assert.AreEqual(102.Days() - 3.Days(), 99.Days());

            Assert.AreEqual(24.Hours(), 1.Days());

            sampleDate = new DateTime(2016, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(3.Days().Since(sampleDate), sampleDate + 3.Days());

            var saturday = new DateTime(2016, 10, 29, 12, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(new DateTime(2016, 11, 5, 12, 0, 0, DateTimeKind.Local), saturday.Next(DayOfWeek.Saturday));

            Assert.AreEqual(new DateTime(2016, 10, 22, 12, 0, 0, DateTimeKind.Local), saturday.Previous(DayOfWeek.Saturday));
        }
        [TestMethod]
        public void DateTime_NextYear()
        {
            var birthday = new DateTime(1988, 7, 8, 17, 0, 0, 0, DateTimeKind.Local);
            var nextYear = birthday.NextYear();
            var expected = new DateTime(1989, 7, 8, 17, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(expected, nextYear);
        }
        [TestMethod]
        public void DateTime_PreviousYear()
        {
            var birthday = new DateTime(1988, 7, 8, 17, 0, 0, 0, DateTimeKind.Local);
            var previousYear = birthday.PreviousYear();
            var expected = new DateTime(1987, 7, 8, 17, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(expected, previousYear);
        }
        [TestMethod]
        public void DateTime_NextYear_WithLeapYear()
        {
            var someBirthday = new DateTime(2016, 2, 29, 17, 0, 0, 0, DateTimeKind.Local);
            var nextYear = someBirthday.NextYear();
            var expected = new DateTime(2017, 2, 28, 17, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(expected, nextYear);
        }
        [TestMethod]
        public void DateTime_PreviousYear_WithLeapYear()
        {
            var someBirthday = new DateTime(2012, 2, 29, 17, 0, 0, 0, DateTimeKind.Local);
            var previousYear = someBirthday.PreviousYear();
            var expected = new DateTime(2011, 2, 28, 17, 0, 0, 0, DateTimeKind.Local);
            Assert.AreEqual(expected, previousYear);
        }
        [TestMethod]
        public void DateTime_Next()
        {
            var friday = new DateTime(2016, 12, 9, 1, 0, 0, 0, DateTimeKind.Local);
            var reallyNextFriday = new DateTime(2016, 12, 16, 1, 0, 0, 0, DateTimeKind.Local);
            var nextFriday = friday.Next(DayOfWeek.Friday);
            Assert.AreEqual(reallyNextFriday, nextFriday);
        }
        [TestMethod]
        public void DateTime_Previous()
        {
            var friday = new DateTime(2016, 12, 9, 1, 0, 0, 0, DateTimeKind.Local);
            var reallyPreviousFriday = new DateTime(2016, 12, 2, 1, 0, 0, 0, DateTimeKind.Local);
            var previousFriday = friday.Previous(DayOfWeek.Friday);
            Assert.AreEqual(reallyPreviousFriday, previousFriday);
        }
        [TestMethod]
        public void DateTime_IsBefore()
        {
            var toCompareWith = DateTime.Today + 1.Days();
            Assert.IsTrue(DateTime.Today.IsBefore(toCompareWith));

            toCompareWith = DateTime.Today;
            Assert.IsFalse(toCompareWith.IsBefore(toCompareWith));
        }
        [TestMethod]
        public void DateTime_IsAfter()
        {
            var toCompareWith = DateTime.Today - 1.Days();
            Assert.IsTrue(DateTime.Today.IsAfter(toCompareWith));

            toCompareWith = DateTime.Today;
            Assert.IsFalse(toCompareWith.IsAfter(toCompareWith));
        }
        [TestMethod]
        public void DateTime_At_SetsHourAndMinutes()
        {
            var expected = new DateTime(2016, 12, 8, 18, 06, 01, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 12, 8, 17, 05, 01, DateTimeKind.Local).At(18, 06));
        }
        [TestMethod]
        public void DateTime_At_SetsHourAndMinutesAndSeconds()
        {
            var expected = new DateTime(2016, 12, 8, 18, 06, 02, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 12, 8, 17, 05, 01, DateTimeKind.Local).At(18, 06, 02));
        }
        [TestMethod]
        public void DateTime_At_SetsHourAndMinutesSecondsAndMilliseconds()
        {
            var expected = new DateTime(2016, 12, 8, 18, 06, 02, 03, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 12, 8, 17, 05, 01, DateTimeKind.Local).At(18, 06, 02, 03));
        }
        [TestMethod]
        public void DateTime_FirstDayOfMonth_SetsTheDayToOneInThatMonth()
        {
            var expected = new DateTime(2016, 12, 1, 17, 05, 01, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 12, 8, 17, 05, 01, DateTimeKind.Local).FirstDayOfMonth());
        }
        [TestMethod]
        public void DateTime_LastDayOfMonth_SetsTheDayToLastDayInThatMonth()
        {
            var expected = new DateTime(2016, 1, 31, 23, 59, 59, 999);
            Assert.AreEqual(expected, new DateTime(2016, 1, 1, 23, 59, 59, 999).LastDayOfMonth());
        }
        [TestMethod]
        public void DateTime_IsInFuture()
        {
            var now = DateTime.Now;
            Assert.IsFalse(now.Subtract(2.Seconds()).IsInFuture());
            Assert.IsFalse(now.IsInFuture());
            Assert.IsTrue(now.Add(2.Seconds()).IsInFuture());
        }
        [TestMethod]
        public void DateTime_IsInPast()
        {
            var now = DateTime.Now;
            Assert.IsTrue(now.Subtract(2.Seconds()).IsInPast());
            Assert.IsFalse(now.Add(2.Seconds()).IsInPast());
        }
        [TestMethod]
        public void DateTime_FirstDayOfWeek_FirstDayOfWeekIsMonday()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var currentCulture = new CultureInfo("en-AU") { DateTimeFormat = { FirstDayOfWeek = DayOfWeek.Monday } };
            Thread.CurrentThread.CurrentCulture = currentCulture;
            var expected = new DateTime(2016, 12, 5, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 5, 6, 7, 8, 9, 10, 11 })
            {
                Assert.AreEqual(expected, new DateTime(2016, 12, value, 0, 0, 0, DateTimeKind.Local).FirstDayOfWeek());
            }
            Thread.CurrentThread.CurrentCulture = ci;
        }
        [TestMethod]
        public void DateTime_FirstDayOfWeek_FirstDayOfWeekIsSunday()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var currentCulture = new CultureInfo("en-AU") { DateTimeFormat = { FirstDayOfWeek = DayOfWeek.Sunday } };
            Thread.CurrentThread.CurrentCulture = currentCulture;
            var expected = new DateTime(2016, 12, 4, 0, 0, 0, DateTimeKind.Local);
            foreach (var value in new List<int>() { 4, 5, 6, 7, 8, 9, 10 })
            {
                Assert.AreEqual(expected, new DateTime(2016, 12, value, 0, 0, 0, DateTimeKind.Local).FirstDayOfWeek());
            }
            Thread.CurrentThread.CurrentCulture = ci;
        }
        [TestMethod]
        public void DateTime_FirstDayOfYear()
        {
            foreach (var value in new List<string>() { "2016-06-22T06:40:20.005", "2016-12-31T06:40:20.005", "2016-01-01T06:40:20.005" })
            {
                Assert.AreEqual(new DateTime(2016, 1, 1), DateTime.Parse(value).FirstDayOfYear());
            }
        }
        [TestMethod]
        public void DateTime_LastDayOfWeek()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var currentCulture = new CultureInfo("en-AU") { DateTimeFormat = { FirstDayOfWeek = DayOfWeek.Monday } };
            Thread.CurrentThread.CurrentCulture = currentCulture;
            foreach (var value in new List<string>() { "2016-12-05T06:40:20.005", "2016-12-06T06:40:20.005", "2016-12-11T06:40:20.005" })
            {
                Assert.AreEqual(new DateTime(2016, 12, 11, 06, 40, 20, 005), DateTime.Parse(value).LastDayOfWeek());
            }
            Thread.CurrentThread.CurrentCulture = ci;
        }
        [TestMethod]
        public void DateTime_LastDayOfYear()
        {
            foreach (var value in new List<string>() { "2016-02-13T06:40:20.005", "2016-01-01T06:40:20.005", "2016-12-31T06:40:20.005" })
            {
                Assert.AreEqual(new DateTime(2016, 12, 31), DateTime.Parse(value).LastDayOfYear());
            }
        }
        [TestMethod]
        public void DateTime_PreviousMonth()
        {
            var expected = new DateTime(2016, 12, 20, 06, 40, 20, 5, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2017, 1, 20, 06, 40, 20, 5, DateTimeKind.Local).PreviousMonth());
        }
        [TestMethod]
        public void DateTime_PreviousMonth_PreviousMonthDoesntHaveThatManyDays()
        {
            var expected = new DateTime(2016, 2, 29, 06, 40, 20, 5, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 3, 31, 06, 40, 20, 5, DateTimeKind.Local).PreviousMonth());
        }
        [TestMethod]
        public void DateTime_NextMonth()
        {
            var expected = new DateTime(2017, 1, 5, 06, 40, 20, 5, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 12, 5, 06, 40, 20, 5, DateTimeKind.Local).NextMonth());
        }
        [TestMethod]
        public void DateTime_PreviousMonth_NextMonthDoesntHaveThatManyDays()
        {
            var expected = new DateTime(2016, 2, 29, 06, 40, 20, 5, DateTimeKind.Local);
            Assert.AreEqual(expected, new DateTime(2016, 1, 31, 06, 40, 20, 5, DateTimeKind.Local).NextMonth());
        }
        [TestMethod]
        public void DateTime_SameYear()
        {
            foreach (var dateValue in new List<string>() { "2016-11-25T00:00:00.000", "2016-12-25T00:00:00.000", "2016-10-25T00:00:00.000" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsTrue(date.SameYear(other));
            }
            foreach (var dateValue in new List<string>() { "2015-11-25T00:00:00.000", "2014-11-25T00:00:00.000", "2013-11-25T00:00:00.000" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsFalse(date.SameYear(other));
            }
        }
        [TestMethod]
        public void DateTime_SameMonth()
        {
            foreach (var dateValue in new List<string>() { "2016-11-25T00:00:00.000", "2016-11-01T00:00:00.000", "2016-11-15T00:00:00.000" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsTrue(date.SameMonth(other));
            }
            foreach (var dateValue in new List<string>() { "2015-11-25T00:00:00.000", "2014-11-01T12:00:00.000", "2013-10-15T18:00:00.000" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsFalse(date.SameMonth(other));
            }
        }
        [TestMethod]
        public void DateTime_SameDay()
        {
            foreach (var dateValue in new List<string>() { "2016-11-25T12:25:00.000", "2016-11-25T23:59:59.999" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsTrue(date.SameDay(other));
            }
            foreach (var dateValue in new List<string>() { "2015-11-25T12:25:00.000", "2014-12-25T23:59:59.999", "2013-10-25T23:59:59.999" })
            {
                var other = DateTime.Parse(dateValue);
                var date = new DateTime(2016, 11, 25);
                Assert.IsFalse(date.SameDay(other));
            }
        }
        [TestMethod]
        public void DateTime_NextOrThis()
        {
            var wednesday = (new DateTime(2017, 3, 1)).NextOrThis(DayOfWeek.Wednesday);
            var reallyWednesday = new DateTime(2017, 3, 1);
            Assert.AreEqual(reallyWednesday, wednesday);

            var thursday = (new DateTime(2017, 3, 1)).NextOrThis(DayOfWeek.Thursday);
            var reallyThursday = new DateTime(2017, 3, 2);
            Assert.AreEqual(reallyThursday, thursday);
        }
        [TestMethod]
        public void DateTime_PreviousOrThis()
        {
            var wednesday = (new DateTime(2017, 3, 1)).PreviousOrThis(DayOfWeek.Wednesday);
            var reallyWednesday = new DateTime(2017, 3, 1);
            Assert.AreEqual(reallyWednesday, wednesday);

            var thursday = (new DateTime(2017, 3, 3)).PreviousOrThis(DayOfWeek.Thursday);
            var reallyThursday = new DateTime(2017, 3, 2);
            Assert.AreEqual(reallyThursday, thursday);
        }
        [TestMethod]
        public void DateTime_WeekAfter()
        {
            var wednesday = (new DateTime(2017, 3, 1)).WeekAfter(1);
            var reallyWednesday = new DateTime(2017, 3, 8);
            Assert.AreEqual(reallyWednesday, wednesday);
            Assert.AreEqual(reallyWednesday.DayOfWeek, wednesday.DayOfWeek);
        }
        [TestMethod]
        public void DateTime_WeekEarlier()
        {
            var wednesday = (new DateTime(2017, 3, 8)).WeekEarlier(1);
            var reallyWednesday = new DateTime(2017, 3, 1);
            Assert.AreEqual(reallyWednesday, wednesday);
            Assert.AreEqual(reallyWednesday.DayOfWeek, wednesday.DayOfWeek);
        }
    }
}
