﻿using ID3iPeriodes.Common.Holidays;
using ID3iPeriodes.Common.Holidays.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace ID3iPeriodes.Tests
{
    //https://www.timeanddate.com/holidays/
    [TestClass]
    public class PublicHolidays
    {
        [TestMethod]
        public void ISO3166_Divers()
        {
            var nb = Enum.GetNames(typeof(CountryCode)).Length;
            Assert.AreEqual(nb, Countries.List.Count);
        }
        [TestMethod]
        public void ISO3166_Name()
        {
            var result = Countries.ByName("france");
            Assert.IsNotNull(result);
            result = Countries.ByName("yolooooooooooo");
            Assert.IsNull(result);
        }
        [TestMethod]
        public void ISO3166_Alpha2()
        {
            var result = Countries.ByAlpha2("fr");
            Assert.IsNotNull(result);
            result = Countries.ByAlpha2("ZZ");
            Assert.IsNull(result);

            result = Countries.ByAlpha2(CountryCode.FR);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void ISO3166_Alpha3()
        {
            var result = Countries.ByAlpha3("fra");
            Assert.IsNotNull(result);
            result = Countries.ByAlpha3("ZZZ");
            Assert.IsNull(result);
        }
        [TestMethod]
        public void ISO3166_Number()
        {
            var result = Countries.ByNumber(250);
            Assert.IsNotNull(result);
            result = Countries.ByNumber(-666);
            Assert.IsNull(result);

        }

        [TestMethod]
        public void PublicHoliday_DimanchePaques()
        {
            var dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(1900);
            Assert.AreEqual(new DateTime(1900, 4, 15), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2014);
            Assert.AreEqual(new DateTime(2014, 4, 20), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2015);
            Assert.AreEqual(new DateTime(2015, 4, 5), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2016);
            Assert.AreEqual(new DateTime(2016, 3, 27), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2017);
            Assert.AreEqual(new DateTime(2017, 4, 16), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2018);
            Assert.AreEqual(new DateTime(2018, 4, 1), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2019);
            Assert.AreEqual(new DateTime(2019, 4, 21), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2020);
            Assert.AreEqual(new DateTime(2020, 4, 12), dimanchePaques);

            dimanchePaques = CatholiqueBaseProvider.GetEasterSunday(2200);
            Assert.AreEqual(new DateTime(2200, 4, 6), dimanchePaques);
        }

        [TestMethod]
        public void PublicHoliday_AT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 26), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.AT);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_AU_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 26), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 25), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.AU);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_AU_Spec()
        {
            //1er lundi de mars.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 3, 6), CountryCode.AU, AustraliaProvider.AD.WA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 5), CountryCode.AU, AustraliaProvider.AD.WA);
            Assert.AreEqual(true, isPublicHoliday);

            //2ème lundi de mars.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 3, 13), CountryCode.AU, AustraliaProvider.AD.ACT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 12), CountryCode.AU, AustraliaProvider.AD.ACT);
            Assert.AreEqual(true, isPublicHoliday);

            //Veille de pâques.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 4, 15), CountryCode.AU, AustraliaProvider.AD.VIC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 31), CountryCode.AU, AustraliaProvider.AD.VIC);
            Assert.AreEqual(true, isPublicHoliday);

            //Pâques.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 4, 16), CountryCode.AU, AustraliaProvider.AD.NSW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 01), CountryCode.AU, AustraliaProvider.AD.NSW);
            Assert.AreEqual(true, isPublicHoliday);

            //1er lundi de mai.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 5, 01), CountryCode.AU, AustraliaProvider.AD.NT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 07), CountryCode.AU, AustraliaProvider.AD.NT);
            Assert.AreEqual(true, isPublicHoliday);

            //1er lundi de juin.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 6, 05), CountryCode.AU, AustraliaProvider.AD.WA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 04), CountryCode.AU, AustraliaProvider.AD.WA);
            Assert.AreEqual(true, isPublicHoliday);

            //2ème lundi de juin.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 6, 12), CountryCode.AU, AustraliaProvider.AD.TAS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 11), CountryCode.AU, AustraliaProvider.AD.TAS);
            Assert.AreEqual(true, isPublicHoliday);

            //1er lundi d'août.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 8, 07), CountryCode.AU, AustraliaProvider.AD.NT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 06), CountryCode.AU, AustraliaProvider.AD.NT);
            Assert.AreEqual(true, isPublicHoliday);

            //1er lundi d'octobre.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 10, 02), CountryCode.AU, AustraliaProvider.AD.SA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 01), CountryCode.AU, AustraliaProvider.AD.SA);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BY_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 7), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 8), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 17), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 9), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 3), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 7), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BY);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 21), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 11), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BO_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 2), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 12), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 13), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);            
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 21), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 2), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 6), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 2), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BO);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BW_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 1), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 16), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 30), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 1), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.BW);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BR_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 21), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 7), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 12), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 2), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 15), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BR);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BS_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 10), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 10), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 5), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 12), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.BS);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_BG_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 3), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 8), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 9), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 6), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 24), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 6), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 22), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.BG);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_CA_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 12), CountryCode.CA, CanadaProvider.AD.BC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 19), CountryCode.CA, CanadaProvider.AD.MB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 17), CountryCode.CA, CanadaProvider.AD.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.CA, CanadaProvider.AD.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 23), CountryCode.CA, CanadaProvider.AD.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.CA, CanadaProvider.AD.QC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 21), CountryCode.CA, CanadaProvider.AD.NT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 24), CountryCode.CA, CanadaProvider.AD.YT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 1), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 12), CountryCode.CA, CanadaProvider.AD.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 6), CountryCode.CA, CanadaProvider.AD.NU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 20), CountryCode.CA, CanadaProvider.AD.YT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 3), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 8), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 11), CountryCode.CA, CanadaProvider.AD.NS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.CA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.CA, CanadaProvider.AD.ON);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_HR_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 22), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 25), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 5), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 8), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.HR);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_CY_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 19), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 25), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 6), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 8), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 9), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 28), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 1), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 28), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.CY);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_CZ_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 8), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 5), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 6), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 28), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 28), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 17), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.CZ);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_DK_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 4, 27), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
            DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.DK);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_EE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 24), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 23), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 24), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 20), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.EE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_FI_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 22), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 23), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 3), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 6), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.FI);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_FR_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.FR, FranceProvider.AD.FR_57);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 8), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 14), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 11), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.FR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.FR, FranceProvider.AD.FR_A);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_DE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.DE, GermanyProvider.AD.BW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.DE, GermanyProvider.AD.BY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 3), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 31), CountryCode.DE, GermanyProvider.AD.MV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.DE, GermanyProvider.AD.RP);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.DE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_GR_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 19), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 25), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 6), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 8), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 9), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 27), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 28), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 28), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.GR);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_GT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 31), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 30), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 15), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 20), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.GT);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_GL_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 27), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6,21), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.GL);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_HN_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 31), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 14), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 15), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 3), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 12), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 21), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.HN);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_HU_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 15), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 20), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 23), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.HU);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_IS_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 19), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 17), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 6), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.IS);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_IE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 17), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 7), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 4), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 6), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 29), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.IE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_IT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 25), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 2), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.IT);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_LV_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 4), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 13), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 23), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 24), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 18), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.LV);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_LI_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 2), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 19), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 29), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 8), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.LI);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_LT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 16), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 11), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 24), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 6), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.LT);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_LU_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 23), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.LU);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_MG_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 26), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.MG);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_MT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 10), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 19), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 31), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 7), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 29), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 8), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 21), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 13), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.MT);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_NA_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 21), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 4), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 25), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 26), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 10), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.NA);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_NL_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 5), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
        }
        [TestMethod]
        public void PublicHoliday_NL_Spec()
        {
            //Tests pour le king's day.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 27), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            //Samedi 26, car le 27 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2014, 4, 26), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            
            //Tests pour liberation day.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 5), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            //Ce n'est qu'a partir de 1945 que le jour est férié.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(1940, 5, 5), CountryCode.NL);
            Assert.AreEqual(false, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(1950, 5, 5), CountryCode.NL);
            Assert.AreEqual(true, isPublicHoliday);
            //Pas ok car c'est tous les 5 ans.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(1951, 5, 5), CountryCode.NL);
            Assert.AreEqual(false, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_NZ_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 6), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 25), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 4), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 22), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
        }
        [TestMethod]
        public void PublicHoliday_NZ_Spec()
        {
            //Test du 1er janvier.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2015, 1, 1), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 3, car le 1er est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2011, 1, 3), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 2, car le 1er est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2012, 1, 2), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);


            //Test du 2 janvier.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2015, 1, 2), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 4, car le 1 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2011, 1, 4), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 3, car le 1 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2012, 1, 3), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);


            //Test Waitangi
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 2, 6), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 6), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 8, car le 6 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2016, 2, 8), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);            
            //Lundi 7, car le 6 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2022, 2, 7), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);

            //Test Anzac
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 4, 25), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 25), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 27, car le 25 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2015, 4, 27), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2021, 4, 26), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);

            //Test Noël
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 27, car le 25 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2016, 12, 27), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 27, car le 25 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 12, 27), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);

            //Test Boxing
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 28, car le 26 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2015, 12, 28), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 28, car le 26 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 12, 28), CountryCode.NZ);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_NO_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 17), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.NO);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_PY_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 1), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 14), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 15), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 12), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 29), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.PY);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_PE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 29), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 28), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 29), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 30), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 8), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.PE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_PL_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 3), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 11), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.PL);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_PT_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 13), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 25), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 1), CountryCode.PT, PortugalProvider.AD.PT_20);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 10), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 1), CountryCode.PT, PortugalProvider.AD.PT_30);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 5), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 1), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.PT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.PT, PortugalProvider.AD.PT_14);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_RO_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 24), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 8), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 9), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 27), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 28), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 1), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 30), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 1), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.RO);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_RU_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 3), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 4), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 5), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 7), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 23), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 8), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 9), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 12), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 4), CountryCode.RU);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_SK_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 8), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 5), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 29), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 1), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 15), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 17), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.SK);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_SI_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 8), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 27), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 2), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 20), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 8), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 25), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 31), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.SI);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_ZA_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 21), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 27), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 16), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 9), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 24), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 16), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.ZA);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_ES_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 28), CountryCode.ES, SpainProvider.AD.AN);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 1), CountryCode.ES, SpainProvider.AD.IB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 29), CountryCode.ES, SpainProvider.AD.AR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.ES, SpainProvider.AD.CT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 23), CountryCode.ES, SpainProvider.AD.AR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 2), CountryCode.ES, SpainProvider.AD.M);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 17), CountryCode.ES, SpainProvider.AD.GA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 30), CountryCode.ES, SpainProvider.AD.IC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.ES, SpainProvider.AD.CM);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 9), CountryCode.ES, SpainProvider.AD.MU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 24), CountryCode.ES, SpainProvider.AD.CT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 25), CountryCode.ES, SpainProvider.AD.GA);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 2), CountryCode.ES, SpainProvider.AD.CE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 8), CountryCode.ES, SpainProvider.AD.O);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 11), CountryCode.ES, SpainProvider.AD.CT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 15), CountryCode.ES, SpainProvider.AD.CB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 9), CountryCode.ES, SpainProvider.AD.VC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 12), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 25), CountryCode.ES, SpainProvider.AD.PV);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 6), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.ES);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.ES, SpainProvider.AD.IB);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_SE_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 1), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 6), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 22), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 6, 23), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 3), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 24), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.SE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_CH_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.CH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.CH, SwitzerlandProvider.AD.ZH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 6), CountryCode.CH, SwitzerlandProvider.AD.UR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 19), CountryCode.CH, SwitzerlandProvider.AD.LU);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.CH, SwitzerlandProvider.AD.BE);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.CH, SwitzerlandProvider.AD.SZ);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 1), CountryCode.CH, SwitzerlandProvider.AD.ZH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 10), CountryCode.CH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 21), CountryCode.CH, SwitzerlandProvider.AD.OW);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 31), CountryCode.CH, SwitzerlandProvider.AD.SO);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 1), CountryCode.CH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 15), CountryCode.CH, SwitzerlandProvider.AD.BL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 17), CountryCode.CH, SwitzerlandProvider.AD.VD);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 1), CountryCode.CH, SwitzerlandProvider.AD.AI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 8), CountryCode.CH, SwitzerlandProvider.AD.TI);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.CH);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.CH, SwitzerlandProvider.AD.TG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 31), CountryCode.CH, SwitzerlandProvider.AD.GE);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_CH_Spec()
        {
            //Tests pour le 2 janvier canton NEUCHÂTEL uniquement.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2016, 1, 2), CountryCode.CH, SwitzerlandProvider.AD.NE);
            Assert.AreEqual(false, isPublicHoliday);
            //Ok car le 1 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 1, 2), CountryCode.CH, SwitzerlandProvider.AD.NE);
            Assert.AreEqual(true, isPublicHoliday);
            //Tests pour le 26 décembre canton NEUCHÂTEL uniquement.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 12, 26), CountryCode.CH, SwitzerlandProvider.AD.NE);
            Assert.AreEqual(false, isPublicHoliday);
            //Ok car le 25 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2016, 12, 26), CountryCode.CH, SwitzerlandProvider.AD.NE);
            Assert.AreEqual(true, isPublicHoliday);            
        }

        [TestMethod]
        public void PublicHoliday_GB_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 17), CountryCode.GB, UnitedKingdomProvider.AD.NIR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 3, 30), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 4, 2), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 7), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 28), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 12), CountryCode.GB, UnitedKingdomProvider.AD.NIR);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 6), CountryCode.GB, UnitedKingdomProvider.AD.SCT);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 8, 27), CountryCode.GB, UnitedKingdomProvider.AD.ENG);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 26), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_GB_Spec()
        {
            //Tests sur le 1er janvier.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 2, car le 1 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 1, 2), CountryCode.GB, UnitedKingdomProvider.AD.ENG);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 3, car le 1 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2011, 1, 3), CountryCode.GB, UnitedKingdomProvider.AD.ENG);
            Assert.AreEqual(true, isPublicHoliday);

            //Tests sur le 2 janvier.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 2), CountryCode.GB, UnitedKingdomProvider.AD.SCT);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 3, car le 1 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 1, 3), CountryCode.GB, UnitedKingdomProvider.AD.SCT);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 4, car le 1 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2011, 1, 4), CountryCode.GB, UnitedKingdomProvider.AD.SCT);
            Assert.AreEqual(true, isPublicHoliday);

            //Tests sur le 25 décembre.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2012, 12, 25), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 27, car le 25 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 12, 27), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 27, car le 25 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2016, 12, 27), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);

            //Tests sur le 26 décembre.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2012, 12, 26), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            //Mardi 28, car le 26 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2010, 12, 28), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 28, car le 26 est un samedi.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2009, 12, 28), CountryCode.GB);
            Assert.AreEqual(true, isPublicHoliday);
        }

        [TestMethod]
        public void PublicHoliday_US_IsPublicHoliday()
        {
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 1), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 15), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 1, 20), CountryCode.US, UnitedStatesProvider.AD.US_DC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 2, 19), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 5, 28), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 7, 4), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 9, 3), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 10, 8), CountryCode.US, UnitedStatesProvider.AD.US_AL);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 22), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 12, 25), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
        }    
        
        [TestMethod]    
        public void PublicHoliday_US_Spec()
        {
            //Test pour le jour des vétérans.
            //Vendredi 10, car le 11 est un samedi.
            var isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 11, 10), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            //Lundi 12, car le 11 est un dimanche.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 11, 12), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2019, 11, 11), CountryCode.US);
            Assert.AreEqual(true, isPublicHoliday);

            //Test pour le Inauguration day.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(1933, 3, 4), CountryCode.US, UnitedStatesProvider.AD.US_DC);
            Assert.AreEqual(true, isPublicHoliday);
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2017, 1, 20), CountryCode.US, UnitedStatesProvider.AD.US_DC);
            Assert.AreEqual(true, isPublicHoliday);
            //Pas ok car c'est tous les 4 ans.
            isPublicHoliday = DateSystem.IsPublicHoliday(new DateTime(2018, 1, 20), CountryCode.US, UnitedStatesProvider.AD.US_DC);
            Assert.AreEqual(false, isPublicHoliday);
        }
    }
}
