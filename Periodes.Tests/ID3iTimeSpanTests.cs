﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Periodes.Tests
{
    [TestClass]
    public class ID3iTimeSpanTests
    {
        [TestMethod]
        public void ID3iTimeSpan_All()
        {
            foreach (var value in new List<int>() { 1, 32, 100, 1000, -1, -100, 0 })
            {
                Assert.AreEqual(value.Years(), new ID3iTimeSpan { Years = value });
                Assert.AreEqual(value.Months(), new ID3iTimeSpan { Months = value });
                Assert.AreEqual(value.Weeks(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromDays(value * 7) });
                Assert.AreEqual(value.Days(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromDays(value) });
                Assert.AreEqual(value.Hours(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromHours(value) });
                Assert.AreEqual(value.Minutes(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromMinutes(value) });
                Assert.AreEqual(value.Seconds(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromSeconds(value) });
                Assert.AreEqual(value.Milliseconds(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromMilliseconds(value) });
                Assert.AreEqual(value.Ticks(), new ID3iTimeSpan { TimeSpan = TimeSpan.FromTicks(value) });
            }
        }

        [TestMethod]
        public void ID3iTimeSpan_Subtract()
        {
            Assert.AreEqual(3, 3.5.Days().Subtract(.5.Days()).Days);
        }

        [TestMethod]
        public void ID3iTimeSpan_GetHashCodeTest()
        {
            Assert.AreEqual(343024320, 3.5.Days().GetHashCode());
        }

        [TestMethod]
        public void ID3iTimeSpan_CompareToID3iTimeSpan()
        {
            Assert.AreEqual(0, 3.Days().CompareTo(3.Days()));
            Assert.AreEqual(-1, 3.Days().CompareTo(4.Days()));
            Assert.AreEqual(1, 4.Days().CompareTo(3.Days()));
        }

        [TestMethod]
        public void ID3iTimeSpan_CompareToTimeSpan()
        {
            Assert.AreEqual(0, 3.Days().CompareTo(TimeSpan.FromDays(3)));
            Assert.AreEqual(-1, 3.Days().CompareTo(TimeSpan.FromDays(4)));
            Assert.AreEqual(1, 4.Days().CompareTo(TimeSpan.FromDays(3)));
        }

        [TestMethod]
        public void ID3iTimeSpan_CompareToObject()
        {
            Assert.AreEqual(0, 3.Days().CompareTo((object)TimeSpan.FromDays(3)));
            Assert.AreEqual(-1, 3.Days().CompareTo((object)TimeSpan.FromDays(4)));
            Assert.AreEqual(1, 4.Days().CompareTo((object)TimeSpan.FromDays(3)));
        }

        [TestMethod]
        public void ID3iTimeSpan_EqualsID3iTimeSpan()
        {
            Assert.IsTrue(3.Days().Equals(3.Days()));
            Assert.IsFalse(4.Days().Equals(3.Days()));
        }

        [TestMethod]
        public void ID3iTimeSpan_EqualsTimeSpan()
        {
            Assert.IsTrue(3.Days().Equals(TimeSpan.FromDays(3)));
            Assert.IsFalse(4.Days().Equals(TimeSpan.FromDays(3)));
        }

        [TestMethod]
        public void ID3iTimeSpan_Equals()
        {
            Assert.IsFalse(3.Days().Equals(null));
        }

        [TestMethod]
        public void ID3iTimeSpan_EqualsTimeSpanAsObject()
        {
            Assert.IsTrue(3.Days().Equals((object)TimeSpan.FromDays(3)));
        }

        [TestMethod]
        public void ID3iTimeSpan_EqualsObject()
        {
            Assert.IsFalse(3.Days().Equals(1));
        }

        [TestMethod]
        public void ID3iTimeSpan_Add()
        {
            Assert.AreEqual(4, 3.5.Days().Add(.5.Days()).Days);
        }

        [TestMethod]
        public void ID3iTimeSpan_ToStringTest()
        {
            Assert.AreEqual("3.12:00:00", 3.5.Days().ToString());
        }

        [TestMethod]
        public void ID3iTimeSpan_Ticks()
        {
            Assert.AreEqual(30000, 3.Milliseconds().Ticks);
        }

        [TestMethod]
        public void ID3iTimeSpan_Milliseconds()
        {
            Assert.AreEqual(100, 1100.Milliseconds().Milliseconds);
        }

        [TestMethod]
        public void ID3iTimeSpan_TotalMilliseconds()
        {
            Assert.AreEqual(1100, 1100.Milliseconds().TotalMilliseconds);
        }

        [TestMethod]
        public void ID3iTimeSpan_Seconds()
        {
            Assert.AreEqual(1, 61.Seconds().Seconds);
        }

        [TestMethod]
        public void ID3iTimeSpan_TotalSeconds()
        {
            Assert.AreEqual(61, 61.Seconds().TotalSeconds);
        }

        [TestMethod]
        public void ID3iTimeSpan_Minutes()
        {
            Assert.AreEqual(1, 61.Minutes().Minutes);
        }

        [TestMethod]
        public void ID3iTimeSpan_TotalMinutes()
        {
            Assert.AreEqual(61, 61.Minutes().TotalMinutes);
        }

        [TestMethod]
        public void ID3iTimeSpan_Hours()
        {
            Assert.AreEqual(1, 25.Hours().Hours);
        }

        [TestMethod]
        public void ID3iTimeSpan_TotalHours()
        {
            Assert.AreEqual(25, 25.Hours().TotalHours);
        }

        [TestMethod]
        public void ID3iTimeSpan_Days()
        {
            Assert.AreEqual(366, 366.Days().Days);
        }

        [TestMethod]
        public void ID3iTimeSpan_TotalDays()
        {
            Assert.AreEqual(366, 366.Days().TotalDays);
        }

        [TestMethod]
        public void ID3iTimeSpan_Years()
        {
            Assert.AreEqual(3, 3.Years().Years);
        }

        [TestMethod]
        public void ID3iTimeSpan_VerifyConvertIsCorrect()
        {
            TimeSpan timeSpan = 10.Years();
            Assert.AreEqual(3652d, timeSpan.TotalDays);
        }
    }
    [TestClass]
    public class OperatorTests
    {
        [TestMethod]
        public void Op_LessThan()
        {
            Assert.IsTrue(1.Seconds() < 2.Seconds());
            Assert.IsTrue(1.Seconds() < TimeSpan.FromSeconds(2));
            Assert.IsTrue(TimeSpan.FromSeconds(1) < 2.Seconds());
        }

        [TestMethod]
        public void Op_LessThanOrEqualTo()
        {
            Assert.IsTrue(1.Seconds() <= 2.Seconds());
            Assert.IsTrue(1.Seconds() <= TimeSpan.FromSeconds(2));
            Assert.IsTrue(TimeSpan.FromSeconds(1) <= 2.Seconds());
        }

        [TestMethod]
        public void Op_GreaterThan()
        {
            Assert.IsTrue(2.Seconds() > 1.Seconds());
            Assert.IsTrue(2.Seconds() > TimeSpan.FromSeconds(1));
            Assert.IsTrue(TimeSpan.FromSeconds(2) > 1.Seconds());
        }

        [TestMethod]
        public void Op_GreaterThanOrEqualTo()
        {
            Assert.IsTrue(2.Seconds() >= 1.Seconds());
            Assert.IsTrue(2.Seconds() >= TimeSpan.FromSeconds(1));
            Assert.IsTrue(TimeSpan.FromSeconds(2) >= 1.Seconds());
        }

        [TestMethod]
        public void Op_Equals()
        {
            Assert.IsTrue(2.Seconds() == 2.Seconds());
            Assert.IsTrue(2.Seconds() == TimeSpan.FromSeconds(2));
            Assert.IsTrue(TimeSpan.FromSeconds(2) == 2.Seconds());
        }

        [TestMethod]
        public void Op_NotEquals()
        {
            Assert.IsTrue(2.Seconds() != 1.Seconds());
            Assert.IsTrue(2.Seconds() != TimeSpan.FromSeconds(1));
            Assert.IsTrue(TimeSpan.FromSeconds(2) != 1.Seconds());
        }

        [TestMethod]
        public void Op_Add()
        {
            Assert.AreEqual(1.Seconds() + 2.Seconds(), 3.Seconds());
            Assert.AreEqual(1.Seconds() + TimeSpan.FromSeconds(2), 3.Seconds());
            Assert.AreEqual(TimeSpan.FromSeconds(1) + 2.Seconds(), 3.Seconds());
        }

        [TestMethod]
        public void Op_Subtract()
        {
            Assert.AreEqual(1.Seconds() - 2.Seconds(), -1.Seconds());
            Assert.AreEqual(1.Seconds() - TimeSpan.FromSeconds(2), -1.Seconds());
            Assert.AreEqual(TimeSpan.FromSeconds(1) - 2.Seconds(), -1.Seconds());
        }
    }    
}
