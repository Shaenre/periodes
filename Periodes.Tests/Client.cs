﻿using ID3iHoliday.Models;
using Periodes.WebService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using static Periodes.WebService.Models.PeriodePosition;
using static Periodes.WebService.Models.PeriodePolicy;
using static Periodes.WebService.Models.CodeApi;
using static Periodes.Gestion.Standard.Data.TypePeriodeReference;
using static Periodes.Gestion.WorkingDays.Data.TypePeriode;

namespace Periodes.Tests
{
    [TestClass]
    public class Client
    {
        [TestMethod]
        public void Client_Hello()
        {
            var wrapper = new WebService.Client.PublicWrapper();
            var rr = wrapper.Client.Index.Hello.Call();
            if (rr.IsRight)
                Assert.IsTrue(true);
            else
                Assert.Fail();
        }
        /// <summary>
        /// Récupération de la période de référence (date et type de référence).
        /// </summary>
        [TestMethod]
        public void Client_GetTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(HALFDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 01, 17, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 01, 17, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceGetTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25)).TypePeriode.Accept(DAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 03, 25, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 03, 25, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceGetTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25)).TypePeriode.Accept(WORKINGDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(null, result.Right.Object);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération de la période du type souhaité par rapport à une période de référence (date et type de référence).
        /// </summary>
        [TestMethod]
        public void Client_GetAnotherTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeSouhaite.Accept(MONTH).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 01, 01, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 01, 31, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceGetAnotherTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25)).TypePeriode.Accept(DAY).TypePeriodeSouhaite.Accept(MONTH).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 03, 01, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 03, 31, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceGetAnotherTypeFromType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25)).TypePeriode.Accept(WORKINGDAY).TypePeriodeSouhaite.Accept(MONTH).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Get.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(null, result.Right.Object);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type de référence.
        /// Dans ce cas le type de décallage est un type plus large que celui de référence.
        /// La période renvoyée est au même index.
        /// </summary>
        [TestMethod]
        public void Client_AddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 08, 17, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 08, 17, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 25, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 25, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInTypeSameIndex()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 11, 14, 00, 00)).TypePeriode.Accept(HALFWORKINGDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 11, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 11, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallé d'un nombre de type de période (offset)
        /// exprimée en type de période souhaité.
        /// Dans ce cas le type de décallage et le type souhaité sont plus large que celui de référence.
        /// La période renvoyé est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Client_AddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(WEEK).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 08, 12, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 08, 18, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(WEEK).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 9, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 15, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInHighThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(HALFWORKINGDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(WEEK).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 9, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 15, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée dans le type de référence.
        /// Dans ce cas le type de décallage est plus large que celui de référence.
        /// La période renvoyée est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Client_AddHighAnotherTypeFromTypeResultInTypeAvgIndex()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 31, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(1).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 02, 14, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 02, 14, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddHighAnotherTypeFromTypeResultInTypeAvgIndex()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2018, 03, 31, 14, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(1).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2018, 04, 15, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2018, 04, 15, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallé d'un nombre de type de période (offset)
        /// exprimée dans le type souhaité.
        /// Dans ce cas le type de décallage est plus large que celui de référence et le type souhaité est plus fin que celui de référence.
        /// La période renvoyée est la période moyenne du type de décallage.
        /// </summary>
        [TestMethod]
        public void Client_AddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(DAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(HALFWORKINGDAY).Finish();            

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 08, 16, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 08, 16, 11, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(DAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(HALFWORKINGDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 16, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 16, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceAddHighAnotherTypeFromTypeResultInLowThirdType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(WORKINGDAY).TypePeriodeOffset.Accept(MONTH).Offset.Accept(7).TypePeriodeSouhaite.Accept(HALFWORKINGDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 10, 16, 12, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 10, 16, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type de référence.
        /// Dans ce cas le type de décallage est plus fin que celui de référence.
        /// La période renvoyée est calculée à partir de la dernière période du type de décallage.
        /// </summary>
        [TestMethod]
        public void Client_AddLowAnotherTypeFromTypeResultInType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(DAY).Offset.Accept(1).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 02, 1, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 02, 28, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddLowAnotherTypeFromTypeResultInType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(DAY).Offset.Accept(1).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 04, 30, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceAddLowAnotherTypeFromTypeResultInType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(WORKINGDAY).Offset.Accept(1).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 04, 30, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        /// <summary>
        /// Récupération, à partir d'une période de référence, d'une période décallée d'un nombre de type de période (offset)
        /// exprimée en type souhaité.
        /// Dans ce cas le type de décallage et le type souhaité sont plus fins que le type de référence.
        /// La période renvoyée est calculée à partir de la dernière période du type de décallage exprimée en type souhaité.
        /// </summary>
        [TestMethod]
        public void Client_AddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 01, 17, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(DAY).Offset.Accept(1).TypePeriodeSouhaite.Accept(DAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 02, 1, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 02, 1, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayReferenceAddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(DAY).Offset.Accept(1).TypePeriodeSouhaite.Accept(DAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 04, 1, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 04, 1, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }
        [TestMethod]
        public void Client_SaturdayNotReferenceAddLowAnotherTypeFromTypeResultInLowAnotherType()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeOffset.Accept(WORKINGDAY).Offset.Accept(1).TypePeriodeSouhaite.Accept(WORKINGDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Add.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2017, 04, 3, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2017, 04, 3, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_GetSpecificDays()
        {
            var factory = SettingsHolidayFactory.Make;
            var settings = factory.Year.Accept(2018).Type.Accept(RuleType.All).Country.Accept("FR").State.Accept("MQ").Finish();
            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.SpecificDay.List.Call(settings);
            Assert.AreEqual(15, result.Right.Object.Count);

            factory = SettingsHolidayFactory.Make;
            settings = factory.Year.Accept(2018).Type.Accept(RuleType.All).Country.Accept("FR").Finish();
            result = wrapper.Client.SpecificDay.List.Call(settings);
            Assert.AreEqual(13, result.Right.Object.Count);

            //Test des cas d'erreurs.
            settings = factory.Year.Accept(2018).Type.Accept(RuleType.All).Country.Accept("FR").State.Accept("ERROR").Finish();
            result = wrapper.Client.SpecificDay.List.Call(settings);
            Assert.AreEqual(true, result.IsRight);
            Assert.AreEqual(Erreur, result.Right.Statut);

            factory = SettingsHolidayFactory.Make;
            settings = factory.Year.Accept(2018).Type.Accept(RuleType.All).Country.Accept("ERROR").Finish();
            result = wrapper.Client.SpecificDay.List.Call(settings);
            Assert.AreEqual(true, result.IsRight);
            Assert.AreEqual(Erreur, result.Right.Statut);
        }

        [TestMethod]
        public void Client_Avg()
        {
            var factory = SettingsFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2019, 08, 17)).TypePeriode.Accept(WEEK).TypePeriodeSouhaite.Accept(DAY).Finish();          

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Avg.Call(settings);
            if (result.IsRight)
            {
                Assert.AreEqual(new DateTime(2019, 08, 15, 0, 0, 0), result.Right.Object.DateDebut);
                Assert.AreEqual(new DateTime(2019, 08, 15, 23, 59, 59), result.Right.Object.DateFin);
            }
            else
                Assert.Fail();
        }       

        [TestMethod]
        public void Client_Count_ReferenceType()
        {
            var factory = SettingsCountFactory.Make;
            var settings = factory.Start.Accept(new DateTime(2017, 03, 25, 14, 00, 00)).End.Accept(new DateTime(2017, 10, 25, 12, 0, 0)).TypePeriode.Accept(HALFDAY).TypePeriodeCounted.Accept(MONTH).Finish();            

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Count.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(8, result.Right.Object);
            else
                Assert.Fail();

            settings = factory.Start.Accept(new DateTime(2017, 06, 12)).End.Accept(new DateTime(2017, 08, 12)).TypePeriode.Accept(MONTH)
                .TypePeriodeCounted.Accept(DAY).Position.Accept(FTL).Policy.Accept(IncludeEndPeriode).Finish();
            result = wrapper.Client.Periodes.Count.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(92, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Count_SameType()
        {
            var factory = SettingsCountFactory.Make;
            var settings = factory.Start.Accept(new DateTime(2017, 03, 20, 00, 00, 00)).End.Accept(new DateTime(2017, 03, 27, 00, 0, 0)).TypePeriode.Accept(DAY).TypePeriodeCounted.Accept(DAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Count.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(8, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Count_SpecType()
        {
            var factory = SettingsCountFactory.Make;
            var settings = factory.Start.Accept(new DateTime(2017, 03, 25, 00, 00, 00)).End.Accept(new DateTime(2017, 04, 01, 00, 0, 0)).TypePeriode.Accept(HALFDAY).TypePeriodeCounted.Accept(WORKINGDAY).Finish();

            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Count.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(5, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Country()
        {
            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.ISO.Country.Call(SettingsISOFactory.Make.Country.Accept("FR").Finish());
            if (result.IsRight)
                Assert.IsNotNull(result.Right.Object);
            else
                Assert.Fail();
            result = wrapper.Client.ISO.Country.Call(SettingsISOFactory.Make.Country.Accept("FR").State.Accept("MQ").Finish());
            if (result.IsRight)
                Assert.IsNotNull(result.Right.Object);
            else
                Assert.Fail();

            result = wrapper.Client.ISO.Country.Call(SettingsISOFactory.Make.Country.Accept("ERROR").Finish());
            if (result.IsRight)
                Assert.IsNull(result.Right.Object);
            else
                Assert.Fail();
            result = wrapper.Client.ISO.Country.Call(SettingsISOFactory.Make.Country.Accept("FR").State.Accept("ERROR").Finish());
            if (result.IsRight)
                Assert.IsNull(result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_SupportedCountry()
        {
            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.ISO.SupportedCountry.Call();
            if (result.IsRight)
                Assert.AreEqual(3, result.Right.Object.Count);
            else
                Assert.Fail(result.Left);
        }

        [TestMethod]
        public void Client_Num1()
        {
            var factory = SettingsNumberFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 06, 09, 15, 00, 00)).TypePeriode.Accept(MONTH).TypePeriodeSouhaite.Accept(YEAR).Finish();
            var wrapper = new WebService.Client.PublicWrapper();

            var result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(6, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = WEEK.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(23, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = DAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(160, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(320, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = WORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(110, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFWORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(220, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Num2()
        {
            var factory = SettingsNumberFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 06, 09, 15, 00, 00)).TypePeriode.Accept(WEEK).TypePeriodeSouhaite.Accept(MONTH).Finish();
            var wrapper = new WebService.Client.PublicWrapper();

            var result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(2, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = DAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(9, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(18, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = WORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(6, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFWORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(12, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Num3()
        {
            var factory = SettingsNumberFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 06, 09, 15, 00, 00)).TypePeriode.Accept(DAY).TypePeriodeSouhaite.Accept(WEEK).Finish();
            var wrapper = new WebService.Client.PublicWrapper();

            var result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(5, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(10, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = WORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(4, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFWORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(8, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Num4()
        {
            var factory = SettingsNumberFactory.Make;
            var settings = factory.Date.Accept(new DateTime(2017, 06, 09, 15, 00, 00)).TypePeriode.Accept(HALFDAY).TypePeriodeSouhaite.Accept(DAY).Finish();
            var wrapper = new WebService.Client.PublicWrapper();

            var result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(2, result.Right.Object);
            else
                Assert.Fail();

            settings.TypePeriode = HALFWORKINGDAY.ToString();
            result = wrapper.Client.Periodes.Num.Call(settings);
            if (result.IsRight)
                Assert.AreEqual(2, result.Right.Object);
            else
                Assert.Fail();
        }

        [TestMethod]
        public void Client_Enumerate()
        {
            var factory = SettingsCountFactory.Make;
            var settings = factory.Start.Accept(new DateTime(2017, 06, 01)).End.Accept(new DateTime(2017, 08, 12)).TypePeriode.Accept(MONTH)
                .TypePeriodeCounted.Accept(DAY).Position.Accept(FTL).Policy.Accept(IncludeEndPeriode).Finish();
            var wrapper = new WebService.Client.PublicWrapper();
            var result = wrapper.Client.Periodes.Enumerate.Call(settings);
            if (result.IsRight)
            {
                foreach (var item in result.Right.Object)
                    Console.WriteLine($"{item.Alias} - {item.DateDebut.ToShortDateString()} - {item.DateFin.ToShortDateString()}");
                Assert.AreEqual(92, result.Right.Object.Count);
            }
            else
                Assert.Fail();
        }
    }
}
