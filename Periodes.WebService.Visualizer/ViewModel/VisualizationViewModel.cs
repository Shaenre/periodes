﻿using ID3iCore;
using ID3iHoliday.Models;
using Periodes.WebService.Client;
using Periodes.WebService.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Visualizer.ViewModel
{
    public class VisualizationViewModel : BindableBase
    {
        public Settings SettingsPeriodeApiView1 { get; set; } = new Settings() { Date = DateTime.Today };
        public DelegateCommand TryGetCommand1 => new DelegateCommand(TryGetExecute1);
        private void TryGetExecute1()
        {
            GetReponse(Wrapper.Client.Periodes.Get.Call(SettingsPeriodeApiView1));
        }

        public Settings SettingsPeriodeApiView2 { get; set; } = new Settings() { Date = DateTime.Today };
        public DelegateCommand TryGetCommand2 => new DelegateCommand(TryGetExecute2);
        private void TryGetExecute2()
        {
            GetReponse(Wrapper.Client.Periodes.Get.Call(SettingsPeriodeApiView2));
        }

        public Settings SettingsPeriodeApiView3 { get; set; } = new Settings() { Date = DateTime.Today };
        public DelegateCommand TryAddCommand1 => new DelegateCommand(TryAddExecute1);
        private void TryAddExecute1()
        {
            GetReponse(Wrapper.Client.Periodes.Add.Call(SettingsPeriodeApiView3));
        }

        public Settings SettingsPeriodeApiView4 { get; set; } = new Settings() { Date = DateTime.Today };
        public DelegateCommand TryAddCommand2 => new DelegateCommand(TryAddExecute2);
        private void TryAddExecute2()
        {
            GetReponse(Wrapper.Client.Periodes.Add.Call(SettingsPeriodeApiView4));
        }

        public Settings SettingsPeriodeApiView5 { get; set; } = new Settings() { Date = DateTime.Today };
        public DelegateCommand TryAvgCommand => new DelegateCommand(TryAvgExecute);
        private void TryAvgExecute()
        {
            GetReponse(Wrapper.Client.Periodes.Avg.Call(SettingsPeriodeApiView5));
        }

        public SettingsCount SettingsCountApiView1 { get; set; } = new SettingsCount() { StartDate = DateTime.Now, EndDate = 1.Days().After(DateTime.Now) };
        public DelegateCommand TryCountCommand => new DelegateCommand(TryCountExecute);
        private void TryCountExecute()
        {
            var res = Wrapper.Client.Periodes.Count.Call(SettingsCountApiView1);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsCount SettingsCountApiView2 { get; set; } = new SettingsCount() { StartDate = DateTime.Now, EndDate = 1.Days().After(DateTime.Now) };
        public DelegateCommand TryEnumerateCommand => new DelegateCommand(TryEnumerateExecute);
        private void TryEnumerateExecute()
        {
            var res = Wrapper.Client.Periodes.Enumerate.Call(SettingsCountApiView2);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsNumber SettingsNumberApiView1 { get; set; } = new SettingsNumber() { Date = DateTime.Now };
        public DelegateCommand TryNumCommand => new DelegateCommand(TryNumExecute);
        private void TryNumExecute()
        {
            var res = Wrapper.Client.Periodes.Num.Call(SettingsNumberApiView1);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public DelegateCommand TryISOGetAllCommand => new DelegateCommand(TryISOGetAllExecute);
        private void TryISOGetAllExecute()
        {
            var res = Wrapper.Client.ISO.SupportedCountry.Call();
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsISO SettingsISOApiView1 { get; set; } = new SettingsISO();
        public DelegateCommand TryISOCommand1 => new DelegateCommand(TryISOExecute1);
        private void TryISOExecute1()
        {
            var res = Wrapper.Client.ISO.Country.Call(SettingsISOApiView1);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsISO SettingsISOApiView2 { get; set; } = new SettingsISO();
        public DelegateCommand TryISOCommand2 => new DelegateCommand(TryISOExecute2);
        private void TryISOExecute2()
        {
            var res = Wrapper.Client.ISO.State.Call(SettingsISOApiView2);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsISO SettingsISOApiView3 { get; set; } = new SettingsISO();
        public DelegateCommand TryISOCommand3 => new DelegateCommand(TryISOExecute3);
        private void TryISOExecute3()
        {
            var res = Wrapper.Client.ISO.Region.Call(SettingsISOApiView3);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView1 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetHolidayCommand1 => new DelegateCommand(TryGetHolidayExecute1);
        private void TryGetHolidayExecute1()
        {
            var res = Wrapper.Client.SpecificDay.List.Call(SettingsHolidayApiView1);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView2 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetHolidayCommand2 => new DelegateCommand(TryGetHolidayExecute2);
        private void TryGetHolidayExecute2()
        {
            var res = Wrapper.Client.SpecificDay.List.Call(SettingsHolidayApiView2);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView3 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetHolidayCommand3 => new DelegateCommand(TryGetHolidayExecute3);
        private void TryGetHolidayExecute3()
        {
            var res = Wrapper.Client.SpecificDay.List.Call(SettingsHolidayApiView3);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView4 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetLongWeekEndsCommand1 => new DelegateCommand(TryGetLongWeekEndsExecute1);
        private void TryGetLongWeekEndsExecute1()
        {
            var res = Wrapper.Client.SpecificDay.LongWeekEnds.Call(SettingsHolidayApiView4);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView5 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetLongWeekEndsCommand2 => new DelegateCommand(TryGetLongWeekEndsExecute2);
        private void TryGetLongWeekEndsExecute2()
        {
            var res = Wrapper.Client.SpecificDay.LongWeekEnds.Call(SettingsHolidayApiView5);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public SettingsHoliday SettingsHolidayApiView6 { get; set; } = new SettingsHoliday();
        public DelegateCommand TryGetLongWeekEndsCommand3 => new DelegateCommand(TryGetLongWeekEndsExecute3);
        private void TryGetLongWeekEndsExecute3()
        {
            var res = Wrapper.Client.SpecificDay.LongWeekEnds.Call(SettingsHolidayApiView6);
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public DelegateCommand TryHelloCommand => new DelegateCommand(TryHelloExecute);
        private void TryHelloExecute()
        {
            var res = Wrapper.Client.Index.Hello.Call();
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }

        public PublicWrapper Wrapper { get; } = new PublicWrapper();

        private string reponse;
        public string Reponse
        {
            get
            {
                return reponse;
            }
            set
            {
                if (reponse != value)
                {
                    reponse = value;
                    RaisePropertyChanged(nameof(Reponse));
                }
            }
        }

        [Dependency]
        public IHolidaySystem HolidaySystem { get; set; }
        public VisualizationViewModel()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                Converters = new List<JsonConverter> { new StringEnumConverter() }
            };           
        }
        
        private void GetReponse(Either<string, RetourApi<CodeApi, PeriodeModel>> res)
        {
            if (res.IsLeft)
                Reponse = JsonConvert.SerializeObject(res.Left);
            else
                Reponse = JsonConvert.SerializeObject(res.Right);
        }
    }
}
