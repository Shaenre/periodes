﻿using ID3iHoliday.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Standard = Periodes.Gestion.Standard.Data;
using WorkingDays = Periodes.Gestion.WorkingDays.Data;

namespace Periodes.WebService.Visualizer
{    
    public class StaticData
    {
        public static StaticData Instance { get; } = new StaticData();
        private StaticData() { }
        public List<string> TypesPeriode =>
            Enum.GetNames(typeof(Standard::TypePeriodeReference)).Where(x => x != Standard::TypePeriodeReference.NONE.ToString())
            .Union(Enum.GetNames(typeof(WorkingDays::TypePeriode)).Where(x => x != WorkingDays::TypePeriode.NONE.ToString())).ToList();
        public List<string> RuleType => Enum.GetNames(typeof(RuleType)).ToList();
        
        public List<string> Years => new List<string>()
        {
            "2017", "2018", "2019", "2020"
        };
    }
}
