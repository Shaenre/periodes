﻿using Periodes.Global;
using Periodes.WebService.Visualizer.View;
using ID3iShared.Module;
using ID3iShared.Regions;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Periodes.WebService.Visualizer
{
    public class ApiVisualizerModule : Module
    {
        public ApiVisualizerModule() { }
        public override void Initialize()
        {
            Container.RegisterTypeForNavigation<VisualizationView>();

            RegionManager.RegisterViewWithRegion(RegionNames.MenuRegion, () => new ViewWithName(new RadRadioButton()
            {
                Content = "WebServices",
                Command = Container.Resolve<IApplicationCommands>().NavigateCommand,
                CommandParameter = nameof(VisualizationView),
                IsChecked = true
            }, $"RadRadioButton_{nameof(VisualizationView)}_{new Random().Next()}"));

            Container.Resolve<IApplicationCommands>().NavigateCommand.Execute(nameof(VisualizationView));
        }
    }
}
