﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.BuilderConfig.NorthAmerican
{
    public class BuilderConfigNorthAmerican : BuilderConfig
    {
        public override Calendar Calendar => new GregorianCalendar();

        public override CalendarWeekRule CalendarWeekRule => CalendarWeekRule.FirstDay;

        public override DayOfWeek FirstDayOfWeek => DayOfWeek.Sunday;

        public override DateTime FirstDayOfYear(int year)
        {
            DateTime jan01 = new DateTime(year, 1, 1, 0, 0, 0);
            switch (jan01.DayOfWeek)
            {
                case DayOfWeek.Sunday: //Le 1 est un dimanche pas de soucis.
                    return jan01;
                case DayOfWeek.Monday: //Le 1 est un lundi, on retourne le dimanche d'avant.
                    return 1.Days().Before(jan01);
                case DayOfWeek.Tuesday: //Le 1 est un mardi, on retourne le dimanche d'avant.
                    return 2.Days().Before(jan01);
                case DayOfWeek.Wednesday: //Le 1 est un mercredi, on retourne le dimanche d'avant.
                    return 3.Days().Before(jan01);
                case DayOfWeek.Thursday: //Le 1 est un jeudi, on retourne le dimanche d'avant.
                    return 4.Days().Before(jan01);
                case DayOfWeek.Friday: //Le 1 est un vendredi, on retourne le dimanche d'avant.
                    return 5.Days().Before(jan01);
                case DayOfWeek.Saturday: //Le 1 est un samedi, on retourne le dimanche d'avant.
                    return 6.Days().Before(jan01);
                default:
                    return jan01;
            }
        }

        public override DateTime LastDayOfYear(int year)
        {
            return 1.Days().Before(FirstDayOfYear(year + 1));
        }
    }
}
