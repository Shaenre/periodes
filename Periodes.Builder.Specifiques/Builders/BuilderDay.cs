﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder.Specifiques.Builders
{
    public class BuilderDay : Builder
    {
        [Dependency]
        public IFactoryVM Factory { get; set; }

        public override void Build()
        {
            int year, maxMonth, maxDay = 0;
            for (int i = 0; i < EndDate.Year - StartDate.Year; i++)
            {
                year = StartDate.AddYears(i).Year;
                maxMonth = Config.Calendar.GetMonthsInYear(year);
                for (int j = 0; j < maxMonth; j++)
                {
                    maxDay = Config.Calendar.GetDaysInMonth(year, j + 1);
                    for (int k = 0; k < maxDay; k++)
                    {
                        DateTime dateDebut = new DateTime(year, j + 1, k + 1, 0, 0, 0);
                        DateTime dateFin = dateDebut.AddDays(1).AddSeconds(-1);

                        VMPeriode periode = Factory.GetPeriode(TypePeriode.Id, dateDebut, dateFin);
                        if (periode == null)
                        {
                            string designation = (year * 10000 + (j + 1) * 100 + (k + 1)).ToString();
                            int index = k + 1;
                            string textSpecifique = Policy.DaySuffixeAlias(dateDebut);
                            if (!string.IsNullOrEmpty(textSpecifique))
                                textSpecifique = " (" + textSpecifique + ")";
                            periode = new VMPeriode()
                            {
                                Designation = designation,
                                Reference = TypePeriode.Designation + "_" + designation,
                                DateDebut = dateDebut,
                                DateFin = dateFin,
                                Quantite = (int)(dateFin - dateDebut).TotalSeconds + 1,
                                IdTypePeriode = TypePeriode.Id,
                                Index = index,
                                Alias = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedDayName(dateDebut.DayOfWeek) + " "
                                    + (index.ToString().Length == 1 ? "0" + index.ToString() : index.ToString()) + " "
                                    + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(j + 1) + " "
                                    + year
                                    + textSpecifique,
                                Active = Policy.IsDayActive(dateDebut, dateFin, dateDebut.DayOfWeek),
                                Year = year
                            };
                            Factory.Periodes.Add(periode);
                        }
                        else
                            periode.Active = Policy.IsDayActive(dateDebut, dateFin, dateDebut.DayOfWeek);
                        periode.Year = year;
                        //Progress.Report(periode.Alias);
                    }
                    Factory.SavePeriodes();
                }
            }
        }
    }
}
