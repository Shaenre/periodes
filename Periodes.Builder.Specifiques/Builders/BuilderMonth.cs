﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder.Specifiques.Builders
{
    public class BuilderMonth : Builder
    {
        [Dependency]
        public IFactoryVM Factory { get; set; }

        public override void Build()
        {
            int year, maxMonth = 0;
            for (int i = 0; i < EndDate.Year - StartDate.Year; i++)
            {
                year = StartDate.AddYears(i).Year;
                maxMonth = Config.Calendar.GetMonthsInYear(year);
                for (int j = 0; j < maxMonth; j++)
                {
                    DateTime dateDebut = new DateTime(year, j + 1, 1, 0, 0, 0);
                    DateTime dateFin = dateDebut.AddMonths(1).AddSeconds(-1);

                    VMPeriode periode = Factory.GetPeriode(TypePeriode.Id, dateDebut, dateFin);
                    if (periode == null)
                    {
                        string designation = (year * 100 + j + 1).ToString();
                        periode = new VMPeriode()
                        {
                            Designation = designation,
                            Reference = TypePeriode.Designation + "_" + designation,
                            DateDebut = dateDebut,
                            DateFin = dateFin,
                            Quantite = (int)(dateFin - dateDebut).TotalSeconds + 1,
                            IdTypePeriode = TypePeriode.Id,
                            Alias = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(j + 1) + " - " + year,
                            Index = j + 1,
                            Active = Policy.IsMonthActive(dateDebut, dateFin),
                            Year = year
                        };
                        Factory.Periodes.Add(periode);
                    }
                    else
                        periode.Active = Policy.IsMonthActive(dateDebut, dateFin);
                    periode.Year = year;
                    //Progress.Report(periode.Alias);
                    Factory.SavePeriodes();
                }
            }
        }
    }
}
