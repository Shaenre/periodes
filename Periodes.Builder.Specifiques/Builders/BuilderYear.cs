﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder.Specifiques.Builders
{
    public class BuilderYear : Builder
    {
        [Dependency]
        public IFactoryVM Factory { get; set; }
        
        public override void Build()
        {
            int year = 0;
            for (int i = 0; i < EndDate.Year - StartDate.Year; i++)
            {
                year = StartDate.AddYears(i).Year;
                DateTime dateDebut = new DateTime(StartDate.AddYears(i).Year, 1, 1, 0, 0, 0);
                DateTime dateFin = dateDebut.AddYears(1).AddSeconds(-1);

                VMPeriode periode = Factory.GetPeriode(TypePeriode.Id, dateDebut, dateFin);
                if (periode == null)
                {
                    string designation = (year).ToString();
                    periode = new VMPeriode()
                    {
                        Designation = designation,
                        Reference = TypePeriode.Designation + "_" + designation,
                        DateDebut = dateDebut,
                        DateFin = dateFin,
                        Quantite = (int)(dateFin - dateDebut).TotalSeconds + 1,
                        IdTypePeriode = TypePeriode.Id,
                        Alias = "Année " + year,
                        Index = i + 1,
                        Active = Policy.IsYearActive(dateDebut, dateFin),
                        Year = year
                    };
                    Factory.Periodes.Add(periode);
                }
                else
                    periode.Active = Policy.IsYearActive(dateDebut, dateFin);
                periode.Year = year;
                //Progress.Report(periode.Alias);
                Factory.SavePeriodes();
            }
        }
    }
}
