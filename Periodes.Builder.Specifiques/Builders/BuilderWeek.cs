﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder.Specifiques.Builders
{
    public class BuilderWeek : Builder
    {
        [Dependency]
        public IFactoryVM Factory { get; set; }
        
        public override void Build()
        {
            int year, maxWeek = 0;
            for (int i = 0; i < EndDate.Year - StartDate.Year; i++)
            {
                year = StartDate.AddYears(i).Year;
                maxWeek = Config.Calendar.GetWeekOfYear(Config.LastDayOfYear(year), Config.CalendarWeekRule, Config.FirstDayOfWeek);
                DateTime firstDayFirstWeekOfYear = Config.FirstDayOfYear(year);
                for (int j = 0; j < maxWeek; j++)
                {
                    DateTime dateDebut = firstDayFirstWeekOfYear.AddDays(7 * j);
                    DateTime dateFin = dateDebut.AddDays(7).AddSeconds(-1);

                    VMPeriode periode = Factory.GetPeriode(TypePeriode.Id, dateDebut, dateFin);
                    if (periode == null)
                    {
                        string designation = (year * 100 + j + 1).ToString();
                        periode = new VMPeriode()
                        {
                            Designation = designation,
                            Reference = TypePeriode.Designation + "_" + designation,
                            DateDebut = dateDebut,
                            DateFin = dateFin,
                            Quantite = (int)(dateFin - dateDebut).TotalSeconds + 1,
                            IdTypePeriode = TypePeriode.Id,
                            Alias = "Semaine " + (j + 1) + " - " + year,
                            Index = j + 1,
                            Active = Policy.IsWeekActive(dateDebut, dateFin),
                            Year = year
                        };
                        Factory.Periodes.Add(periode);
                    }
                    else
                        periode.Active = Policy.IsWeekActive(dateDebut, dateFin);
                    periode.Year = year;
                    //Progress.Report(periode.Alias);
                }
                Factory.SavePeriodes();
            }
        }
    }
}
