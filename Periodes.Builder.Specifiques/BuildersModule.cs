﻿using Periodes.Builder.Specifiques.Builders;
using ID3iShared.Module;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder.Specifiques
{
    public class BuildersModule : Module
    {
        public override void Initialize()
        {
            Container.RegisterInstance<IBuilder>("YEAR", Container.Resolve<BuilderYear>(), new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IBuilder>("MONTH", Container.Resolve<BuilderMonth>(), new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IBuilder>("WEEK", Container.Resolve<BuilderWeek>(), new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IBuilder>("DAY", Container.Resolve<BuilderDay>(), new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IBuilder>("HALFDAY", Container.Resolve<BuilderHalfDay>(), new ContainerControlledLifetimeManager());
        }
    }
}
