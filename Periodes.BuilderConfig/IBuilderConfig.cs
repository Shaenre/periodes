﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.BuilderConfig
{
    public interface IBuilderConfig
    {
        /// <summary>
        /// Calendrier sur lequel sera basé le calcul des périodes.
        /// </summary>
        Calendar Calendar { get; }
        /// <summary>
        /// Règle déterminant la première semaine de l'année.
        /// </summary>
        CalendarWeekRule CalendarWeekRule { get; }
        /// <summary>
        /// Premier jour de la semaine.
        /// </summary>
        DayOfWeek FirstDayOfWeek { get; }
        /// <summary>
        /// Fonction qui permet pour une année donnée de renvoyer le PREMIER LUNDI de l'année.
        /// </summary>
        /// <param name="iYear">Année.</param>
        DateTime FirstDayOfYear(int year);
        /// <summary>
        /// Fonction qui permet pour une année donnée de renvoyer le DERNIER DIMANCHE de l'année.
        /// </summary>
        /// <param name="iYear">Année.</param>
        DateTime LastDayOfYear(int year);
    }
}
