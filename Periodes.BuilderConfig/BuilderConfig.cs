﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.BuilderConfig
{
    public abstract class BuilderConfig : IBuilderConfig
    {
        public abstract Calendar Calendar { get; }

        public abstract CalendarWeekRule CalendarWeekRule { get; }

        public abstract DayOfWeek FirstDayOfWeek { get; }

        public abstract DateTime FirstDayOfYear(int year);

        public abstract DateTime LastDayOfYear(int year);
    }
}
