﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Preferences
{
    public interface IPreferences
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }
}
