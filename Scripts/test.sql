;WITH MaxBefore(IdEmetteurBefore) AS
(
select MAX(CO_PERIODE.IDEMETTEUR) from CO_PERIODE
inner join PERIODE pe on CO_PERIODE.IdEmetteur = pe.ID_PERIODE
inner join PERIODE pr on CO_PERIODE.IdRecepteur = pr.ID_PERIODE
where pe.DATE_DEBUT <= '2017-03-25' and IdTPEmetteur = 6
)
,MinAfter(IdEmetteurAfter) AS
(select MIN(CO_PERIODE.IDEMETTEUR) from CO_PERIODE
inner join PERIODE pe on CO_PERIODE.IdEmetteur = pe.ID_PERIODE
inner join PERIODE pr on CO_PERIODE.IdRecepteur = pr.ID_PERIODE
where pe.DATE_FIN >= '2017-03-25' and IdTPEmetteur = 6)
select MaxBefore.*, CO_PERIODE.*, MinAfter.* from maxbefore, minAfter, CO_PERIODE
left outer join PERIODE pe on CO_PERIODE.IdEmetteur = pe.ID_PERIODE
left outer join PERIODE pr on CO_PERIODE.IdRecepteur = pr.ID_PERIODE
where pe.DATE_DEBUT <= '2017-03-25' and pe.DATE_FIN >= '2017-03-25' and IdTPEmetteur = 6