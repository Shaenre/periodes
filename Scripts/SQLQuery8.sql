DECLARE @IdPeriode INT -- Période de départ.
SET @IdPeriode = 17500

DECLARE @NbPeriodeAjout INT -- Nombre de période à ajouter.
SET @NbPeriodeAjout = 3

DECLARE @TypePeriodeAjout INT -- type de la période à ajouter.
SET @TypePeriodeAjout = 8

DECLARE @TypePeriodeSouhaitee INT -- type de la période à retourner.
SET @TypePeriodeSouhaitee = 4

DECLARE @IndexSouhaitee INT -- Index de la période à retourner dans le type de periode à retourner.
SET @IndexSouhaitee = 1

SELECT * FROM CO_PERIODE WHERE IDF = @IdPeriode

--SELECT * FROM CO_PERIODE 
--INNER JOIN (
--	SELECT (IdEmetteur + @NbPeriodeAjout) ID_PERIODE FROM CO_PERIODE WHERE IDF = @IdPeriode AND IdTPEmetteur = @TypePeriodeAjout
--) PeriodeFind ON CO_PERIODE.IdEmetteur = PeriodeFind.ID_PERIODE
--WHERE IndRelatif = @IndexSouhaitee AND IdTPRecepteur = @TypePeriodeSouhaitee

-- New méthode sans utiliser les ID_PERIODE, on se base sur les index qui sont plus sûr.
SELECT CO_PERIODE.* FROM CO_PERIODE WHERE IdTPEmetteur = @TypePeriodeAjout 
	AND IndEmetteur = (SELECT IndEmetteur FROM CO_PERIODE WHERE IDF = @IdPeriode AND IdTPEmetteur = @TypePeriodeAjout) + @NbPeriodeAjout 
	AND IdTPRecepteur = @TypePeriodeSouhaitee
	AND IndRelatif = @IndexSouhaitee