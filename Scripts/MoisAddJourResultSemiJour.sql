DECLARE @IdPeriode INT -- P�riode de d�part.
SET @IdPeriode = 13

DECLARE @NbPeriodeAjout INT -- Nombre de p�riode � ajouter.
SET @NbPeriodeAjout = 1

DECLARE @TypePeriodeAjout INT -- type de la p�riode � ajouter.
SET @TypePeriodeAjout = 4

DECLARE @TypePeriodeSouhaitee INT -- type de la p�riode � retourner.
SET @TypePeriodeSouhaitee = 5

DECLARE @IndexSouhaitee INT -- Index de la p�riode � retourner dans le type de periode � retourner.
SET @IndexSouhaitee = 40

SELECT * FROM CO_PERIODE WHERE IDF = @IdPeriode

-- P�riode moyenne.
SELECT CO_PERIODE.* FROM CO_PERIODE WHERE IdTPEmetteur = @TypePeriodeAjout 
	AND IndEmetteur = (SELECT Max(IndEmetteur) FROM CO_PERIODE WHERE IDF = @IdPeriode AND IdTPEmetteur = @TypePeriodeAjout) + @NbPeriodeAjout 
	AND IdTPRecepteur = @TypePeriodeSouhaitee
	AND IndRelatif = (CASE WHEN NbIndEmetteur % 2 = 0 THEN NbIndEmetteur / 2 ELSE (NbIndEmetteur / 2) + 1 END)

-- P�riode avec au moins le m�me index, sinon le moyen.
SELECT CO_PERIODE.* FROM CO_PERIODE WHERE IdTPEmetteur = @TypePeriodeAjout 
	AND IndEmetteur = (SELECT Max(IndEmetteur) FROM CO_PERIODE WHERE IDF = @IdPeriode AND IdTPEmetteur = @TypePeriodeAjout) + @NbPeriodeAjout 
	AND IdTPRecepteur = @TypePeriodeSouhaitee
	AND IndRelatif = (CASE WHEN @IndexSouhaitee <= NbIndEmetteur 
						THEN @IndexSouhaitee
						ELSE
							CASE WHEN NbIndEmetteur % 2 = 0 THEN NbIndEmetteur / 2 ELSE (NbIndEmetteur / 2) + 1 END END)
