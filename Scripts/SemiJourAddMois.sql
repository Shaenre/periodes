DECLARE @IdPeriode INT -- P�riode de d�part.
SET @IdPeriode = 21172

DECLARE @NbPeriodeAjout INT -- Nombre de p�riode � ajouter.
SET @NbPeriodeAjout = 7

DECLARE @TypePeriodeAjout INT -- type de la p�riode � ajouter.
SET @TypePeriodeAjout = 2

DECLARE @TypePeriodeSouhaitee INT -- type de la p�riode � retourner.
SET @TypePeriodeSouhaitee = 5

DECLARE @IndexSouhaitee INT -- Index de la p�riode � retourner dans le type de periode � retourner.
SET @IndexSouhaitee = 40

SELECT * FROM CO_PERIODE WHERE IDF = @IdPeriode

-- New m�thode sans utiliser les ID_PERIODE, on se base sur les index qui sont plus s�r.
SELECT CO_PERIODE.* FROM CO_PERIODE WHERE IdTPEmetteur = @TypePeriodeAjout 
	AND IndEmetteur = (SELECT Max(IndEmetteur) FROM CO_PERIODE WHERE IDF = @IdPeriode AND IdTPEmetteur = @TypePeriodeAjout) + @NbPeriodeAjout 
	AND IdTPRecepteur = @TypePeriodeSouhaitee
	AND IndRelatif = @IndexSouhaitee 

