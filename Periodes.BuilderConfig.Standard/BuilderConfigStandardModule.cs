﻿using ID3iShared.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace Periodes.BuilderConfig.Standard
{
    public class BuilderConfigStandardModule : Module
    {
        public override void Initialize()
        {
            Container.RegisterInstance<IBuilderConfig>(Container.Resolve<BuilderConfigStandard>());
        }
    }
}
