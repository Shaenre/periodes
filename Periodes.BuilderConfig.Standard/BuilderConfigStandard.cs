﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.BuilderConfig.Standard
{
    public class BuilderConfigStandard : BuilderConfig
    {
        public override Calendar Calendar => new GregorianCalendar();

        public override CalendarWeekRule CalendarWeekRule => CalendarWeekRule.FirstFourDayWeek;

        public override DayOfWeek FirstDayOfWeek => DayOfWeek.Monday;

        public override DateTime FirstDayOfYear(int year)
        {
            DateTime jan01 = new DateTime(year, 1, 1, 0, 0, 0);
            switch (jan01.DayOfWeek)
            {
                case DayOfWeek.Sunday: //Le 1 est un dimanche, on retourne le lundi qui arrive.
                    return 1.Days().After(jan01);
                case DayOfWeek.Monday: //Le 1 est un lundi pas de soucis.
                    return jan01;
                case DayOfWeek.Tuesday: //Le 1 est un mardi, on retourne le lundi d'avant.
                    return 1.Days().Before(jan01);
                case DayOfWeek.Wednesday: //Le 1 est un mercredi, on retourne le lundi d'avant.
                    return 2.Days().Before(jan01);
                case DayOfWeek.Thursday: //Le 1 est un jeudi, on retourne le lundi d'avant.
                    return 3.Days().Before(jan01);
                case DayOfWeek.Friday: //Le 1 est un vendredi, on retourne le lundi qui arrive.
                    return 3.Days().After(jan01);
                case DayOfWeek.Saturday: //Le 1 est un samedi, on retourne le lundi qui arrive.
                    return 2.Days().After(jan01);
                default:
                    return jan01;
            }
        }

        public override DateTime LastDayOfYear(int year)
        {
            DateTime firstDayOfYear = FirstDayOfYear(year);
            DateTime dec31 = new DateTime(year, 12, 31, 0, 0, 0);
            DateTime lastDayWeek52 = new DateTime();
            switch (dec31.DayOfWeek)
            {
                case DayOfWeek.Sunday: //Le 31 est un dimanche pas de soucis.
                    lastDayWeek52 = dec31;
                    break;
                case DayOfWeek.Monday: //Le 31 est un lundi, on retourne le dimanche d'avant.
                    lastDayWeek52 = 1.Days().Before(dec31);
                    break;
                case DayOfWeek.Tuesday: //Le 31 est un mardi, on retourne le dimanche d'avant.
                    lastDayWeek52 = 2.Days().Before(dec31);
                    break;
                case DayOfWeek.Wednesday: //Le 31 est un mercredi, on retourne le dimanche d'avant.
                    lastDayWeek52 = 3.Days().Before(dec31);
                    break;
                case DayOfWeek.Thursday: //Le 31 est un jeudi, on retourne le dimanche d'avant.
                    lastDayWeek52 = 4.Days().Before(dec31);
                    break;
                case DayOfWeek.Friday:
                    if (firstDayOfYear.DayOfWeek == DayOfWeek.Thursday & DateTime.IsLeapYear(year)) //Le 31 est un vendredi et l'année à commencée par un jeudi et est bissextile, on retourne le dimanche d'avant.
                        lastDayWeek52 = 5.Days().Before(dec31);
                    else //Le 31 est un vendredi mais l'année n'est pas spéciale, on retourne le dimanche qui arrive.
                        lastDayWeek52 = 2.Days().After(dec31);
                    break;
                case DayOfWeek.Saturday: //Le 31 est un samedi, on retourne le dimanche qui arrive.
                    lastDayWeek52 = 1.Days().After(dec31);
                    break;
                default:
                    break;
            }
            if (lastDayWeek52 < dec31) //Si la semaine 52 se termine avant le 31 décembre, soit il y a 53 semaines soit la semaine 1 de l'année suivante commence ce mois-ci.
            {
                int nbJour = (dec31 - lastDayWeek52).Days;
                if (nbJour > 3) //Il y a plus de 4 jours qui séparent la fin de la semaine 52 et le 31, il y a donc une 53ème semaine, on retourne le dimanche de la 53ème.
                    return lastDayWeek52.WeekAfter();
                else //Il y au plus 3 jours qui séparent la fin de la semaine 52 du 31, l'année suivante commence donc dans cette année, on retourne le dimanche calculé.
                    return lastDayWeek52;
            }
            else //La fin de la semaine 52 se termine dans l'année suivante, on retourne le dimanche calculé.
                return lastDayWeek52;
        }
    }
}
