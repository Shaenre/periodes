﻿using Periodes.Data.Factory;
using Periodes.Data.WebService.Factory;
using ID3iShared.Module;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.WebService
{
    public class WebServiceFactoryModule : Module
    {
        public override void Initialize()
        {
            Container.RegisterInstance<IFactoryVM>(Container.Resolve<FactoryVM>(), new ContainerControlledLifetimeManager());
        }
    }
}
