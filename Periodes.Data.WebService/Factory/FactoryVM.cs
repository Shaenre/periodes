﻿using Periodes.WebService.Client;
using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Periodes.WebService.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.WebService.Factory
{
    public class FactoryVM : FactoryVMBase
    {
        private AdminWrapper AdminWrapper { get; set; } = new AdminWrapper();

        public FactoryVM(IUnityContainer container) : base(container)
        {
            Precharger();
        }

        public override int SaveChanges() => 0;
        
        public override void InitActionsUtilisateur()
        {
            InitActions<VMUtilisateur>(
                get: () => new List<VMUtilisateur>(),
                set: null,
                del: null);
        }

        public override void InitActionsTypePeriode()
        {
            InitActions<VMTypePeriode>(
                get: () =>
                {
                    var result = AdminWrapper.Client.TypePeriode.All.Call();
                    if (result.IsRight)
                        return result.Right.Select(x => TypePeriodeModelToVM(x)).AsQueryable();
                    return new List<VMTypePeriode>();
                },
                set: null,
                del: null);
        }
        public override int OnSaveTypesPeriodes()
        {
            var nb = TypesPeriodes.ModifiedItems.Count;
            TypesPeriodes.ModifiedItems.ForEach(x => AdminWrapper.Client.TypePeriode.Register.Call(VMTypePeriodeToModel(x)));
            return nb;
        }
        private TypePeriodeModel VMTypePeriodeToModel(VMTypePeriode vm)
        {
            if (vm == null)
                return null;
            return new TypePeriodeModel()
            {
                Id = vm.Id,
                Code = vm.Code,
                Designation = vm.Designation,
                EstReference = vm.EstReference,
                Index = vm.Index,
                TypeReference = vm.IdTypePeriodeReference.HasValue ? VMTypePeriodeToModel(TypesPeriodes.FirstOrDefault(x => x.IdTypePeriodeReference == vm.IdTypePeriodeReference)) : null
            };
        }
        private VMTypePeriode TypePeriodeModelToVM(TypePeriodeModel model)
        {
            if (model == null)
                return null;
            return new VMTypePeriode()
            {
                Id = model.Id,
                Code = model.Code,
                Designation = model.Designation,
                EstReference = model.EstReference,
                Index = model.Index,
                IdTypePeriodeReference = model.TypeReference?.Id
            };
        }

        public override void InitActionsPeriode()
        {
            InitActions<VMPeriode>(
                get: () =>
                {
                    IQueryable<VMPeriode> query = new List<VMPeriode>().AsQueryable();
                    foreach (var item in TypesPeriodes)
                    {
                        var result = AdminWrapper.Client.Periode.Some.Call(item.Code);
                        if (result.IsRight)
                        {
                            if (query == null)
                                query = result.Right.Select(x => PeriodeModelFullToVM(x)).AsQueryable();
                            else
                                query = query.Concat(result.Right.Select(x => PeriodeModelFullToVM(x)).AsQueryable());
                        }
                    }
                    return query;
                },
                set: null,
                del: null);
        }
        public override int OnSavePeriodes()
        {
            var nb = Periodes.ModifiedItems.Count;
            AdminWrapper.Client.Periode.MassSave.Call(Periodes.ModifiedItems.Select(x => VMPeriodeToModel(x)).ToList());
            return nb;
        }
        private VMPeriode PeriodeModelFullToVM(PeriodeModelFull model)
        {
            if (model == null)
                return null;
            return new VMPeriode()
            {
                Id = model.Id,
                IdTypePeriode = model.IdTypePeriode,
                Reference = model.Reference,
                Designation = model.Designation,
                Quantite = model.Quantite,
                Alias = model.Alias,
                DateDebut = model.DateDebut,
                DateFin = model.DateFin,
                Index = model.Index,
                Active = model.Active,
                Year = model.Year
            };
        }
        private PeriodeModelFull VMPeriodeToModel(VMPeriode vm)
        {
            if (vm == null)
                return null;
            return new PeriodeModelFull()
            {
                Id = vm.Id,
                IdTypePeriode = vm.IdTypePeriode,
                Reference = vm.Reference,
                Designation = vm.Designation,
                Quantite = vm.Quantite,
                Alias = vm.Alias,
                DateDebut = vm.DateDebut,
                DateFin = vm.DateFin,
                Index = vm.Index,
                Active = vm.Active,
                Year = vm.Year
            };
        }
    }
}
