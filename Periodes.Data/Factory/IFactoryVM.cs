﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Data.ViewModel;

namespace Periodes.Data.Factory
{
    public interface IFactoryVM : INotifyPropertyChanged
    {
        void InitActions();
        void Remove<T>(T elem);
        void Set<T>(T elem);
        T Get<T>();
        int SaveChanges();
        void Precharger();
        bool ResetAll();

        #region Utilisateurs
        VMCollection<VMUtilisateur> Utilisateurs { get; }
        int SaveUtilisateurs();
        void InitActionsUtilisateur();
        #endregion

        VMCollection<VMTypePeriode> TypesPeriodes { get; }
        int SaveTypesPeriodes();
        void InitActionsTypePeriode();

        VMCollection<VMPeriode> Periodes { get; set; }
        int SavePeriodes();
        void InitActionsPeriode();
    }

    public static class FactoryExtensions
    {
        public static VMPeriode GetPeriode(this IFactoryVM @this, int idTypePeriode, DateTime dateDebut, DateTime dateFin)
        {
            return @this.Periodes.FirstOrDefault(x => x.IdTypePeriode == idTypePeriode && x.DateDebut == dateDebut && x.DateFin == dateFin);
        }
        public static VMTypePeriode GetTypePeriode(this IFactoryVM @this, int idTypePeriode)
        {
            return @this.TypesPeriodes.FirstOrDefault(x => x.Id == idTypePeriode);
        }
    }
}
