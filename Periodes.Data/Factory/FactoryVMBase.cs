﻿using ID3iCore;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Data.ViewModel;
using ID3iShared.Logger;

namespace Periodes.Data.Factory
{
    public abstract class FactoryVMBase : VMBindableBase, IFactoryVM
    {
        protected virtual Dictionary<Type, Action<object>> DelActions { get; } = new Dictionary<Type, Action<object>>();
        protected virtual Dictionary<Type, Action<object>> SetActions { get; } = new Dictionary<Type, Action<object>>();
        protected virtual Dictionary<Type, Func<object>> GetActions { get; } = new Dictionary<Type, Func<object>>();

        protected IUnityContainer Container { get; set; }
        
        public FactoryVMBase(IUnityContainer container)
        {
            InitActions();
            Container = container;
        }

        public virtual bool ResetAll()
        {
            Utilisateurs.Reset();
            TypesPeriodes.Reset();
            Periodes.Reset();
            return true;
        }
        public void InitActions()
        {
            InitActionsUtilisateur();
            InitActionsTypePeriode();
            InitActionsPeriode();
        }

        protected virtual void InitActions<T>(Func<object> get, Action<object> set = null, Action<object> del = null)
        {
            if (get != null) GetActions.Add(typeof(IEnumerable<T>), get);
            if (set != null) SetActions.Add(typeof(T), set);
            if (del != null) DelActions.Add(typeof(T), del);
        }

        public void Remove<T>(T elem) => DelActions.GetOrDefault(typeof(T))?.Invoke(elem);

        public void Set<T>(T elem) => SetActions.GetOrDefault(typeof(T))?.Invoke(elem);

        public T Get<T>() => (T)GetActions.GetOrDefault(typeof(T))?.Invoke();

        public abstract int SaveChanges();

        private int Save<T>(VMCollection<T> collection, Func<int> onSave = null) where T : INotifyPropertyChanged
        {
            collection.DeletedItems.ForEach(Remove);
            collection.ModifiedItems.ForEach(Set);

            int num = 0;

            try { num = onSave == null ? SaveChanges() : onSave(); }
            catch (Exception ex) { Container.Resolve<IExceptionLoggerFacade>().Log(ex); }

            if (num > 0) collection.Reset();

            return num;
        }
        public void Precharger()
        {
            object o = null;
            o = TypesPeriodes;
            o = Periodes;
        }

        private VMCollection<T> GetVMCollection<T>() where T : INotifyPropertyChanged
            => new VMCollection<T>(Get<IEnumerable<T>>);

        private VMCollection<VMUtilisateur> utilisateurs = null;
        public VMCollection<VMUtilisateur> Utilisateurs
        {
            get
            {
                if (utilisateurs == null)
                    utilisateurs = GetVMCollection<VMUtilisateur>();
                return utilisateurs;
            }
            set { SetProperty(ref utilisateurs, value); }
        }
        public virtual int SaveUtilisateurs() => Save(Utilisateurs);
        public abstract void InitActionsUtilisateur();

        private VMCollection<VMTypePeriode> typesPeriodes = null;
        public VMCollection<VMTypePeriode> TypesPeriodes
        {
            get
            {
                if (typesPeriodes == null)
                    typesPeriodes = GetVMCollection<VMTypePeriode>();
                return typesPeriodes;
            }
            set { SetProperty(ref typesPeriodes, value); }
        }
        public int SaveTypesPeriodes() => Save(TypesPeriodes, OnSaveTypesPeriodes);
        public virtual int OnSaveTypesPeriodes() => SaveChanges();
        public abstract void InitActionsTypePeriode();

        private VMCollection<VMPeriode> periodes = null;
        public VMCollection<VMPeriode> Periodes
        {
            get
            {
                if (periodes == null)
                    periodes = GetVMCollection<VMPeriode>();
                return periodes;
            }
            set { SetProperty(ref periodes, value); }
        }
        public int SavePeriodes() => Save(Periodes, OnSavePeriodes);
        public virtual int OnSavePeriodes() => SaveChanges();
        public abstract void InitActionsPeriode();
    }
}
