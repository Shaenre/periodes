﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.ViewModel
{
    public class VMUtilisateur : VMBindableBase
    {
        private int idUtilisateur;
        private string login;
        private string nom;
        private string prenom;

        public int IdUtilisateur { get => idUtilisateur; set => SetProperty(ref idUtilisateur, value); }
        public string Login { get => login; set => SetProperty(ref login, value); }
        public string Nom { get => nom; set => SetProperty(ref nom, value); }
        public string Prenom { get => prenom; set => SetProperty(ref prenom, value); }
    }
}
