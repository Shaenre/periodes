﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.ViewModel
{
    public class VMTypePeriode : VMBindableBase
    {
        private int id;
        public int Id { get => id; set => SetProperty(ref id, value); }

        private int? idTypePeriodeReference;
        public int? IdTypePeriodeReference { get => idTypePeriodeReference; set => SetProperty(ref idTypePeriodeReference, value); }

        private string designation;
        public string Designation { get => designation; set => SetProperty(ref designation, value); }

        private int index;
        public int Index { get => index; set => SetProperty(ref index, value); }

        private bool estReference;
        public bool EstReference { get => estReference; set => SetProperty(ref estReference, value); }

        private string code;
        public string Code { get => code; set => SetProperty(ref code, value); }
    }
}
