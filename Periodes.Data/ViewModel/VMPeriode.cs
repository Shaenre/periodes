﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.ViewModel
{
    public class VMPeriode : VMBindableBase
    {
        private int id;
        public int Id { get => id; set => SetProperty(ref id, value); }

        private int idTypePeriode;
        public int IdTypePeriode { get => idTypePeriode; set => SetProperty(ref idTypePeriode, value); }

        private string reference;
        public string Reference { get => reference; set => SetProperty(ref reference, value); }

        private string designation;
        public string Designation { get => designation; set => SetProperty(ref designation, value); }

        private int quantite;
        public int Quantite { get => quantite; set => SetProperty(ref quantite, value); }

        private string alias;
        public string Alias { get => alias; set => SetProperty(ref alias, value); }

        private DateTime dateDebut;
        public DateTime DateDebut { get => dateDebut; set => SetProperty(ref dateDebut, value); }

        private DateTime dateFin;
        public DateTime DateFin { get => dateFin; set => SetProperty(ref dateFin, value); }

        private int index;
        public int Index { get => index; set => SetProperty(ref index, value); }

        private bool active;
        public bool Active { get => active; set => SetProperty(ref active, value); }

        private int year;
        public int Year { get => year; set => SetProperty(ref year, value); }
    }
}
