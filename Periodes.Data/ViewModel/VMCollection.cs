﻿using ID3iCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Data.ViewModel
{
    public class VMCollection<T> : ObservableCollection<T> where T : INotifyPropertyChanged
    {
        List<T> OriginalItems { get; set; } = new List<T>();
        public IEnumerable<T> DeletedItems => OriginalItems.Except(this);
        public List<T> ModifiedItems { get; set; } = new List<T>();

        private Func<IEnumerable<T>> loadFunction;
        private Func<IEnumerable<T>> LoadFunction
        {
            get => loadFunction;
            set
            {
                if (loadFunction != value)
                {
                    loadFunction = value;
                    Reset();
                }
            }
        }
        public VMCollection() => CollectionChanged += VMCollection_CollectionChanged;
        public VMCollection(Func<IEnumerable<T>> loadFunction) : this()
        {
            LoadFunction = loadFunction;
        }
        ~VMCollection()
        {
            this.ForEach(x => x.PropertyChanged -= Item_PropertyChanged);
            OriginalItems.Clear();
            CollectionChanged -= VMCollection_CollectionChanged;
        }
        public new void Clear()
        {
            this.ForEach(x => x.PropertyChanged -= Item_PropertyChanged);
            base.Clear();
        }
        public void Reset()
        {
            OriginalItems.Clear();
            OriginalItems.AddRange(LoadFunction?.Invoke());
            Clear();
            OriginalItems.ForEach(Add);
            ModifiedItems.Clear();
        }

        private void VMCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Move)
            {
                e.OldItems?.Cast<T>().ForEach((x) => x.PropertyChanged -= Item_PropertyChanged);
                e.NewItems?.Cast<T>().ForEach((x) =>
                {
                    x.PropertyChanged += Item_PropertyChanged;
                    Item_PropertyChanged(x, null);
                });
                if (e.Action == NotifyCollectionChangedAction.Remove)
                    e.OldItems.Cast<T>().ForEach((x) => ModifiedItems.Remove(x));
            }
        }
        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!ModifiedItems.Contains((T)sender))
                ModifiedItems.Add((T)sender);
        }
    }
}
