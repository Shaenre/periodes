﻿using Periodes.Gestion.View;
using Periodes.Global;
using ID3iShared.Module;
using ID3iShared.Regions;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Periodes.Gestion
{
    public class GestionModule : Module
    {
        public override void Initialize()
        {
            Container.RegisterInstance(Container.Resolve<GestionController>(), new ContainerControlledLifetimeManager());

            Container.RegisterTypeForNavigation<LayoutGestionView>();

            RegionManager.RegisterViewWithRegion(RegionNames.MenuRegion, () => new ViewWithName(new RadRadioButton()
            {
                Content = "Gestion",
                Command = Container.Resolve<IApplicationCommands>().NavigateCommand,
                CommandParameter = nameof(LayoutGestionView)
            }, $"RadRadioButton_{nameof(LayoutGestionView)}_{new Random().Next()}"));
        }
    }
}
