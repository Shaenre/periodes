﻿using Periodes.Global;
using Periodes.Policy;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion
{
    public class GestionController : BindableBase
    {
        [Dependency]
        public IApplicationCommands ApplicationCommands { get; set; }
        public string ModuleActif { get; set; }
        public IPolicy Policy { get; set; }
        public GestionController() { }
    }
}
