﻿using Periodes.Builder;
using Periodes.BuilderConfig;
using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Periodes.Global.ViewModel;
using Periodes.Preferences;
using ID3iShared.Command;
using Microsoft.Practices.Unity;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Periodes.Gestion.ViewModel
{
    public class LayoutGestionViewModel : ViewModelBase
    {
        [Dependency]
        public IUnityContainer Container { get; set; }
        [Dependency]
        public IFactoryVM Factory { get; set; }
        private GestionController Controller { get; set; }
        public DelegateCommand<Enum> SetTypePeriodeCommand { get; set; }
        public AutoDelegateCommand GenererCommand { get; set; }

        private IPreferences Preferences { get; set; }

        public VMTypePeriode SelectedTypePeriode { get; set; }
        public ICollectionView Periodes => CollectionViewSource.GetDefaultView(Factory.Periodes);

        public LayoutGestionViewModel(GestionController controller)
        {
            Controller = controller;
            SetTypePeriodeCommand = new DelegateCommand<Enum>(SetTypePeriodeExecute);
            GenererCommand = new AutoDelegateCommand(GenererExecute, CanGenererExecute);

            AddActivableCommand(Controller.ApplicationCommands.GenererCommand, GenererCommand);
        }
        ~LayoutGestionViewModel()
        {
            RemoveActivableCommand(Controller.ApplicationCommands.GenererCommand, GenererCommand);
        }
        private void SetTypePeriodeExecute(Enum val)
        {
            SelectedTypePeriode = Factory.TypesPeriodes.FirstOrDefault(x => x.Code.ToUpper() == val.ToString().ToUpper());
            Periodes.Filter = x => ((VMPeriode)x).IdTypePeriode == SelectedTypePeriode.Id;
            Periodes.Refresh();
        }
        private void GenererExecute()
        {
            string codeTypePeriodeReference = SelectedTypePeriode.Code;
            if (!SelectedTypePeriode.EstReference)
                codeTypePeriodeReference = Factory.GetTypePeriode(SelectedTypePeriode.IdTypePeriodeReference.GetValueOrDefault()).Code;

            Container.Resolve<IBuilder>(codeTypePeriodeReference)
                .Setup(Container.Resolve<IBuilderConfig>(), Controller.Policy)
                .Configure(SelectedTypePeriode, Preferences.StartDate, Preferences.EndDate)
                .Build();
        }
        private bool CanGenererExecute()
        {
            if (Container.IsRegistered<IPreferences>($"{Controller.ModuleActif}_Preferences"))
                Preferences = Container.Resolve<IPreferences>($"{Controller.ModuleActif}_Preferences");
            else
                Preferences = null;
            return SelectedTypePeriode != null && Preferences != null && Preferences.StartDate < Preferences.EndDate;
        }
    }
}
