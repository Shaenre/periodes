﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.Standard.Data
{
    public enum TypePeriodeReference { NONE, YEAR, MONTH, WEEK, DAY, HALFDAY }
}
