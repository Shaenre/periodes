﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Policy
{
    public class DefaultPolicy : IPolicy
    {
        public bool IsYearActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsMonthActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsWeekActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek) => true;

        public string DaySuffixeAlias(DateTime dateDebut) => "";

        public bool IsHalfDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek, bool isAM) => true;

        public string HalfDaySuffixeAlias(DateTime dateDebut) => "";
    }
}
