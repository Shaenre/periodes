﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Policy
{
    public interface IPolicy
    {
        /// <summary>
        /// Méthode qui permet de déterminer si l'année doit être marquée comme active ou pas.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        /// <param name="dateFin">Date de fin de la période.</param>
        bool IsYearActive(DateTime dateDebut, DateTime dateFin);
        /// <summary>
        /// Méthode qui permet de déterminer si le mois doit être marqué comme actif ou pas.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        /// <param name="dateFin">Date de fin de la période.</param>
        bool IsMonthActive(DateTime dateDebut, DateTime dateFin);
        /// <summary>
        /// Méthode qui permet de déterminer si la semaine doit être marquée comme active ou pas.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        /// <param name="dateFin">Date de fin de la période.</param>
        bool IsWeekActive(DateTime dateDebut, DateTime dateFin);
        /// <summary>
        /// Méthode qui permet de déterminer si le jour doit être marqué comme actif ou pas.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        /// <param name="dateFin">Date de fin de la période.</param>
        /// <param name="dayOfWeek">DayOfWeek correspondant aux dates.</param>
        bool IsDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek);
        /// <summary>
        /// Méthode qui permet d'ajouter du texte supplémentaire à l'alias d'un jour.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        string DaySuffixeAlias(DateTime dateDebut);
        /// <summary>
        /// Méthode qui permet de déterminer si le semi-jour doit être marqué comme actif ou pas.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        /// <param name="dateFin">Date de fin de la période.</param>
        /// <param name="dayOfWeek">DayOfWeek correspondant aux dates.</param>
        /// <param name="isAM">Indication pour savoir si on est le matin ou l'après-midi.</param>
        bool IsHalfDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek, bool isAM);
        /// <summary>
        /// Méthode qui permet d'ajouter du texte supplémentaire à l'alias d'un semi-jour.
        /// </summary>
        /// <param name="dateDebut">Date de début de la période.</param>
        string HalfDaySuffixeAlias(DateTime dateDebut);
    }
}
