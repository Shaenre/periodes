﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3iShared.Popup;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Periodes.Global.Wrapper
{
    public class RadWindowWrapper : IWindowPopup
    {
        private RadWindow window;
        public DependencyObject DependencyWindow => window;

        public object DataContext { get => window.DataContext; set => window.DataContext = value; }
        public string Title { get => window.Header.ToString(); set => window.Header = value; }
        public event EventHandler ContentRendered
        {
            add { window.Loaded += (new RoutedEventHandler(value)); }
            remove { window.Loaded -= (new RoutedEventHandler(value)); }
        }
        public event EventHandler Closed
        {
            add { window.Closed += (new EventHandler<WindowClosedEventArgs>(value)); }
            remove { window.Closed -= (new EventHandler<WindowClosedEventArgs>(value)); }
        }
        public IWindowPopup Get()
        {
            window = new RadWindow();
            return this;
        }

        public object Content { get => window.Content; set => window.Content = value; }

        public void Close()
        {
            window.Close();
        }

        public SizeToContent SizeToContent { get; set; }
        public ContentControl Owner { get => window.Owner; set => window.Owner = value; }

        public void Show()
        {
            window.Show();
        }

        public void ShowDialog()
        {
            window.ShowDialog();
        }

        public event SizeChangedEventHandler SizeChanged
        {
            add { window.SizeChanged += value; }
            remove { window.SizeChanged -= value; }
        }

        public double Top { get => window.Top; set => window.Top = value; }
        public double Left { get => window.Left; set => window.Left = value; }
        public double ActualHeight { get => window.ActualHeight; }
        public double ActualWidth { get => window.ActualWidth; }
        public Style Style { get => window.Style; set => window.SetValue(FrameworkElement.StyleProperty, value); }
    }
}
