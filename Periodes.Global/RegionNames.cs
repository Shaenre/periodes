﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Global
{
    public class RegionNames
    {
        public static string MainRegion => nameof(MainRegion);
        public static string MenuRegion => nameof(MenuRegion);
        public static string GestionRegion => nameof(GestionRegion);
        public static string TypePeriodeRegion => nameof(TypePeriodeRegion);
        public static string SettingsRegion => nameof(SettingsRegion);
        public static string ThemeRegion => nameof(ThemeRegion);
    }
}
