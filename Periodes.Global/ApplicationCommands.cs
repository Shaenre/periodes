﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Global
{
    public static class ApplicationCommands
    {
        public static CompositeCommand SaveCommand = new CompositeCommand(true);
        public static CompositeCommand NavigateCommand = new CompositeCommand();
        public static CompositeCommand NavigateToRegionCommand = new CompositeCommand(true);
        public static CompositeCommand GoBackCommand = new CompositeCommand();
        public static CompositeCommand GenererCommand = new CompositeCommand(true);
    }
    public interface IApplicationCommands
    {
        CompositeCommand SaveCommand { get; }
        CompositeCommand NavigateCommand { get; }
        CompositeCommand NavigateToRegionCommand { get; }
        CompositeCommand GoBackCommand { get; }
        CompositeCommand GenererCommand { get; }
    }
    public class ApplicationCommandsProxy : IApplicationCommands
    {
        public CompositeCommand SaveCommand => ApplicationCommands.SaveCommand;

        public CompositeCommand NavigateCommand => ApplicationCommands.NavigateCommand;

        public CompositeCommand NavigateToRegionCommand => ApplicationCommands.NavigateToRegionCommand;

        public CompositeCommand GoBackCommand => ApplicationCommands.GoBackCommand;

        public CompositeCommand GenererCommand => ApplicationCommands.GenererCommand;
    }
}
