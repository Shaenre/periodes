﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3iShared;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;

namespace Periodes.Global.ViewModel
{
    public class NotificationPopupViewModel : NotifyPropertyChangedBase, IConfirmation
    {
        private bool confirmed;
        private string title;
        private object content;
        private bool close;
        private DialogResult dialogResult;

        public DelegateCommand OkCommand { get; private set; }
        public DelegateCommand AnnulerCommand { get; private set; }
        public DelegateCommand OuiCommand { get; private set; }
        public DelegateCommand NonCommand { get; private set; }

        public Action<NotificationPopupViewModel> DoOnOk { get; set; }
        public Action<NotificationPopupViewModel> DoOnAnnuler { get; set; }
        public Action<NotificationPopupViewModel> DoOnOui { get; set; }
        public Action<NotificationPopupViewModel> DoOnNon { get; set; }

        public MessageBoxButtons MessageBoxButtons { get; set; }

        public bool Confirmed { get => confirmed; set => SetProperty(ref confirmed, value); }
        public string Title { get => title; set => SetProperty(ref title, value); }
        public object Content { get => content; set => SetProperty(ref content, value); }
        public bool Close { get => close; set => SetProperty(ref close, value); }
        public DialogResult DialogResult { get => dialogResult; private set => SetProperty(ref dialogResult, value); }

        public Visibility OkButtonVisibility { get => MessageBoxButtons == MessageBoxButtons.OK || MessageBoxButtons == MessageBoxButtons.OKCancel ? Visibility.Visible : Visibility.Collapsed; }
        public Visibility AnnulerButtonVisibility { get => MessageBoxButtons == MessageBoxButtons.OKCancel || MessageBoxButtons == MessageBoxButtons.RetryCancel || MessageBoxButtons == MessageBoxButtons.YesNoCancel ? Visibility.Visible : Visibility.Collapsed; }
        public Visibility OuiButtonVisibility { get => MessageBoxButtons == MessageBoxButtons.YesNo || MessageBoxButtons == MessageBoxButtons.YesNoCancel ? Visibility.Visible : Visibility.Collapsed; }
        public Visibility NonButtonVisibility { get => MessageBoxButtons == MessageBoxButtons.YesNo || MessageBoxButtons == MessageBoxButtons.YesNoCancel ? Visibility.Visible : Visibility.Collapsed; }

        public NotificationPopupViewModel()
        {
            OkCommand = new DelegateCommand(() => { DoOnOk?.Invoke(this); DialogResult = DialogResult.OK; });
            AnnulerCommand = new DelegateCommand(() => { DoOnAnnuler?.Invoke(this); DialogResult = DialogResult.Cancel; });
            OuiCommand = new DelegateCommand(() => { DoOnOui?.Invoke(this); DialogResult = DialogResult.Yes; });
            NonCommand = new DelegateCommand(() => { DoOnNon?.Invoke(this); DialogResult = DialogResult.No; });

            PropertyChanged += NotificationPopupViewModel_PropertyChanged;
        }

        private void NotificationPopupViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(DialogResult):
                    Close = true;
                    break;
            }
        }
    }
}
