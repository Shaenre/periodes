﻿using Prism;
using Prism.Commands;
using Prism.Regions;
using ID3iShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Global.ViewModel
{
    public class ViewModelBase : NotifyPropertyChangedBase, INavigationAware, IActiveAware, IConfirmNavigationRequest
    {
        public virtual void OnNavigatedTo(NavigationContext navigationContext) { }
        public virtual bool IsNavigationTarget(NavigationContext navigationContext) => navigationContext.NavigationService.Journal.CurrentEntry.Uri != navigationContext.Uri;
        public virtual void OnNavigatedFrom(NavigationContext navigationContext) { }

        private bool isActive;
        public bool IsActive
        {
            get => isActive;
            set => SetProperty(ref isActive, value, null, () => { OnIsActiveChanged(); });
        }
        public event EventHandler IsActiveChanged;
        public virtual void OnIsActiveChanged() => IsActiveChanged?.Invoke(this, new EventArgs());

        public ViewModelBase() => IsActiveChanged += ViewModelBase_IsActiveChanged;
        private void ViewModelBase_IsActiveChanged(object sender, EventArgs e) => ActivableCommands.ForEach(x => x.IsActive = IsActive);

        private List<DelegateCommandBase> ActivableCommands = new List<DelegateCommandBase>();
        protected void AddActivableCommand(CompositeCommand compositeCommand, DelegateCommandBase command)
        {
            ActivableCommands.Add(command);
            compositeCommand.RegisterCommand(command);
        }
        protected void RemoveActivableCommand(CompositeCommand compositeCommand, DelegateCommandBase command)
        {
            if (command != null)
            {
                compositeCommand.UnregisterCommand(command);
                ActivableCommands.Remove(command);
            }
        }
        public void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback) => continuationCallback?.Invoke(IsNavigationTarget(navigationContext));
    }
}
