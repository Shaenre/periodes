﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Global.ViewModel;
using Prism.Interactivity.InteractionRequest;

namespace Periodes.Global
{
    public static class ApplicationRequests
    {
        public static InteractionRequest<NotificationPopupViewModel> NotificationPopupRequest { get; } = new InteractionRequest<NotificationPopupViewModel>();
    }

    public interface IApplicationRequests
    {
        InteractionRequest<NotificationPopupViewModel> NotificationPopupRequest { get; }
    }
    public class ApplicationRequestsProxy : IApplicationRequests
    {
        public InteractionRequest<NotificationPopupViewModel> NotificationPopupRequest => ApplicationRequests.NotificationPopupRequest;
    }
}
