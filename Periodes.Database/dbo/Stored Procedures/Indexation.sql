﻿CREATE PROCEDURE [dbo].[Indexation]
AS
BEGIN
IF OBJECT_ID('TEMPDB..#CalculIndexPeriode') IS NOT NULL drop table [#CalculIndexPeriode]
IF OBJECT_ID('TEMPDB..#LienPeriode') IS NOT NULL drop table [#LienPeriode]
IF OBJECT_ID('TEMPDB..#LienPeriode2') IS NOT NULL drop table [#LienPeriode2]
IF OBJECT_ID('TEMPDB..#LienPeriode3') IS NOT NULL drop table [#LienPeriode3]

-- Mise à jour des index sur la table periode.
UPDATE P SET P.IDX = PIdx.Idx 
	FROM PERIODE P 
	INNER JOIN (SELECT ID_PERIODE, ROW_NUMBER() OVER (PARTITION BY P.ID_TYPE_PERIODE ORDER BY P.DESIGNATION) Idx 
				FROM PERIODE P WHERE P.ACTIVE = 1) PIdx 
	ON P.ID_PERIODE = PIdx.ID_PERIODE

-- Mise à -1 des index pour les périodes inactives.
UPDATE P SET P.IDX = -1 FROM PERIODE P WHERE ACTIVE = 0


 CREATE TABLE #CalculIndexPeriode(
              IdTP INT NOT NULL,
              IDP INT NOT NULL,
              IndP INT NULL,
              IdTPppdc INT NULL,
              IdTPppdc2 INT NULL,
              MaxInd INT NULL,
              MinInd INT NULL,
              NbInd INT NULL)
 CREATE CLUSTERED INDEX Idx_IdTP_IndP ON #CalculIndexPeriode
(IdTP ASC, IndP ASC, idTPppdc asc)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

CREATE NONCLUSTERED INDEX Idx_IdTP_IdTPppdc ON #CalculIndexPeriode
(IdTP, IdTPppdc) INCLUDE (IDP)

;WITH PeriodeOnMultipleYear(IdTP, IDP, IndP, IdTPppdc, MaxInd, MinInd, NbInd)
AS (
	SELECT PBase.ID_TYPE_PERIODE, PBase.ID_PERIODE, PBase.IDX, P.ID_TYPE_PERIODE,
			MAX(P.IDX) MaxInd, MIN(P.IDX) MinInd, MAX(P.IDX) - MIN(P.IDX) + 1 NBind
	FROM PERIODE PBase
	INNER JOIN PERIODE P ON P.DATE_DEBUT >= PBase.DATE_DEBUT AND P.DATE_FIN <= PBase.DATE_FIN
	WHERE PBase.ACTIVE = 1 AND P.ACTIVE = 1 AND PBase.YEAR != P.YEAR AND YEAR(PBase.DATE_DEBUT) != YEAR(PBase.DATE_FIN)
	GROUP BY PBase.ID_TYPE_PERIODE, PBase.ID_PERIODE, PBase.IDX, P.ID_TYPE_PERIODE, P.ID_TYPE_PERIODE)
,PeriodeOnSameYear(IdTP, IDP, IndP, IdTPppdc, MaxInd, MinInd, NbInd)
AS (
	SELECT PBase.ID_TYPE_PERIODE, PBase.ID_PERIODE IDP, PBase.IDX IndP, P.ID_TYPE_PERIODE IdTPppdc, 
			MAX(P.IDX) MaxInd,MIN(P.IDX) MinInd, MAX(P.IDX) - MIN(P.IDX) + 1 NBind
	FROM PERIODE PBase 
	INNER JOIN PERIODE p ON P.DATE_DEBUT < PBase.DATE_FIN AND P.DATE_FIN > PBase.DATE_DEBUT
	WHERE (P.ACTIVE = 1) AND (PBase.ACTIVE = 1) AND P.YEAR = PBase.YEAR
	GROUP BY PBase.ID_TYPE_PERIODE, PBase.ID_PERIODE, PBase.IDX, P.ID_TYPE_PERIODE)
INSERT INTO #CalculIndexPeriode (IdTP, IDP, IndP, IdTPppdc, IdTPppdc2, MaxInd, MinInd, NbInd)
SELECT PSY.IdTP, PSY.IDP, PSY.IndP, PSY.IdTPppdc, PSY.IdTPppdc,
CASE WHEN PMY.MaxInd > PSY.MaxInd THEN PMY.MaxInd ELSE PSY.MaxInd END MaxInd,
CASE WHEN PMY.MinInd < PSY.MinInd THEN PMY.MinInd ELSE PSY.MinInd END MinInd,
PSY.NbInd + ISNULL(PMY.NbInd, 0) NbInd
FROM PeriodeOnSameYear PSY
LEFT OUTER JOIN PeriodeOnMultipleYear PMY ON PSY.IDP = PMY.IDP AND PSY.IdTPppdc = PMY.IdTPppdc

CREATE TABLE #LienPeriode( 
IdTPSup INT NOT NULL, 
IndPSup INT NULL, 
IdTPInf INT NOT NULL, 
IndPInf INT NULL, 
IdTPppdc INT NULL, 
MaxIndInf INT NULL, 
MinIndInf INT NULL, 
NbIndInf INT NULL) 
 
CREATE CLUSTERED INDEX Idx_IdTPSup_IndPSup ON #LienPeriode  
(IdTPSup ASC, IndPSup ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

INSERT INTO #LienPeriode(IdTPSup, IndPSup, IdTPInf, IndPInf, IdTPppdc, MaxIndInf, MinIndInf, NbIndInf) 
	SELECT CIPSup.IdTP IdTPSup, 
	CIPSup.IndP IndPSup, 
	CIPInf.IdTP IdTPInf, 
	CIPInf.IndP IndPInf, 
	CIPSup.IdTPppdc2 IdTPppdc,  
	CIPInf.MaxInd MaxIndInf, 
	CIPInf.MinInd MinIndInf, 
	CIPInf.NbInd NbIndInf 
	FROM #CalculIndexPeriode CIPSup 
	INNER JOIN #CalculIndexPeriode CIPInf ON CIPSup.MaxInd >= CIPInf.IndP 
											AND CIPSup.MinInd <= CIPInf.IndP 
											AND CIPSup.IdTPppdc2 = CIPInf.IdTPppdc 
											AND CIPSup.IdTPppdc = CIPInf.IdTP
										
SELECT TP.IdTPSup, 
TP.IndPSup, 
TP.IdTPInf, 
TP.IndPInf, 
TP.IdTPppdc,  
TP.MaxIndInf, 
TP.MinIndInf, 
TP.NbIndInf, 
CIPSupNb.MaxInd MaxIndSup, 
CIPSupNb.MinInd MinIndSup,  
CIPSupNb.NbInd NbIndSup, 
CASE WHEN CIPSupNb.MaxInd < TP.MaxIndinf THEN CIPSupNb.MaxInd ELSE TP.MaxIndinf END MaxInd,
CASE WHEN CIPSupNb.MinInd > TP.MinIndinf THEN CIPSupNb.MinInd ELSE TP.MinIndinf END MinInd, 
CASE WHEN CIPSupNb.MaxInd < TP.MaxIndinf THEN CIPSupNb.MaxInd ELSE TP.MaxIndinf END - CASE WHEN CIPSupNb.MinInd > TP.MinIndinf THEN CIPSupNb.MinInd ELSE TP.MinIndinf END +1 NbCom 
INTO #LienPeriode2 
FROM #CalculIndexPeriode CIPSupNb 
INNER JOIN #LienPeriode TP ON CIPSupNb.IdTP = TP.IdTPSup AND CIPSupNb.IndP = TP.IndPSup AND CIPSupNb.IdTPppdc = TP.IdTPppdc  
   
SELECT *, CONVERT(DECIMAL(19,0),NbCom)/CONVERT(DECIMAL(19,0),NbIndInf) CoefInf, CONVERT(DECIMAL(19,0),NbCom)/CONVERT(DECIMAL(19,0),NbIndSup) CoefSup 
INTO #LienPeriode3 
FROM #LienPeriode2  
ORDER BY IdTPSup, IndPSup, IdTPInf, IndPInf

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CO_PERIODE]') AND type in (N'U')) DROP TABLE CO_PERIODE 
 
CREATE TABLE CO_PERIODE ( 
IdEmetteur INT NOT NULL, 
IdTPEmetteur INT NOT NULL, 
AliasEmetteur NVARCHAR(MAX) NULL, 
IndEmetteur INT NULL, 
IdRecepteur INT NOT NULL, 
IdTPRecepteur INT NOT NULL, 
AliasRecepteur NVARCHAR(MAX) NULL, 
IndRecepteur INT NULL, 
IdTPppdc INT NULL, 
MinIndRecepteur INT NULL, 
MaxIndRecepteur INT NULL, 
NbIndRecepteur INT NULL, 
MinIndEmetteur INT NULL, 
MaxIndEmetteur INT NULL, 
NbIndEmetteur INT NULL, 
MaxInd INT NULL, 
MinInd INT NULL, 
NbCom INT NULL, 
Coef DECIMAL(38,19) NULL, 
IndRelatif INT NULL, 
IDP AS (IdEmetteur), 
IDF AS (IdRecepteur), 
QuantitePPDC AS (NbCom), 
QuantiteP  AS (Coef), 
QuantiteF  AS ((1)), 
NbIndex as (IndRelatif))
  
CREATE CLUSTERED INDEX Pk_Lien ON CO_PERIODE 
(IdTPEmetteur ASC, IndEmetteur ASC, IdTPRecepteur ASC, IndRecepteur ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

 INSERT INTO CO_PERIODE (IdEmetteur, IdTPEmetteur, AliasEmetteur, IndEmetteur, IdRecepteur, IdTPRecepteur, AliasRecepteur , IndRecepteur, IdTPppdc, MinIndRecepteur, MaxIndRecepteur, 
NbIndRecepteur, MinIndEmetteur, MaxIndEmetteur, NbIndEmetteur, MaxInd, MinInd, NbCom, Coef) 
SELECT * FROM   
	(SELECT PRecepteur.ID_PERIODE IdEmetteur, 
	LP3.IdTPSup IdTPEmetteur, 
	PRecepteur.Alias AliasEmetteur, 
	PRecepteur.IDX IndEmetteur,  
	PEmetteur.ID_PERIODE IdRecepteur, 
	LP3.IdTPInf IdTPRecepteur, 
	PEmetteur.Alias AliasRecepteur, 
	PEmetteur.IDX IndRecepteur, 
	LP3.IdTPppdc,  
	LP3.MinIndInf MinIndRecepteur, 
	LP3.MaxIndInf MaxIndRecepteur, 
	LP3.NbIndInf NbIndRecepteur, 
	LP3.MinIndSup MinIndEmetteur,  
	LP3.MaxIndSup MaxIndEmetteur, 
	LP3.NbIndSup NbIndEmetteur, 
	LP3.MaxInd, 
	LP3.MinInd, 
	LP3.NbCom, 
	LP3.CoefSup Coef 
	FROM #LienPeriode3 LP3 
	INNER JOIN PERIODE PRecepteur ON LP3.IdTPSup = PRecepteur.ID_TYPE_PERIODE AND LP3.IndPSup = PRecepteur.IDX 
	INNER JOIN PERIODE PEmetteur ON LP3.IdTPInf = PEmetteur.ID_TYPE_PERIODE AND LP3.IndPInf = PEmetteur.IDX 
	WHERE (LP3.IdTPInf >= LP3.IdTPSup AND PEmetteur.ACTIVE = 1 AND PRecepteur.ACTIVE = 1)
	
	UNION                     
	
	SELECT PEmetteur.ID_PERIODE IdEmetteur, 
	LP3.IdTPInf IdTPEmetteur, 
	PEmetteur.Alias AliasEmetteur, 
	PEmetteur.IDX IndEmetteur,  
	PRecepteur.ID_PERIODE IdRecepteur, 
	LP3.IdTPSup IdTPRecepteur, 
	PRecepteur.Alias AliasRecepteur, 
	PRecepteur.IDX IndRecepteur,  
	LP3.IdTPppdc, 
	LP3.MinIndSup MinIndRecepteur,
	LP3.MaxIndSup MaxIndRecepteur, 
	LP3.NbIndSup NbIndRecepteur,  
	LP3.MinIndInf MinIndEmetteur, 
	LP3.MaxIndInf MaxIndEmetteur,
	LP3.NbIndInf NbIndEmetteur, LP3.MaxInd,
	LP3.MinInd,  
	LP3.NbCom, 
	LP3.CoefInf Coef 
	FROM #LienPeriode3 LP3 
	INNER JOIN PERIODE PRecepteur ON LP3.IdTPSup = PRecepteur.ID_TYPE_PERIODE AND LP3.IndPSup = PRecepteur.IDX 
	INNER JOIN PERIODE PEmetteur ON LP3.IdTPInf = PEmetteur.ID_TYPE_PERIODE AND LP3.IndPInf = PEmetteur.IDX 
	WHERE (LP3.IdTPInf > LP3.IdTPSup) AND PRecepteur.ACTIVE = 1 AND PEmetteur.ACTIVE = 1) TableLien 
  
 
UPDATE L SET L.IndRelatif = Calcul.IndRelatif  
FROM CO_PERIODE L 
INNER JOIN (
		SELECT IndEmetteur, IdTPEmetteur, IdTPRecepteur, IndRecepteur, ROW_NUMBER() OVER (PARTITION BY IndEmetteur, IdTPEmetteur, IdTPRecepteur ORDER BY IndRecepteur) IndRelatif 
		FROM CO_PERIODE) Calcul 
ON L.IndEmetteur = Calcul.IndEmetteur AND L.IdTPEmetteur = Calcul.IdTPEmetteur AND L.IdTPRecepteur = Calcul.IdTPRecepteur AND L.IndRecepteur = Calcul.IndRecepteur
END