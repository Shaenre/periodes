﻿using ID3iShared;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Periodes.Shell
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : IShell
    {
        [Dependency]
        public IUnityContainer Container { get; set; }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RadRadioButton_Click(object sender, RoutedEventArgs e)
        {
            Container.Resolve<Global.IApplicationRequests>().NotificationPopupRequest.Raise(new Global.ViewModel.NotificationPopupViewModel()
            {
                Title = "MessageBox exemple",
                Content = "Ceci est une message box",
                MessageBoxButtons = System.Windows.Forms.MessageBoxButtons.OK
            });
        }
    }
}
