﻿using Mindscape.Raygun4Net;
using Mindscape.Raygun4Net.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3iShared.Logger;

namespace Periodes.Shell.Logger
{
    public class RaygunSender : IExceptionSender
    {
        public static RaygunSender Instance { get; } = new RaygunSender();
        private static RaygunClient RaygunClient { get; } = new RaygunClient("NO_API_KEY");
        public void Send(Exception ex)
        {
            RaygunClient.Send(ex, null,
                new Dictionary<string, string>()
                {
                    { "UserName", Environment.UserName },
                    { "MachineName", Environment.MachineName }
                },
                new RaygunIdentifierMessage(Environment.UserName));
        }
    }
}
