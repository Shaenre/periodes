﻿using Prism.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3iCore;
using ID3iShared.Logger;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Periodes.Shell.Logger
{
    public class Log4NetLoggerFacade : ILoggerFacade, IExceptionLoggerFacade
    {
        public log4net.ILog Logger { get; } = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Dependency]
        public IExceptionSender ExceptionSender { get; set; }

        public void Log(string message, Category category, Priority priority)
        {
            switch (category)
            {
                case Category.Debug:
                    Logger.Debug(message);
                    break;
                case Category.Exception:
                    Logger.Error(message);
                    break;
                case Category.Info:
                    Logger.Info(message);
                    break;
                case Category.Warn:
                    Logger.Warn(message);
                    break;
                default:
                    break;
            }
        }

        public void Log(Exception ex)
        {
            Log(ex.FullStackTrace(), Category.Exception, Priority.High);
            ExceptionSender?.Send(ex);
        }
    }
}
