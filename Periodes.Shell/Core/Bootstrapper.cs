﻿using Prism.Unity;
using Prism.Regions.Behaviors;
using Prism.Regions;
using Prism.Logging;
using Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ID3iShared;
using ID3iShared.Behavior;
using ID3iShared.Logger;
using ID3iShared.RegionAdapter;
using Periodes.Splash;
using Periodes.Global;
using Periodes.Global.View;
using Periodes.Shell.Logger;
using Periodes.Theming;
using Periodes.Configuration;
using Periodes.Auth.Sans;
using Periodes.Authorisation.Sans;
using Periodes.Gestion.Standard;
using Periodes.Gestion.WorkingDays;
using ID3iShared.Command;
using Periodes.Gestion;
using Periodes.Builder.Specifiques;
using Periodes.BuilderConfig.Standard;
using Periodes.Data.WebService;
using ID3iHoliday.Engine.Modularity;
using ID3iHoliday.Countries.Modularity;
using Periodes.Holiday;
using Periodes.WebService.Visualizer;

namespace Periodes.Shell.Core
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell() => Container.Resolve<IShell>() as DependencyObject;
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<IApplicationCommands, ApplicationCommandsProxy>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IApplicationRequests, ApplicationRequestsProxy>(new ContainerControlledLifetimeManager());

            Container.RegisterInstance<IExceptionSender>(Container.Resolve<RaygunSender>(), new ContainerControlledLifetimeManager());
            var log4NetLoggerFacade = Container.Resolve<Log4NetLoggerFacade>();
            Container.RegisterInstance<ILoggerFacade>(log4NetLoggerFacade, new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IExceptionLoggerFacade>(log4NetLoggerFacade, new ContainerControlledLifetimeManager());

            Container.RegisterType<IShell, MainWindow>(new ContainerControlledLifetimeManager());
            Container.RegisterTypeForNavigation<NotificationPopupView>();
        }
        protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
        {
            ServiceLocator.Current.GetInstance<IRegionBehaviorFactory>().AddIfMissing(AutoPopulateRegionBehavior.BehaviorKey, typeof(AutoPopulateRegionBehaviorWithName));
            return base.ConfigureDefaultRegionBehaviors();
        }
        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
            mappings.RegisterMapping(typeof(System.Windows.Controls.StackPanel), Container.TryResolve<StackPanelRegionAdapter>());
            return mappings;
        }
        protected override ILoggerFacade CreateLogger()
        {
            return new Log4NetLoggerFacade();
        }
        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();
            ModuleCatalog.AddModule(new ModuleInfo(nameof(ThemeModule), typeof(ThemeModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(SplashModule), typeof(SplashModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(AuthSansModule), typeof(AuthSansModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(AuthorisationSansModule), typeof(AuthorisationSansModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });

            ModuleCatalog.AddModule(new ModuleInfo(nameof(HolidayEngineModule), typeof(HolidayEngineModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(ADModule), typeof(ADModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(AGModule), typeof(AGModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(FRModule), typeof(FRModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });

            ModuleCatalog.AddModule(new ModuleInfo(nameof(WebServiceFactoryModule), typeof(WebServiceFactoryModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });

            ModuleCatalog.AddModule(new ModuleInfo(nameof(BuildersModule), typeof(BuildersModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(BuilderConfigStandardModule), typeof(BuilderConfigStandardModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(GestionModule), typeof(GestionModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(StandardModule), typeof(StandardModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(WorkingDaysModule), typeof(WorkingDaysModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });

            ModuleCatalog.AddModule(new ModuleInfo(nameof(HolidayModule), typeof(HolidayModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
            ModuleCatalog.AddModule(new ModuleInfo(nameof(ApiVisualizerModule), typeof(ApiVisualizerModule).AssemblyQualifiedName) { InitializationMode = InitializationMode.WhenAvailable });
        }
        protected override void InitializeModules()
        {
            Container.Resolve<IApplicationCommands>().GoBackCommand.RegisterCommand(
                    new AutoDelegateCommand(Container.Resolve<IRegionManager>().Regions[RegionNames.MainRegion].NavigationService.Journal.GoBack,
                                            () => Container.Resolve<IRegionManager>().Regions[RegionNames.MainRegion].NavigationService.Journal.CanGoBack));

            Container.Resolve<IApplicationCommands>().NavigateCommand.RegisterCommand(
                new AutoDelegateCommand<string>(x => Container.Resolve<IRegionManager>().RequestNavigate(RegionNames.MainRegion, x)));

            ConfigFile.CheckConsistency();
            Container.Resolve<IModuleManager>().LoadModule(nameof(ThemeModule));
            Container.Resolve<IModuleManager>().LoadModule(nameof(AuthSansModule));
            Container.Resolve<IModuleManager>().LoadModule(nameof(AuthorisationSansModule));
            Container.Resolve<IModuleManager>().LoadModule(nameof(SplashModule));
        }
    }
}
