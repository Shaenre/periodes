﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Periodes.Shell.Core;
using Periodes.Data.Factory;
using Periodes.Global;
using Periodes.Global.ViewModel;
using Prism.Unity;
using ID3iShared.Logger;


namespace Periodes.Shell
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Bootstrapper BootStrapper { get; set; }
        protected override void OnStartup(StartupEventArgs e)
        {
            BootStrapper = new Bootstrapper();
            BootStrapper.Run();
        }

        public App()
        {
            DispatcherUnhandledException += App_DispatcherUnhandledException;
        }
        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            if (BootStrapper != null)
            {
                BootStrapper.Container.TryResolve<IExceptionLoggerFacade>()?.Log(e.Exception);
                BootStrapper.Container.TryResolve<IApplicationRequests>().NotificationPopupRequest?.Raise(new NotificationPopupViewModel()
                {
                    Title = "Une erreur est survenue",
                    Content = $"L'application a rencontré un problème.{Environment.NewLine}Tentative de restauration en cours.{Environment.NewLine}En cas d'échec de la restauration l'application se fermera.{Environment.NewLine}Exception : {e.Exception.Message}",
                    MessageBoxButtons = System.Windows.Forms.MessageBoxButtons.OK
                });
                BootStrapper.Container.TryResolve<IFactoryVM>()?.ResetAll();
            }
            e.Handled = true;

            return;
        }
    }
}
