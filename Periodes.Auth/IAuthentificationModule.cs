﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Modularity;

namespace Periodes.Auth
{
    public interface IAuthentificationModule : IModule { }
}
