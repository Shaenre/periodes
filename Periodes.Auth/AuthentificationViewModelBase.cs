﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Mvvm;

namespace Periodes.Auth
{
    public abstract class AuthentificationViewModelBase : BindableBase, IAuthentificationViewModel
    {
        public ICommand AuthentificationCommand { get; set; }
        public ICommand AuthentificationInitializeCommand { get; set; }
        public abstract bool IsAutomatic { get; set; }

        public event EventHandler<string> AuthentificationEnded;
        protected void OnAuthentificationEnded(string str) => AuthentificationEnded?.Invoke(this, str);

        public event EventHandler<string> AuthentificationFailed;
        protected void OnAuthentificationFailed(string message) => AuthentificationFailed?.Invoke(this, message);
    }
}
