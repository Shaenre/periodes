﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Periodes.Auth
{
    public interface IAuthentificationViewModel
    {
        ICommand AuthentificationCommand { get; set; }
        ICommand AuthentificationInitializeCommand { get; set; }
        bool IsAutomatic { get; set; }
        event EventHandler<string> AuthentificationEnded;
        event EventHandler<string> AuthentificationFailed;
    }
}
