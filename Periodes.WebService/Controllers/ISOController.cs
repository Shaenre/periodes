﻿using ID3iHoliday.Engine.Standard;
using ID3iHoliday.Models;
using Periodes.WebService.Core.Mvc;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using static Periodes.WebService.Models.CodeApi;

namespace Periodes.WebService.Controllers
{
    [ApiKey]
    [SecureApi]
    public class ISOController : BaseController
    {
        [HttpGet]
        public JsonCustomResult All()
        {
            List<CountryModel> lst = new List<CountryModel>();
            HolidaySystem.Instance.CountriesAvailable.ForEach(x => lst.Add(Transform(x)));
            return Json200(new RetourApi<CodeApi, List<CountryModel>>() { Statut = Ok, Object = lst });
        }

        private CountryModel Transform(Country country)
        {
            if (country == null)
                return null;
            return new CountryModel()
            {
                Code = country.Code,
                Alpha3Code = country.Alpha3Code,
                Names = country.Names.ToDictionary(x => x.Key.ToString(), x => x.Value),
                Langues = country.Langues.Select(x => x.ToString()).ToList(),
                States = country.States.Select(x => Transform(x)).ToList()
            };
        }
        private StateModel Transform(State state)
        {
            if (state == null)
                return null;
            return new StateModel()
            {
                Code = state.Code,
                Names = state.Names.ToDictionary(x => x.Key.ToString(), x => x.Value),
                Langues = state.Langues.Select(x => x.ToString()).ToList(),
                Regions = state.Regions.Select(x => Transform(x)).ToList()
            };
        }
        private RegionModel Transform(Region region)
        {
            if (region == null)
                return null;
            return new RegionModel()
            {
                Code = region.Code,
                Names = region.Names.ToDictionary(x => x.Key.ToString(), x => x.Value),
                Langues = region.Langues.Select(x => x.ToString()).ToList()
            };
        }

        [HttpPost]
        public JsonCustomResult Get(SettingsISO settings)
        {
            Country country = null;
            State state = null;
            Region region = null;

            if (settings.CountryCode.IsNullOrEmpty())
                return Json200(new RetourApi<CodeApi, GeographicElementModel>() { Statut = Erreur, Message = "CountryCode null value." });

            country = HolidaySystem.Instance.CountriesAvailable.FirstOrDefault(x => x.Code == settings.CountryCode);

            if (country == null)
                return Json200(new RetourApi<CodeApi, GeographicElementModel>() { Statut = Erreur, Message = "Coutry not found." });

            if (settings.StateCode.IsNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
                return Json200(new RetourApi<CodeApi, CountryModel>() { Statut = Ok, Object = Transform(country) });
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
            {
                state = country.States.FirstOrDefault(x => x.Code == settings.StateCode);

                if (state == null)
                    return Json200(new RetourApi<CodeApi, GeographicElementModel>() { Statut = Erreur, Message = "State not found." });

                return Json200(new RetourApi<CodeApi, StateModel>() { Statut = Ok, Object = Transform(state) });
            }
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.RegionCode.IsNotNullOrEmpty())
            {
                region = state.Regions?.FirstOrDefault(x => x.Code == settings.RegionCode);

                if (region == null)
                    return Json200(new RetourApi<CodeApi, GeographicElementModel>() { Statut = Erreur, Message = "Region not found." });

                return Json200(new RetourApi<CodeApi, RegionModel>() { Statut = Ok, Object = Transform(region) });
            }
            else
                return Json200(new RetourApi<CodeApi, GeographicElementModel>() { Statut = Erreur, Message = "null value." });
        }
    }
}