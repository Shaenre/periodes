﻿using Periodes.WebService.Core.Mvc;
using Periodes.WebService.Data;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Periodes.WebService.Controllers
{
    [ApiKey]
    [SecureApi]
    public class TypePeriodeController : BaseController
    {
        [HttpGet]
        public JsonCustomResult All()
        {
            List<TypePeriodeModel> lst = new List<TypePeriodeModel>();
            using (SQLModelEntities db = new SQLModelEntities())
                db.TYPE_PERIODE.ForEach(x => lst.Add(x));
            return Json200(lst);
        }

        [HttpGet]
        public JsonCustomResult Type(string param)
        {
            TypePeriodeModel type = null;
            using (SQLModelEntities db = new SQLModelEntities())
                type = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE.ToUpper() == param.ToUpper());
            return Json200(type);
        }

        [HttpPost]
        public JsonCustomResult Register(TypePeriodeModel model)
        {
            TYPE_PERIODE typePeriode = null;
            using (SQLModelEntities db = new SQLModelEntities())
            {
                typePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.CODE == model.Code);
                TYPE_PERIODE typePeriodeReference = null;
                if (model.TypeReference != null)
                    typePeriodeReference = db.TYPE_PERIODE.FirstOrDefault(x => x.ID_TYPE_PERIODE == model.TypeReference.Id);
                if (typePeriode == null)
                {
                    typePeriode = db.TYPE_PERIODE.Create();
                    if (typePeriodeReference != null)
                        typePeriode.ID_TYPE_PERIODE_REFERENCE = typePeriodeReference.ID_TYPE_PERIODE;
                    typePeriode.DESIGNATION = model.Designation;
                    typePeriode.IDX = model.Index;
                    typePeriode.CODE = model.Code;
                    if (typePeriodeReference != null)
                        typePeriodeReference.TYPE_PERIODE1.Add(typePeriode);
                    db.TYPE_PERIODE.Add(typePeriode);
                    db.SaveChanges();
                }
            }
            return Json200((TypePeriodeModel)typePeriode);
        }
    }
}