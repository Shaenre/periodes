﻿using Periodes.WebService.Core.Mvc;
using Periodes.WebService.Data;
using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using static Periodes.WebService.Models.CodeApi;

namespace Periodes.WebService.Controllers
{
    [ApiKey]
    [SecureApi]
    public class PeriodesController : BaseController
    {
        [HttpGet]
        public JsonCustomResult All()
        {
            List<PeriodeModel> lst = new List<PeriodeModel>();
            using (SQLModelEntities db = new SQLModelEntities())
                db.PERIODE.ForEach(x => lst.Add(x));
            return Json200(lst);
        }

        [HttpGet]
        public JsonCustomResult Some(string param)
        {
            int idTypePeriode = Global.DicTypePeriode.GetOrDefault(param);
            List<PeriodeModelFull> lst = new List<PeriodeModelFull>();
            using (SQLModelEntities db = new SQLModelEntities())
                db.PERIODE.Where(x => x.ID_TYPE_PERIODE == idTypePeriode).ForEach(x => lst.Add(x));
            return Json200(lst);
        }

        [HttpPost]
        public JsonCustomResult Get(Settings settings)
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeSouhaite = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeSouhaite);
                PeriodeModel periode = null;

                if (idTypePeriode > 0 && idTypePeriodeSouhaite > 0)
                    periode = db.Get(settings.Date, idTypePeriode, idTypePeriodeSouhaite).ValueOrDefault(x => x);
                else if (idTypePeriode > 0)
                    periode = db.Get(settings.Date, idTypePeriode).ValueOrDefault(x => x);
                else
                {
                    Global.Log.Error($"Get (TypePeriode : {settings.TypePeriode} - {idTypePeriode}, TypePeriodeSouhaite : {settings.TypePeriodeSouhaite} - {idTypePeriodeSouhaite})");
                    return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Erreur });
                }
                return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Ok, Object = periode, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeSouhaite) });
            }
        }

        [HttpPost]
        public JsonCustomResult Add(Settings settings)
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeSouhaite = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeSouhaite);
                int idTypePeriodeOffset = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeOffset);
                PeriodeModel periode = null;

                if (idTypePeriode > 0 && idTypePeriodeOffset > 0 && idTypePeriodeSouhaite > 0)
                    periode = db.Add(settings.Date, idTypePeriode, settings.Offset, idTypePeriodeOffset, idTypePeriodeSouhaite).ValueOrDefault(x => x);
                else if (idTypePeriode > 0 && idTypePeriodeOffset > 0)
                    periode = db.Add(settings.Date, idTypePeriode, settings.Offset, idTypePeriodeOffset).ValueOrDefault(x => x);
                else
                {
                    Global.Log.Error($"Add (TypePeriode : {settings.TypePeriode} - {idTypePeriode}, TypePeriodeOffset : {settings.TypePeriodeOffset} - {idTypePeriodeOffset}, TypePeriodeSouhaite : {settings.TypePeriodeSouhaite} - {idTypePeriodeSouhaite})");
                    return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Erreur });
                }
                return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Ok, Object = periode, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeOffset) });
            }
        }

        [HttpPost]
        public JsonCustomResult Avg(Settings settings)
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeSouhaite = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeSouhaite);
                PeriodeModel periode = null;

                if (idTypePeriode > 0 && idTypePeriodeSouhaite > 0)
                    periode = db.Avg(settings.Date, idTypePeriode, idTypePeriodeSouhaite).ValueOrDefault(x => x);
                else
                {
                    Global.Log.Error($"Avg (TypePeriode : {settings.TypePeriode} - {idTypePeriode}, TypePeriodeSouhaite : {settings.TypePeriodeSouhaite} - {idTypePeriodeSouhaite})");
                    return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Erreur });
                }
                return Json200(new RetourApi<CodeApi, PeriodeModel>() { Statut = Ok, Object = periode, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeSouhaite) });
            }
        }

        [HttpPost]
        public JsonCustomResult Count(SettingsCount settings)
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeCounted = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeCounted);

                var counted = db.Count(settings.StartDate, settings.EndDate, idTypePeriode, idTypePeriodeCounted, settings.Position, settings.Policy);
                return Json200(new RetourApi<CodeApi, int>() { Statut = Ok, Object = counted, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeCounted) });
            }
        }

        [HttpPost]
        public JsonCustomResult Enumerate(SettingsCount settings)
        {
            List<PeriodeModel> lst = new List<PeriodeModel>();
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeCounted = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeCounted);
                var result = db.Enumerate(settings.StartDate, settings.EndDate, idTypePeriode, idTypePeriodeCounted, settings.Position, settings.Policy);
                result.ForEach(x => lst.Add(x));
            }
            return Json200(new RetourApi<CodeApi, List<PeriodeModel>>() { Statut = Ok, Object = lst, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeCounted) });
        }

        [HttpPost]
        public JsonCustomResult Num(SettingsNumber settings)
        {
            using (SQLModelEntities db = new SQLModelEntities())
            {
                int idTypePeriode = Global.DicTypePeriode.GetOrDefault(settings.TypePeriode);
                int idTypePeriodeSouhaite = Global.DicTypePeriode.GetOrDefault(settings.TypePeriodeSouhaite);
                int num = -1;

                if (idTypePeriode > 0 && idTypePeriodeSouhaite > 0)
                    num = db.Num(settings.Date, idTypePeriode, idTypePeriodeSouhaite);
                else
                {
                    Global.Log.Error($"Num (TypePeriode : {settings.TypePeriode} - {idTypePeriode}, TypePeriodeSouhaite : {settings.TypePeriodeSouhaite} - {idTypePeriodeSouhaite}");
                    return Json200(new RetourApi<CodeApi, int>() { Statut = Erreur, Object = num });
                }

                return Json200(new RetourApi<CodeApi, int>() { Statut = Ok, Object = num, Message = WarningMessage(settings.TypePeriode, settings.TypePeriodeSouhaite) });
            }
        }

        [HttpPost]
        public JsonCustomResult Save(PeriodeModelFull periodeModel)
        {
            PERIODE periode = null;
            if (periodeModel != null)
            {
                if (periodeModel.Id > 0)
                {
                    using (var db = new SQLModelEntities())
                    {
                        periode = db.PERIODE.FirstOrDefault(x => x.ID_PERIODE == periodeModel.Id);
                        if (periode != null)
                        {
                            periode.ACTIVE = periodeModel.Active;
                            periode.YEAR = periodeModel.Year;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    using (var db = new SQLModelEntities())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;
                        var typePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.ID_TYPE_PERIODE == periodeModel.IdTypePeriode);
                        periode = new PERIODE()
                        {
                            DESIGNATION = periodeModel.Designation,
                            REFERENCE = periodeModel.Reference,
                            DATE_DEBUT = periodeModel.DateDebut,
                            DATE_FIN = periodeModel.DateFin,
                            QUANTITE = periodeModel.Quantite,
                            TYPE_PERIODE = typePeriode,
                            IDX = periodeModel.Index,
                            ALIAS = periodeModel.Alias,
                            ACTIVE = periodeModel.Active,
                            YEAR = periodeModel.Year
                        };
                        typePeriode.PERIODE.Add(periode);
                        db.PERIODE.Add(periode);
                        db.ChangeTracker.DetectChanges();
                        db.SaveChanges();
                    }
                }
            }
            return Json200(new RetourApi<CodeApi, PeriodeModelFull>() { Statut = Ok, Object = periode });
        }

        private void InsertOrUpdatePeriode(SQLModelEntities db, PeriodeModelFull periodeModel)
        {
            PERIODE periode = null;
            if (periodeModel != null)
            {
                if (periodeModel.Id > 0)
                {
                    periode = db.PERIODE.FirstOrDefault(x => x.ID_PERIODE == periodeModel.Id);
                    if (periode != null)
                    {
                        periode.ACTIVE = periodeModel.Active;
                        periode.YEAR = periodeModel.Year;
                        db.SaveChanges();
                    }
                }
                else
                {
                    var typePeriode = db.TYPE_PERIODE.FirstOrDefault(x => x.ID_TYPE_PERIODE == periodeModel.IdTypePeriode);
                    periode = new PERIODE()
                    {
                        DESIGNATION = periodeModel.Designation,
                        REFERENCE = periodeModel.Reference,
                        DATE_DEBUT = periodeModel.DateDebut,
                        DATE_FIN = periodeModel.DateFin,
                        QUANTITE = periodeModel.Quantite,
                        TYPE_PERIODE = typePeriode,
                        IDX = periodeModel.Index,
                        ALIAS = periodeModel.Alias,
                        ACTIVE = periodeModel.Active,
                        YEAR = periodeModel.Year
                    };
                    typePeriode.PERIODE.Add(periode);
                    db.PERIODE.Add(periode);
                }
            }
        }

        [HttpPost]
        public JsonCustomResult MassSave(List<PeriodeModelFull> lst)
        {
            using (var db = new SQLModelEntities())
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                db.Configuration.ValidateOnSaveEnabled = false;

                foreach (var item in lst)
                    InsertOrUpdatePeriode(db, item);

                db.ChangeTracker.DetectChanges();
                db.SaveChanges();
            }
            return Json200(new RetourApiBase<CodeApi>() { Statut = Ok });
        }

        private string WarningMessage(string typePeriode1, string typePeriode2)
        {
            if (typePeriode1.IsNotNullOrEmpty()
                && typePeriode2.IsNotNullOrEmpty()
                && Global.TypesPeriodesNotNormalised.GetOrDefault(typePeriode1) == typePeriode2)
                return "The result might be not accurate because there is not normalized relation between the two types passed.";
            return null;
        }
    }
}