﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Periodes.WebService.Core.Mvc;
using ID3iHoliday.Models;
using Periodes.WebService.Models;

using static Periodes.WebService.Models.CodeApi;
using static ID3iHoliday.Models.RuleType;
using ID3iHoliday.Engine.Standard;

namespace Periodes.WebService.Controllers
{
    [ApiKey]
    [SecureApi]
    public class SpecificDayController : BaseController
    {
        [HttpPost]
        public JsonCustomResult Get(SettingsHoliday settings)
        {
            Country country = null;
            State state = null;
            Region region = null;

            RuleType type = All;
            if (Enum.TryParse(settings.Type, true, out RuleType t))
                type = t;
            else
                return Json200(new RetourApi<CodeApi, List<SpecificDay>>() { Statut = Erreur, Message = "Rule type not found." });

            if (settings.CountryCode.IsNullOrEmpty())
                return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "CountryCode null value." });

            country = HolidaySystem.Instance.CountriesAvailable.FirstOrDefault(x => x.Code == settings.CountryCode);

            if (country == null)
                return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "Country not found." });

            IEnumerable<SpecificDayModel> days = null;
            if (settings.StateCode.IsNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
                days = Transform(HolidaySystem.Instance.All(settings.Year, settings.CountryCode, type));
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
            {
                state = country.States.FirstOrDefault(x => x.Code == settings.StateCode);

                if (state == null)
                    return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "State not found." });

                days = Transform(HolidaySystem.Instance.All(settings.Year, settings.CountryCode, settings.StateCode, type));
            }
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.StateCode.IsNotNullOrEmpty())
            {
                region = state.Regions?.FirstOrDefault(x => x.Code == settings.RegionCode);

                if (region == null)
                    return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "Region not found." });

                days = Transform(HolidaySystem.Instance.All(settings.Year, settings.CountryCode, settings.StateCode, settings.RegionCode, type));
            }
            return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>()
            {
                Object = days?.ToList(),
                Statut = Ok
            });
        }

        [HttpPost]
        public JsonCustomResult LongWeekEnds(SettingsHoliday settings)
        {
            Country country = null;
            State state = null;
            Region region = null;

            if (settings.CountryCode.IsNullOrEmpty())
                return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "CountryCode null value." });

            country = HolidaySystem.Instance.CountriesAvailable.FirstOrDefault(x => x.Code == settings.CountryCode);

            if (country == null)
                return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "Country not found." });

            IEnumerable<LongWeekEndModel> weekEnds = null;
            if (settings.StateCode.IsNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
                weekEnds = Transform(HolidaySystem.Instance.LongWeekEnds(settings.Year, settings.CountryCode));
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.RegionCode.IsNullOrEmpty())
            {
                state = country.States.FirstOrDefault(x => x.Code == settings.StateCode);

                if (state == null)
                    return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "State not found." });

                weekEnds = Transform(HolidaySystem.Instance.LongWeekEnds(settings.Year, settings.CountryCode, settings.StateCode));
            }
            else if (settings.StateCode.IsNotNullOrEmpty() && settings.StateCode.IsNotNullOrEmpty())
            {
                region = state.Regions?.FirstOrDefault(x => x.Code == settings.RegionCode);

                if (region == null)
                    return Json200(new RetourApi<CodeApi, List<SpecificDayModel>>() { Statut = Erreur, Message = "Region not found." });

                weekEnds = Transform(HolidaySystem.Instance.LongWeekEnds(settings.Year, settings.CountryCode, settings.StateCode, settings.RegionCode));
            }
            return Json200(new RetourApi<CodeApi, List<LongWeekEndModel>>()
            {
                Object = weekEnds?.ToList(),
                Statut = Ok
            });
        }

        private IEnumerable<SpecificDayModel> Transform(IEnumerable<SpecificDay> days)
        {
            return days?.Select(x => new SpecificDayModel(x.Date, x.Names.ToDictionary(y => y.Key.ToString(), y => y.Value)));
        }
        private IEnumerable<LongWeekEndModel> Transform(IEnumerable<LongWeekEnd> longWeekends)
        {
            return longWeekends?.Select(x => new LongWeekEndModel(x.StartDate, x.EndDate, x.HasBridge, x.ToString()));
        }
    }
}