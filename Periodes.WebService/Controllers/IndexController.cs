﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Periodes.WebService.Core.Mvc;
using Periodes.WebService.Models;

namespace Periodes.WebService.Controller
{
    public class IndexController : BaseController
    {
        [HttpGet]
        public JsonCustomResult Hello()
        {
            return Json200(new RetourApi<CodeApi, string>() { Object = "Hello", Statut = CodeApi.Ok });
        }
    }
}
