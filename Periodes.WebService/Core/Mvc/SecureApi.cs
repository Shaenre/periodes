﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Periodes.WebService.Core.Mvc
{
    public class SecureApi : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userAgent = HttpContext.Current.Request.UserAgent;
            if (!userAgent.ToUpper().Contains("Client-ID3i".ToUpper()))
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            base.OnActionExecuting(filterContext);
        }
    }
}
