﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;

namespace Periodes.WebService.Core.Mvc
{
    public class ApiKey : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var isLogged = true;
            string auth = filterContext.HttpContext.Request.Headers["Authorization"];
            if (string.IsNullOrEmpty(auth))
                isLogged = false;
            else
            {
                var tab = auth.Split(' ');
                var parameters = new { Scheme = tab[0], ApiKey = tab[1] };
                if (parameters.Scheme.ToUpper() != "ID3i-Auth".ToUpper())
                    isLogged = false;
            }
            if (!isLogged)
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            base.OnActionExecuting(filterContext);
        }
    }
}
