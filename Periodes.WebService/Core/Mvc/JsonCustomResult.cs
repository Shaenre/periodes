﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Periodes.WebService.Core.Mvc
{
    public class JsonCustomResult : ActionResult
    {
        public JsonCustomResult(object data)
        {
            Data = data;
        }

        public JsonCustomResult(object data, JsonSerializerSettings settings) : this(data)
        {
            Data = data;
            Settings = settings;
        }

        public JsonCustomResult(object data, IContractResolver contract) : this(data, new JsonSerializerSettings { ContractResolver = contract }) { }

        public JsonCustomResult(object data, NullValueHandling nullValueHandling) : this(data, new JsonSerializerSettings { NullValueHandling = nullValueHandling }) { }

        public object Data { get; private set; }
        private JsonSerializerSettings Settings;

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "application/json";
            response.ContentEncoding = Encoding.UTF8;

            if (Data != null)
            {
                string srlzd;
                if (Settings == null)
                    srlzd = JsonConvert.SerializeObject(Data, Formatting.None);
                else
                    srlzd = JsonConvert.SerializeObject(Data, Formatting.None, Settings);
                response.Write(srlzd);
            }
        }
    }
}
