﻿using ID3iCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Mvc = System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace Periodes.WebService.Core.Mvc
{
    public class BaseController : Mvc::Controller
    {
        public string RequestAppCode = "";
        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext.HttpContext.Request.Headers["ID3iPeriodesApp"] != null)
                RequestAppCode = requestContext.HttpContext.Request.Headers["ID3iPeriodesApp"].ToString();
            base.Initialize(requestContext);
        }
        protected override void OnException(Mvc::ExceptionContext filterContext)
        {
            Global.Log.Error("Erreur non catchée : " + filterContext.Exception.FullStackTrace());
            base.OnException(filterContext);
        }
        protected override void Dispose(bool disposing) { }
        protected JsonCustomResult Json200<T>(T data)
        {
            Response.StatusCode = 200;
            return new JsonCustomResult(data);
        }
        protected JsonCustomResult Json200<T>(T data, NullValueHandling nullValueHandling)
        {
            Response.StatusCode = 200;
            return new JsonCustomResult(data, nullValueHandling);
        }
        protected JsonCustomResult Json200<T>(T data, IContractResolver contract = null)
        {
            Response.StatusCode = 200;
            if (contract == null)
                return new JsonCustomResult(data);
            else
                return new JsonCustomResult(data, contract);
        }
        protected JsonCustomResult Json204()
        {
            Response.StatusCode = 204;
            return new JsonCustomResult(new object());
        }
        protected JsonCustomResult Json403<T>(T message = null) where T : class
        {
            Response.StatusCode = 403;
            return JsonStatham4xxOr5xx(message);
        }
        protected JsonCustomResult Json404<T>(T message = null) where T : class
        {
            Response.StatusCode = 404;
            return JsonStatham4xxOr5xx(message);
        }
        protected JsonCustomResult Json400<T>(T message = null) where T : class
        {
            Response.StatusCode = 400;
            return JsonStatham4xxOr5xx(message);
        }
        protected JsonCustomResult Json500<T>(T message = null) where T : class
        {
            Response.StatusCode = 500;
            return JsonStatham4xxOr5xx(message);
        }
        private JsonCustomResult JsonStatham4xxOr5xx<T>(T message = null) where T : class
        {
            var json = new JObject();
            if (message != null)
                json.Add("message", JToken.FromObject(message));
            Response.TrySkipIisCustomErrors = true;
            return new JsonCustomResult(json);
        }
    }
}
