﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Periodes.WebService.Core.Configuration
{
    public class ConfigFile : ConfigurationSection
    {
        public static ConfigFile Conf { get; private set; }

        static ConfigFile()
        {
            Conf = (ConfigFile)ConfigurationManager.GetSection(nameof(ConfigFile));
            if (Conf == null || !Conf.ElementInformation.IsPresent)
                throw new Exception($"La section de configuration {nameof(ConfigFile)} n'a pas été trouvée.");
        }
        public static void CheckConsistency() { }
    }
}