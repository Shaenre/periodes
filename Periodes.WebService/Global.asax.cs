﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Periodes.WebService.Core.Configuration;
using Periodes.WebService.Data;

using static Periodes.Gestion.Standard.Data.TypePeriodeReference;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Periodes.WebService
{
    public class Global : HttpApplication
    {
        public static Dictionary<string, int> DicTypePeriode { get; private set; } = new Dictionary<string, int>();
        public static Dictionary<string, string> TypesPeriodesNotNormalised { get; private set; } = new Dictionary<string, string>();

        public static log4net.ILog Log { get; } = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start(object sender, EventArgs e)
        {
            Log.Info("Démarrage de l'application");
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            try
            {
                ConfigFile.CheckConsistency();

                using (SQLModelEntities db = new SQLModelEntities())
                    db.TYPE_PERIODE.ForEach(x => DicTypePeriode.Add(x.CODE, x.ID_TYPE_PERIODE));

                /* 
                 * Mise en place d'un dictionnaire permettant de savoir entre quels types aucune norme n'a été édictée.
                 * Et donc pour les quels les calculs ne seront pas forcément fiable.
                 */
                TypesPeriodesNotNormalised.Add(MONTH.ToString(), WEEK.ToString());
                TypesPeriodesNotNormalised.Add(WEEK.ToString(), MONTH.ToString());
            }
            catch (Exception ex)
            {
                Log.Error("Une erreur est survenue lors du lancement de l'application" + Environment.NewLine + ex.FullStackTrace());
            }
        }

        protected void Session_Start(object sender, EventArgs e) { }
        protected void Application_BeginRequest(object sender, EventArgs e) { }
        protected void Application_AuthenticateRequest(object sender, EventArgs e) { }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception lastException = Server.GetLastError();
            Log.Error("Erreur non catchée : " + lastException.FullStackTrace());
        }
        protected void Session_End(object sender, EventArgs e) { }
        protected void Application_End(object sender, EventArgs e) { }
    }
}