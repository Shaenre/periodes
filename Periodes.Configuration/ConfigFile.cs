﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace Periodes.Configuration
{
    public class ConfigFile : ConfigurationSection
    {
        public static ConfigFile Conf { get; private set; }

        [ConfigurationProperty(nameof(ApplicationName), IsRequired = true)]
        public string ApplicationName
        {
            get => (string)this[nameof(ApplicationName)];
            set => this[nameof(ApplicationName)] = value;
        }

        public string ApplicationDirectory
        {
            get
            {
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ApplicationName);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                return path;
            }
        }

        static ConfigFile()
        {
            Conf = (ConfigFile)ConfigurationManager.GetSection(nameof(ConfigFile));
            if (!Conf.ElementInformation.IsPresent)
                throw new Exception($"La section de configuration {nameof(ConfigFile)} n'a pas été trouvée.");
        }
        public static void CheckConsistency() { }
    }
}
