﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.WebService.Models;
using System.Linq.Expressions;

using static Periodes.WebService.Models.PeriodePosition;
using static Periodes.WebService.Models.PeriodePolicy;

namespace Periodes.WebService.Data
{
    public partial class SQLModelEntities : DbContext
    {
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> exprimé dans le type de référence à partir d'une date et d'un type de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        private CO_PERIODE GetPeriode(DateTime date, int idTypePeriode)
        {
            return GetPeriode(date, idTypePeriode, idTypePeriode);
        }
        /// <summary>
        /// Obtient le dernier <see cref="CO_PERIODE"/> exprimé dans le type souhaité à partir d'une date et d'un type de référence. 
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaité.</param>
        private CO_PERIODE GetPeriode(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return Periodes(date, idTypePeriode, idTypePeriodeSouhaite).MaxBy(x => x.IndEmetteur);
        }
        /// <summary>
        /// Obtient tous les <see cref="CO_PERIODE"/> exprimé dans le type souhaité à partir d'une date et d'un type de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type des périodes souhaité.</param>
        private IQueryable<CO_PERIODE> Periodes(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return CO_PERIODE.Where(x =>
            x.IdTPRecepteur == idTypePeriode
            && x.IdTPEmetteur == idTypePeriodeSouhaite
            && x.PERIODE1.DATE_DEBUT <= date
            && x.PERIODE1.DATE_FIN >= date);
        }
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> exprimé dans le type souhaité à partir d'un type de référence.
        /// Ce <see cref="CO_PERIODE"/> est forcément le dernier avant la date souhaitée.
        /// </summary>
        /// <param name="date">Date avant laquelle on souhaite récupérer le <see cref="CO_PERIODE"/>.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaitée.</param>
        private CO_PERIODE GetLastCoPeriodeBeforeDate(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return CO_PERIODE.Where(x => x.IdTPRecepteur == idTypePeriode
                                        && x.IdTPEmetteur == idTypePeriodeSouhaite
                                        && x.PERIODE1.DATE_DEBUT <= date).OrderByDescending(x => x.IndRecepteur).FirstOrDefault();
        }
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> exprimé dans le type souhaité à partir d'un type de référence.
        /// Ce <see cref="CO_PERIODE"/> est forcément le premier après la date souhaitée.
        /// </summary>
        /// <param name="date">Date après laquelle on souhaite récupérer le <see cref="CO_PERIODE"/>.</param>
        /// <param name="idTypePeriode">Tpe de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaitée.</param>
        private CO_PERIODE GetFirstCoPeriodeAfterDate(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return CO_PERIODE.Where(x => x.IdTPRecepteur == idTypePeriode
                                        && x.IdTPEmetteur == idTypePeriodeSouhaite
                                        && x.PERIODE1.DATE_FIN >= date).FirstOrDefault();
        }
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> exprimé dans le type de référence à partir d'une date et d'un type de référence.
        /// Cette période est calculée à partir d'une période de référence à laquelle on ajoute un nombre de périodes exprimé dans le type d'offset.
        /// Dans le cas ou la période de référence n'est pas trouvée alors on vient utiliser la dernière période avant la date de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="offset">Nombre de périodes de décallage.</param>
        /// <param name="idTypePeriodeOffset">Type des périodes de décallage.</param>
        /// <returns>
        ///     La période correspondante.
        ///     Cette periode a dans le meilleur des cas le même index que la période de référence, 
        ///     sinon c'est la période moyenne du type de référence qui est renvoyée.
        /// </returns>
        private CO_PERIODE OffsetPeriode(DateTime date, int idTypePeriode, int offset, int idTypePeriodeOffset)
        {
            var coPeriode = GetPeriode(date, idTypePeriode, idTypePeriodeOffset);
            if (coPeriode == null)
                coPeriode = GetLastCoPeriodeBeforeDate(date, idTypePeriode, idTypePeriodeOffset);

            if (coPeriode != null)
                return CO_PERIODE.Where(x => x.IdTPEmetteur == idTypePeriodeOffset
                                        && x.IndEmetteur == (coPeriode.IndEmetteur + offset)
                                        && x.IdTPRecepteur == idTypePeriode
                                        && x.IndRelatif == (coPeriode.IndRelatif <= x.NbIndEmetteur ? coPeriode.IndRelatif : (x.NbIndEmetteur % 2 == 0 ? x.NbIndEmetteur / 2 : (x.NbIndEmetteur / 2) + 1)))
                                 .FirstOrDefault();
            return null;
        }
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> exprimé dans le type souhaité à partir d'une date et d'un type de référence.
        /// Cette période est calculée à partir d'une periode de référence à laquelle on ajoute un nombre de périodes exprimé dans le type d'offset.
        /// Dans le cas ou la période de référence n'est pas trouvée alors on vient utiliser la dernière période avant la date de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="offset">Nombre de périodes de décallage.</param>
        /// <param name="idTypePeriodeOffset">Type des périodes de décallage.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaité en sortie.</param>
        /// <returns>
        ///     La période correspondante.
        ///     Cette période peut être dans un autre type que la période de référence et peut ne pas être le type de période que l'on ajoute,
        ///     c'est pourquoi la période retournée est toujours la période moyenne du type souhaité.
        /// </returns>
        private CO_PERIODE OffsetPeriode(DateTime date, int idTypePeriode, int offset, int idTypePeriodeOffset, int idTypPeriodeSouhaite)
        {
            var coPeriode = GetPeriode(date, idTypePeriode, idTypePeriodeOffset);
            if (coPeriode == null)
                coPeriode = GetLastCoPeriodeBeforeDate(date, idTypePeriode, idTypePeriodeOffset);

            if (coPeriode != null)
                return CO_PERIODE.Where(x => x.IdTPEmetteur == idTypePeriodeOffset
                                            && x.IndEmetteur == (coPeriode.IndEmetteur + offset)
                                            && x.IdTPRecepteur == idTypPeriodeSouhaite
                                            && x.IndRelatif == (x.NbIndEmetteur % 2 == 0 ? x.NbIndEmetteur / 2 : (x.NbIndEmetteur / 2) + 1))
                                 .FirstOrDefault();
            return null;
        }
        /// <summary>
        /// Obtient le <see cref="CO_PERIODE"/> moyen exprimé dans le type souhaité à partir d'une période de référence.
        /// </summary>
        /// <param name="periode">Période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaité</param>
        private CO_PERIODE AvgPeriode(DateTime date, int idTypePeriode, int idTypPeriodeSouhaite)
        {
            var coPeriode = GetPeriode(date, idTypePeriode, idTypePeriode);
            if (coPeriode != null)
                return CO_PERIODE.Where(x => x.IdTPEmetteur == idTypePeriode
                                            && x.IdTPRecepteur == idTypPeriodeSouhaite
                                            && x.IdEmetteur == coPeriode.PERIODE1.ID_PERIODE
                                            && x.IndRelatif == (x.NbIndEmetteur % 2 == 0 ? x.NbIndEmetteur / 2 : (x.NbIndEmetteur / 2) + 1))
                                 .FirstOrDefault();
            return null;
        }

        /// <summary>
        /// Obtient la <see cref="PERIODE"/> exprimée dans le type de référence à partir d'une date et d'un type de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période.</param>
        /// <returns>
        ///     La <see cref="PERIODE"/> correspondante.
        /// </returns>
        public PERIODE Get(DateTime date, int idTypePeriode)
        {
            return GetPeriode(date, idTypePeriode).ValueOrDefault(x => x.PeriodeEmetteur);
        }
        /// <summary>
        /// Obtient la <see cref="PERIODE"/> exprimée dans le type souhaité à partir d'une date et d'un type de référence. 
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaitée.</param>
        /// <returns>
        ///     La période correspondante.
        /// </returns>
        public PERIODE Get(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return GetPeriode(date, idTypePeriode, idTypePeriodeSouhaite).ValueOrDefault(x => x.PeriodeEmetteur);
        }
        /// <summary>
        /// Obtient la <see cref="PERIODE"/> exprimée dans le type de référence à partir d'une date et d'un type de référence.
        /// Cette période est calculée à partir d'une période de référence à laquelle on ajoute un nombre de périodes exprimé dans le type d'offset.
        /// Dans le cas ou la période de référence n'est pas trouvée alors on vient utiliser la dernière période avant la date de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="offset">Nombre de périodes de décallage.</param>
        /// <param name="idTypePeriodeOffset">Type des périodes de décallage.</param>
        /// <returns>
        ///     La <see cref="PERIODE"/> correspondante.
        ///     Cette periode a dans le meilleur des cas le même index que la période de référence, 
        ///     sinon c'est la période moyenne du type de référence qui est renvoyée.
        /// </returns>
        public PERIODE Add(DateTime date, int idTypePeriode, int offset, int idTypePeriodeOffset)
        {
            return OffsetPeriode(date, idTypePeriode, offset, idTypePeriodeOffset).ValueOrDefault(x => x.PeriodeRecepteur);
        }
        /// <summary>
        /// Obtient la <see cref="PERIODE"/> exprimée dans le type souhaité à partir d'une date et d'un type de référence.
        /// Cette période est calculée à partir d'une periode de référence à laquelle on ajoute un nombre de périodes exprimé dans le type d'offset.
        /// Dans le cas ou la période de référence n'est pas trouvée alors on vient utiliser la dernière période avant la date de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="offset">Nombre de périodes de décallage.</param>
        /// <param name="idTypePeriodeOffset">Type des périodes de décallage.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaité en sortie.</param>
        /// <returns>
        ///     La <see cref="PERIODE"/> correspondante.
        ///     Cette période peut être dans un autre type que la période de référence et peut ne pas être le type de période que l'on ajoute,
        ///     c'est pourquoi la période retournée est toujours la période moyenne du type souhaité.
        /// </returns>
        public PERIODE Add(DateTime date, int idTypePeriode, int offset, int idTypePeriodeOffset, int idTypePeriodeSouhaite)
        {
            return OffsetPeriode(date, idTypePeriode, offset, idTypePeriodeOffset, idTypePeriodeSouhaite).ValueOrDefault(x => x.PeriodeRecepteur);
        }
        /// <summary>
        /// Obtient la <see cref="PERIODE"/> moyenne exprimé dans le type souhaité à partir d'une date et d'un type de référence.
        /// </summary>
        /// <param name="date">Date de la période de référence.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaité.</param>
        /// <returns>
        ///     La <see cref="PERIODE"/> correspondante.
        /// </returns>
        public PERIODE Avg(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return AvgPeriode(date, idTypePeriode, idTypePeriodeSouhaite).ValueOrDefault(x => x.PeriodeRecepteur);
        }

        /// <summary>
        /// Méthode qui permet d'obtenir le nombre de période entre deux périodes de référence (définies par leurs dates et le type de période).
        /// Dans le cas ou la période de début n'est pas trouvée on on utilisera la prochaine période après la date de début.
        /// Dans le cas ou la période de fin n'est pas trouvée alors on utilisera la dernière période avant la date de fin.
        /// </summary>
        /// <param name="startDate">Date de début.</param>
        /// <param name="endDate">Date de fin.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeCounted">Type des périodes à compter.</param>
        /// <param name="position">Positions pour les périodes de début et de fin de comptage.</param>
        /// <param name="policy">Politique de gestion des extrémités des périodes.</param>
        /// <returns>
        ///     Le nombre de période entre les deux dates.
        /// </returns>
        public int Count(DateTime startDate, DateTime endDate, int idTypePeriode, int idTypePeriodeCounted,
            PeriodePosition position = FTL,
            PeriodePolicy policy = IncludeEndPeriode)
        {
            var result = GetPeriodesForCounting(startDate, endDate, idTypePeriode, idTypePeriodeCounted, position);

            if (result.StartPeriode != null && result.EndPeriode != null)
                return Math.Abs(result.EndPeriode.IDX - result.StartPeriode.IDX)
                    + (policy.HasFlag(IncludeEndPeriode) ? 1 : 0)
                    - (policy.HasFlag(ExcludeStartPeriode) ? 1 : 0);
            return 0;
        }
        private (PERIODE StartPeriode, PERIODE EndPeriode)
            GetPeriodesForCounting(DateTime startDate, DateTime endDate, int idTypePeriode, int idTypePeriodeCounted, PeriodePosition position)
        {
            PERIODE startPeriode = null;
            var query = Periodes(startDate, idTypePeriode, idTypePeriodeCounted);
            if (position.HasFlag(FirstInStart))
                startPeriode = query.MinBy(x => x.IndEmetteur).ValueOrDefault(x => x.PeriodeEmetteur);
            else
                startPeriode = query.MaxBy(x => x.IndEmetteur).ValueOrDefault(x => x.PeriodeEmetteur);
            if (startPeriode == null)
                startPeriode = GetFirstCoPeriodeAfterDate(startDate, idTypePeriode, idTypePeriodeCounted).ValueOrDefault(x => x.PeriodeEmetteur);

            PERIODE endPeriode = null;
            query = Periodes(endDate, idTypePeriode, idTypePeriodeCounted);
            if (position.HasFlag(FirstInEnd))
                endPeriode = query.MinBy(x => x.IndEmetteur).ValueOrDefault(x => x.PeriodeEmetteur);
            else
                endPeriode = query.MaxBy(x => x.IndEmetteur).ValueOrDefault(x => x.PeriodeEmetteur);
            if (endPeriode == null)
                endPeriode = GetLastCoPeriodeBeforeDate(endDate, idTypePeriode, idTypePeriodeCounted).ValueOrDefault(x => x.PeriodeEmetteur);
            return (startPeriode, endPeriode);
        }
        /// <summary>
        /// Méthode qui permet d'obtenir les périodes entre deux périodes de référence (définies par leurs dates et le type de période).
        /// Dans le cas ou la période de début n'est pas trouvée alors on utilisatera la prochaine période après la date de début.
        /// Dans le cas ou la période de fin n'est pas trouvée alors on utilisera la dernière période avant la date de fin.
        /// </summary>
        /// <param name="startDate">Date de début.</param>
        /// <param name="endDate">Date de fin.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeCounted">Type des périodes à compter.</param>
        /// <param name="position">Positions pour les périodes de début et de fin de comptage.</param>
        /// <param name="policy">Politique de gestion des extrémités des périodes.</param>
        /// <returns>
        ///     Les périodes entre les deux dates.
        /// </returns>
        public IEnumerable<PERIODE> Enumerate(DateTime startDate, DateTime endDate, int idTypePeriode, int idTypePeriodeCounted,
            PeriodePosition position = FTL,
            PeriodePolicy policy = IncludeEndPeriode)
        {
            var result = GetPeriodesForCounting(startDate, endDate, idTypePeriode, idTypePeriodeCounted, position);

            if (result.StartPeriode != null && result.EndPeriode != null)
            {
                Expression<Func<PERIODE, bool>> funcStart = x => x.IDX >= result.StartPeriode.IDX;
                if (policy.HasFlag(ExcludeStartPeriode))
                    funcStart = x => x.IDX > result.StartPeriode.IDX;

                Expression<Func<PERIODE, bool>> funcEnd = x => x.IDX < result.EndPeriode.IDX;
                if (policy.HasFlag(IncludeEndPeriode))
                    funcEnd = x => x.IDX <= result.EndPeriode.IDX;

                if (result.StartPeriode != null && result.EndPeriode != null)
                    foreach (var item in PERIODE.Where(x => x.ID_TYPE_PERIODE == idTypePeriodeCounted).Where(funcStart).Where(funcEnd))
                        yield return item;
            }
            yield break;
        }

        /// <summary>
        /// Méthode qui permet d'obtenir l'index de la période définie par la date et le type de période dans le type de période souhaité.
        /// </summary>
        /// <param name="date">Date de la période.</param>
        /// <param name="idTypePeriode">Type de la période de référence.</param>
        /// <param name="idTypePeriodeSouhaite">Type de la période souhaitée.</param>
        /// <returns>
        ///     L'index de la période dans le type souhaité.
        /// </returns>
        public int Num(DateTime date, int idTypePeriode, int idTypePeriodeSouhaite)
        {
            return GetPeriode(date, idTypePeriode, idTypePeriodeSouhaite).ValueOrDefault(x => (int)x.IndRelatif, -1);
        }
    }
}
