﻿using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Data
{
    public partial class TYPE_PERIODE
    {
        public TYPE_PERIODE TypePeriodeReference => TYPE_PERIODE2;

        public bool EstReference => TypePeriodeReference == null;

        public static implicit operator TypePeriodeModel(TYPE_PERIODE typePeriode)
        {
            if (typePeriode == null)
                return null;
            return new TypePeriodeModel()
            {
                Id = typePeriode.ID_TYPE_PERIODE,
                TypeReference = typePeriode.TypePeriodeReference,
                Designation = typePeriode.DESIGNATION,
                Index = typePeriode.IDX,
                EstReference = typePeriode.EstReference,
                Code = typePeriode.CODE
            };
        }
    }
}
