﻿using Periodes.WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Data
{
    public partial class PERIODE
    {
        public static implicit operator PeriodeModel(PERIODE periode)
        {
            if (periode == null)
                return null;
            return new PeriodeModel()
            {
                Alias = periode.ALIAS,
                DateDebut = periode.DATE_DEBUT,
                DateFin = periode.DATE_FIN
            };
        }
        public static implicit operator PeriodeModelFull(PERIODE periode)
        {
            if (periode == null)
                return null;
            return new PeriodeModelFull()
            {
                Id = periode.ID_PERIODE,
                IdTypePeriode = periode.ID_TYPE_PERIODE,
                Reference = periode.REFERENCE,
                Designation = periode.DESIGNATION,
                Quantite = periode.QUANTITE,
                Alias = periode.ALIAS,
                DateDebut = periode.DATE_DEBUT,
                DateFin = periode.DATE_FIN,
                Index = periode.IDX,
                Active = periode.ACTIVE,
                Year = periode.YEAR
            };
        }
    }
}
