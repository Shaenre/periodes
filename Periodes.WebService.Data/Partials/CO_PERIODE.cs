﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.WebService.Data
{
    public partial class CO_PERIODE
    {
        public PERIODE PeriodeEmetteur => PERIODE;
        public PERIODE PeriodeRecepteur => PERIODE1;
        public TYPE_PERIODE TypePeriodeEmetteur => TYPE_PERIODE;
        public TYPE_PERIODE TypePeriodeRecepteur => TYPE_PERIODE1;
    }
}
