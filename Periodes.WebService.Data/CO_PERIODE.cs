//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Periodes.WebService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CO_PERIODE
    {
        public int IdEmetteur { get; set; }
        public int IdTPEmetteur { get; set; }
        public string AliasEmetteur { get; set; }
        public int IndEmetteur { get; set; }
        public int IdRecepteur { get; set; }
        public int IdTPRecepteur { get; set; }
        public string AliasRecepteur { get; set; }
        public int IndRecepteur { get; set; }
        public int IdTPppdc { get; set; }
        public int MinIndRecepteur { get; set; }
        public int MaxIndRecepteur { get; set; }
        public int NbIndRecepteur { get; set; }
        public int MinIndEmetteur { get; set; }
        public int MaxIndEmetteur { get; set; }
        public int NbIndEmetteur { get; set; }
        public int MaxInd { get; set; }
        public int MinInd { get; set; }
        public int NbCom { get; set; }
        public decimal Coef { get; set; }
        public int IndRelatif { get; set; }
        public int IDP { get; set; }
        public int IDF { get; set; }
        public int QuantitePPDC { get; set; }
        public decimal QuantiteP { get; set; }
        public int QuantiteF { get; set; }
        public int NbIndex { get; set; }
    
        public virtual PERIODE PERIODE { get; set; }
        public virtual PERIODE PERIODE1 { get; set; }
        public virtual TYPE_PERIODE TYPE_PERIODE { get; set; }
        public virtual TYPE_PERIODE TYPE_PERIODE1 { get; set; }
    }
}
