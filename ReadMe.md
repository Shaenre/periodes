﻿# Solution ID3iPeriodes
Cette solution à été générée automatiquement grâce à un template développé par ID3i.  
Les librairies utilisées dans cette solution sont les suivantes :
## Prism / Unity
https://github.com/PrismLibrary/Prism  
https://github.com/PrismLibrary/Prism-Samples-Wpf
## Telerik NoXaml WPF 45
Les librairies Telerik sont obligatoires et automatiquement intégrées aux projets présents à la création de la solution.  
Les thèmes utilisés et supportés par le template sont Office2016 et Material. Office2016Touch est prévu pour plus tard.
## ID3iCore
Cette librairie contient des méthodes d'extensions.
* **Général**
  * FromHierarchy
  * FromObjectHierarchy
  * FromListHierarchy
  * ForEach
  * ValueOrDefault
  * DoIfNotNull
  * IsNull
  * IsNotNull
  * DeepCopy
  * Cartesian
  * Repeat
  * MinBy
  * MaxBy
  * OrderBy
  * ThenBy
  * Initialize
* **Dictionary**
  * GetOrDefault
* **Exception**
  * FullStackStrace
* **String**
  * AsEnum
* **ICollection**
  * Remove
  * RemoveAll
## ID3iCore.WPF
Cette librairie contient des éléments à utiliser dans le XAML.
* **Behavior**
  * ColumnWidthHelper
  ```C#
  ///Propriété attachée qui permet de définir les width d'une colonne d'une grid 
  ///et de les appliquer selon un binding.
  <ColumnDefinition ColumnWidthHelper.ExpandWidth="*"
                    ColumnWidthHelper.CollapseWidth="Auto"
                    ColumnWidthHelper.Attach="True"
                    ColumnWidthHelper.IsExpand={Binding XXX}/>
  ```
  * PasswordBoxHelper
  ```C#
  ///Propriété attachée qui permet de pouvoir binder   
  ///la propriété Password du PasswordBox à une propriété d'un ViewModel.
  <PasswordBox PasswordHelper.Attach="True"
               PasswordHelper.Password="{BindingPropertyChanged FieldPasswordIntoVM}"/>
  ```
  * WindowCloseBehavior
  ```C#
  ///Propriété attachée qui permet via le ViewModel de :
  ///fermer la fenêtre si elle est placé sur une fenêtre.
  ///fermer la fenêtre dans laquelle est l'userControl si elle est placé sur un userControl
  <Window.Style>
        <Style>
            <Style.Triggers>
                <DataTrigger Binding="{Binding Close}" Value="True">
                    <Setter Property="WindowCloseBehavior.Close" Value="True"/>
                </DataTrigger>
            </Style.Triggers>
        </Style>
    </Window.Style>
  ```
  * WindowMoveBehavior
  ```C#
  ///Propriété attachée qui permet de bouger la fenêtre. 
  ///Utilisée dans des cas très particulier comme le splashScreen par exemple.
  <Setter Property="WindowMoveBehavior.Enabled" Value="True"/>
  ```
* Bind
  * BindingPropertyChanged
  ```C#
  <TextBlock Text="{BindingPropertyChanged Field}"/>
  ///Au lieu de
  <TextBlock Text="{Binding Field, UpdateSourceTrigger=PropertyChanged}"/>
  ```
  * IndexerBinding
  ```C#
  <TextBlock Text="{IndexerBinding Indexer='SonNom', Value={x:Static Classe.StaticPropertyNomValue}}"/>
  ```
  * NestedBinding
  ```C#
  <Grid.Visibility>
    <NestedBinding Converter="{MultiConverterOrBooleanToVisibility}">
        <Binding Path="boolField1"/>
        <NestedBinding>
            <NestedBinding.MultiBinding>
                <MultiBinding Converter="{MultiConverterAndBoolean}">
                    <Binding Path="boolField2"/>
                    <Binding Path="boolField3"/>
                </MultiBinding>
            </NestedBinding.MultiBinding>
        </NestedBinding>
        <NestedBinding>
            <NestedBinding.MultiBinding>
                <MultiBinding Converter="{MultiConverterAndBoolean}">
                    <Binding Path="boolField4"/>
                    <Binding Path="boolField5"/>
                </MultiBinding>
            </NestedBinding.MultiBinding>
        </NestedBinding>
    </NestedBinding>
  </Grid.Visibility>
  ```
* Converters
  * ConverterBoolean
  ```C#
  <Grid IsEnabled="{Binding boolField, Converter={ConverterBoolean IsReversed=True}}"/>
  <Grid IsEnabled="{Binding boolField, Converter={ConverterBoolean IsReversed=False}}"/>
  ```
  * ConverterBooleanToVisibility
  ```C#
  ///Pour ce converter il est possible de redéfinir soi-même les valeurs 
  ///pour FalseVisibility, TrueVisibility, NanVisibility
  <Grid Visibility="{Binding boolField, Converter={ConverterBooleanToVisibility Inverted=True}}"/>
  <Grid Visibility="{Binding boolField, Converter={ConverterBooleanToVisibility Inverted=False}}"/>
  ```
  * ConverterEnumToString
  ```C#
  <TextBlock Text="{Binding enumField, Converter={ConverterEnumToString EnumerationType={x:Type xmlnsDef:enumType}}}"/>
  ```
  * ConverterNullToBoolean
  ```C#
  <Grid IsEnabled="{Binding Field, Converter={ConverterNullToBoolean}}"/>
  ```
  * ConverterNullToVisibility
  ```C#
  ///Pour ce converter il est possible de définir si on souhaite utiliser Hidden à la place de Collapse.
  <Grid Visibility="{Binding boolField, Converter={ConverterNullToVisibility IsReversed=True}}"/>
  <Grid Visibility="{Binding boolField, Converter={ConverterNullToVisibility IsReversed=False}}"/>
  ```
  * ConverterStringMaxChar
  ```C#
  <TextBlock Text="{Binding stringField, Converter={ConverterStringMaxChar MaxChar=10}}"/>
  ```
  * MultiConverterAndBoolean  
    **Se référer à l'exemple sur le NestedBinding.**
  * MultiConverterAndBooleanToVisibility  
    **Se référer à l'exemple sur le NestedBinding.**
  * MultiConverterOrBoolean  
    **Se référer à l'exemple sur le NestedBinding.**
  * MultiConverterOrBooleanToVisibility  
    **Se référer à l'exemple sur le NestedBinding.**
* Dictionaries
  * DesignResourceDictionary
  ```C#
  <ResourceDictionary.MergedDictionaries>
        <DesignResourceDictionary>
            <DesignResourceDictionary.DesignDictionaries>
                <ResourceDictionary Source="/ProjetQuiContientLaFeuilleXaml;component/PathDeLaFeuilleXamlDansleProjet"/>
            </DesignResourceDictionary.DesignDictionaries>
        </DesignResourceDictionary>
  </ResourceDictionary.MergedDictionaries>
  ```
  * ThemeResourceDictionary  
  *Usage interne uniquement ne pas s'en servir.*
* Providers
  * ProviderEnumToItemSource
  ```C#
  <telerik:RadComboBox DisplayMemberPath="Value" SelectedValuePath="Key" SelectedValue="{Binding enumField}">
    <telerik:RadComboBox.ItemsSource>
        <ProviderEnumToItemSource EnumType="{x:Type xmlnsDef:enumType}">
            <ProviderEnumToItemSource.ExcludeValues>
                <xmlnsDef:enumType>VAL_1</xmlnsDef:enumType>
            </ProviderEnumToItemSource.ExcludeValues>
        </ProviderEnumToItemSource>
    </telerik:RadComboBox.ItemsSource>
  </telerik:RadComboBox>
  ```
## ID3iHttpClient
Cette librairie permet de réaliser des appels Web Api.  
Elle permet entre autre de définir le wrapper spécifique au besoin.  
Par exemple pour pouvoir faire un appel à une adresse telle que : http://MonSith.fr/Api/Index/Hello il va falloir créer plusieurs classes.  
**Une pour faire l'appel à l'action Hello.**
```C#
public class HelloPath : SegmentPath
{
    protected override string Segment => "Hello";
    internal HelloPath(SegmentPath parent) : base(parent) { }
    /// <summary>
    /// Méthode qui permet de tester si l'Api est accessible.
    /// </summary>
    public Either<string, TypeDeRetour> Call() => DoJob<TypeDeRetour>();
}
```
**Une pour le controller Index.**
```C#
public class IndexPath : SegmentPath
{
    protected override string Segment => "Index";
    public IndexPath(SegmentPath parent) : base(parent) { }
    public HelloPath Hello => new HelloPath(this);
}
```
**Une pour le root de l'Api.**  
Ici on peut définir si on souhaite utiliser
* des header custom
* des accepts (json, ...)
* des user agents
* l'authorization.
```C#
public class ClientApiPath : SegmentPath
{
    protected override string Segment => "Api";
    internal ClientApiPath(SegmentPath parent) : base(parent) { }
    public IndexPath Index => new IndexPath(this);

    internal NonGenericHeader<ClientApiPath> NonGenericHeader => new NonGenericHeader<ClientApiPath>(this);
    internal AcceptHeader<ClientApiPath> Accept => new AcceptHeader<ClientApiPath>(this);
    internal UserAgentHeader<ClientApiPath> UserAgent => new UserAgentHeader<ClientApiPath>(this);
    internal AuthorizationHeader<ClientApiPath> Authorize => new AuthorizationHeader<ClientApiPath>(this);
}
```
**Une pour le fichier de configuration.**
```C#
public class ConfigFile : Core::ClientConfiguration
{
    public static ConfigFile Conf { get; private set; }

    static ConfigFile()
    {
        Conf = (ConfigFile)ConfigurationManager.GetSection(nameof(ConfigFile));
        if (!Conf.ElementInformation.IsPresent)
            throw new Exception($"La section de configuration {nameof(ConfigFile)} n'a pas été trouvée.");
    }
    public static void CheckConsistency() { }
}
```
**Une pour le wrapper.**  
Si on ne souhaite pas pouvoir définir l'Authorize c'est possible, il faut aussi penser à le retirer de la classe ClientApiPath.
```C#
public class Wrapper : Core::ClientWrapper
{
    public ClientApiPath Client { get; set; }
    public Wrapper() : base() { }
    protected override void OnConfigure()
    {
        ConfigFile.CheckConsistency();
        Client = new ClientApiPath(new Core::Paths.RootPath(this, ConfigFile.Conf.BaseApiUrl));
        Client
            .NonGenericHeader
                .Add("ID3iApp", ConfigFile.Conf.AppCode)
                .Configure()
            .Accept
                .Add(DefaultAcceptHeader.Json)
                .Configure()
            .UserAgent
                .Add(new UserAgentHeader<ClientApiPath>.UserAgentParameter("Client-ID3i"))
                .Configure()
            .Authorize
                .Set(new AuthorizationHeader<ClientApiPath>.AuthorizationParameter("ID3i-App", ConfigFile.Conf.ApiKey))
                .Configure();
    }
}
```
## ID3iShared
Cette librairie contient des éléments pour Prism & Unity.
* **Behavior**
  * AutoPopulateRegionBehaviorWithName  
    Cet élément permet de re-déclarer l'ajout de view dans les régions. Il est très utile lors que l'on référence des views avec l'objet **_ViewWithName_**.  
    On l'ajoute dans le bootStrapper de la manière suivante.
    ```C#
    protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
    {
        ServiceLocator.Current.GetInstance<IRegionBehaviorFactory>().AddIfMissing(AutoPopulateRegionBehavior.BehaviorKey, typeof(AutoPopulateRegionBehaviorWithName));
        return base.ConfigureDefaultRegionBehaviors();
    }
    ```
* **Command**
  * AutoDelegateCommand  
    Cette command éxécute le canExecute au moindre changement de propriété. A ne pas utiliser si dans le canExecute il y a des appels à entity.
* **Logger**
  * IExceptionLoggerFacade  
    Cette interface permet d'étendre le ILoggerFacade de Prism et de pouvoir logger des exceptions.
    ```C#
    protected override void ConfigureContainer()
    {
        base.ConfigureContainer();
        Container.RegisterInstance<IExceptionLoggerFacade>(myLogger, new ContainerControlledLifetimeManager());
    }
    ```
  * IExceptionSender  
    Cette interface permet de déclarer un service d'envoit d'exceptions. Utile dans le cas de raygun par exemple.
    ```C#
    protected override void ConfigureContainer()
    {
        base.ConfigureContainer();
        Container.RegisterInstance<IExceptionSender>(ExceptionSenderInstance, new ContainerControlledLifetimeManager());
    }
    ```
* **Module**  
  * Module  
    Classe de base pour les modules Prims. Il est embarqué le RegionManager et le container Unity.  
    Deux méthodes sont incluses avec RegisterViewWithRegion & RegisterViewInstanceViewRegion.
* **Popup**
  * IWindowPopup  
    Cette inteface permet de définir les différents éléments pour créer un wrapper à utiliser dans le LazyWindowPopupAction
  * LazyPopupWindowAction  
    Cette classe permet de gérer une fenêtre popup.
    ```C#
    xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"

    <i:Interaction.Triggers>
        <prism:InteractionRequestTrigger SourceObject="{x:Static global:ApplicationRequests.NotificationPopupRequest}" >
            <LazyPopupWindowAction RegionName="PopupRegion" WindowStyle="{DynamicResource styleRadWindowPopup}"
                                   NavigationUri="NotificationPopupView" IsModal="True" CenterOverAssociatedObject="True">
                <LazyPopupWindowAction.WindowPopup>
                    <RadWindowWrapper />
                </LazyPopupWindowAction.WindowPopup>
            </LazyPopupWindowAction>
        </prism:InteractionRequestTrigger>
    </i:Interaction.Triggers>
    ```
* **RegionAdapter**
  * StackPanelRegionAdapter  
    Cette classe permet de définir le comportement à avoir quand une région est placée sur un stackPanel.
    ```C#
    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
        RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
        mappings.RegisterMapping(typeof(System.Windows.Controls.StackPanel), Container.TryResolve<StackPanelRegionAdapter>());
        return mappings;
    }
    ```
* **Regions**
  * ViewWithName  
    Cet élément permet d'associer à une view un nom. Très utile dans le cas d'élément déclarer dans le code directement comme des buttons par exemple.
* **Divers**
  * IShell  
    Interface à placer sur la MainWindow.
  * NotifyPropertyChanged  
    Classe de base pour les viewModels.
## Projets de la solution
* **ID3iPeriodes.Auth**  
Ce projet permet de définir la structure des différents modules d'authentification.
* **ID3iPeriodes.Auth.ActiveDirectory**  
Ce module permet de s'authentifier via l'active directory, dans ce cas de figure l'utilisateur saisit son login et mot de passe.
* **ID3iPeriodes.Auth.Sans**  
Ce module est factice et ne réalise pas d'authentification. Très utile dans les cas où l'authentification n'est pas requise.
* **ID3iPeriodes.Auth.WebService**  
Ce module permet de s'authentifier via un webservice, dans ce cas de figure l'utilisateur saisit son login et mot de passe.
* **ID3iPeriodes.Authorisation**  
Ce projet permet de définir la structure des différents modules d'authorisation.
* **ID3iPeriodes.Authorisation.Data**  
Ce module permet d'obtenir les autorisations à partir de la base de données.
* **ID3iPeriodes.Authorisation.Sans**  
Ce module est factice et ne réaliser pas d'autorisation et donne accès à tout sans restriction. Très utile dans les cas où il n'y a pas de gestion des droits.
* **ID3iPeriodes.Data**  
Ce projet permet de définir la structure pour les différents modules d'accès aux données. C'est ce projet qui doit être référencé un peu partout. Il contient notamment les objets utilisés dans l'application : les "VM" ainsi que le "Factory" qui permet de récupérer ces fameuses "VM".
* **ID3iPeriodes.Data.SQLServer**  
Ce module permet de définir la manière de récupérer, modifier et supprimer des "VM" dans une base de données SQL Server.
* **ID3iPeriodes.Configuration**  
Ce projet contient l'accès aux clefs du fichier de configuration. C'est un projet qqui ne sert qu'a ça pour pouvoir être référencé un peu partout sans restriction.
* **ID3iPeriodes.Controls**  
Ce projet permet de rassembler des "Control" en un seul point.
* **ID3iPeriodes.Global**  
Ce projet contient des éléments potentiellement utilisable par tous les modules. Dans ce projet aucun accès aux données ne doit être fait.
* **ID3iPeriodes.Shell**  
Ce projet est l'éxécutable c'est lui qui référence un peu tous les projets pour pouvoir charger les différents modules.
* **ID3iPeriodes.Splash**  
Ce module permet d'afficher un splashScreen, de lancer le processus d'authentification, le processus d'authorisation et de charger ou pas les modules adéquats.
* **ID3iPeriodes.Theming**  
Ce module permet de gérer les thèmes Telerik et d'application à utiliser.
* **ID3iPeriodes.WebService**  
Ce projet est l'Api de gestion des données, selon la configuration de la solution la gestion de l'authentification peut être incluse.
* **ID3iPeriodes.WebService.Data**  
Ce projet contient l'accès à la base de données.
* **ID3iPeriodes.WebService.Models**  
Ce projet contient les models de communication entre l'Api et les services tiers.
* **ID3iPeriodes.WebService.Auth**  
Ce projet est l'Api d'authentification.
* **ID3iPeriodes.WebService.Auth.Data**  
Ce projet contient l'accès à la base de données.
* **ID3iPeriodes.WebService.Auth.Models**  
Ce projet contient les models de communication entre l'Api et les services tiers.  

#### Reste à faire :  
* Rajouter une interface pour l'affichage des ponts, hors du visualizer d'api.