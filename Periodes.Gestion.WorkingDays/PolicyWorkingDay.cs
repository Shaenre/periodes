﻿using ID3iHoliday.Models;
using Periodes.Policy;
using Periodes.Preferences;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.DayOfWeek;

namespace Periodes.Gestion.WorkingDays
{
    public class PolicyWorkingDay : IPolicy
    {
        [Dependency]
        public IHolidaySystem HolidaySystem { get; set; }
        [Dependency]
        public IUnityContainer Container { get; set; }

        public Preferences Preferences => Container.Resolve<IPreferences>($"{WorkingDaysModule.Name}_{nameof(Preferences)}") as Preferences;

        public bool IsYearActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsMonthActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsWeekActive(DateTime dateDebut, DateTime dateFin) => true;

        public bool IsDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek)
        {
            bool isOk = true;
            isOk = HolidaySystem.Single(dateDebut, Preferences.Code, Preferences.State, Preferences.Region) == null;
            if (isOk)
            {
                switch (dayOfWeek)
                {
                    case Sunday:
                        isOk = Preferences.Dimanche;
                        break;
                    case Monday:
                        isOk = Preferences.Lundi;
                        break;
                    case Tuesday:
                        isOk = Preferences.Mardi;
                        break;
                    case Wednesday:
                        isOk = Preferences.Mercredi;
                        break;
                    case Thursday:
                        isOk = Preferences.Jeudi;
                        break;
                    case Friday:
                        isOk = Preferences.Vendredi;
                        break;
                    case Saturday:
                        isOk = Preferences.Samedi;
                        break;
                    default:
                        isOk = true;
                        break;
                }
            }
            return isOk;
        }

        public string DaySuffixeAlias(DateTime dateDebut)
        {
            var holiday = HolidaySystem.Single(dateDebut, Preferences.Code, Preferences.State, Preferences.Region);
            if (holiday != null)
            {
                var langue = Preferences.Langue.AsEnum<Langue>();
                return holiday.Names.GetOrDefault(langue, () => holiday.Names.FirstOrDefault().Value);
            }
            return "";
        }

        public bool IsHalfDayActive(DateTime dateDebut, DateTime dateFin, DayOfWeek dayOfWeek, bool isAM)
        {
            bool isOk = true;
            isOk = HolidaySystem.Single(dateDebut, Preferences.Code, Preferences.State, Preferences.Region) == null;
            if (isOk)
            {
                switch (dayOfWeek)
                {
                    case Sunday:
                        isOk = isAM ? Preferences.DimancheMatin : Preferences.DimancheApresMidi;
                        break;
                    case Monday:
                        isOk = isAM ? Preferences.LundiMatin : Preferences.LundiApresMidi;
                        break;
                    case Tuesday:
                        isOk = isAM ? Preferences.MardiMatin : Preferences.MardiApresMidi;
                        break;
                    case Wednesday:
                        isOk = isAM ? Preferences.MercrediMatin : Preferences.MercrediApresMidi;
                        break;
                    case Thursday:
                        isOk = isAM ? Preferences.JeudiMatin : Preferences.JeudiApresMidi;
                        break;
                    case Friday:
                        isOk = isAM ? Preferences.VendrediMatin : Preferences.VendrediApresMidi;
                        break;
                    case Saturday:
                        isOk = isAM ? Preferences.SamediMatin : Preferences.SamediApresMidi;
                        break;
                    default:
                        isOk = true;
                        break;
                }
            }
            return isOk;
        }

        public string HalfDaySuffixeAlias(DateTime dateDebut)
        {
            var holiday = HolidaySystem.Single(dateDebut, Preferences.Code, Preferences.State, Preferences.Region);
            if (holiday != null)
            {
                var langue = Preferences.Langue.AsEnum<Langue>();
                return holiday.Names.GetOrDefault(langue, () => holiday.Names.FirstOrDefault().Value);
            }
            return "";
        }
    }
}
