﻿using Periodes.Data.Factory;
using Periodes.Data.ViewModel;
using Periodes.Gestion.WorkingDays.Data;
using Periodes.Gestion.WorkingDays.View;
using Periodes.Global;
using Periodes.Preferences;
using ID3iShared.Module;
using ID3iShared.Regions;
using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Periodes.Gestion.WorkingDays
{
    public class WorkingDaysModule : Module
    {
        public static string Name => "WD";
        public override void Initialize()
        {
            Configure();

            Container.RegisterInstance(Container.Resolve<WorkingDaysController>(), new ContainerControlledLifetimeManager());

            Container.RegisterInstance<IPreferences>($"{Name}_{nameof(Preferences)}", Container.Resolve<Preferences>().Initialize(x => x.Charger()), new ContainerControlledLifetimeManager());

            Container.RegisterInstance(Container.Resolve<PolicyWorkingDay>(), new ContainerControlledLifetimeManager());

            Container.RegisterTypeForNavigation<LayoutTPWDView>();
            Container.RegisterTypeForNavigation<PreferencesWDView>();

            RegionManager.RegisterViewWithRegion(RegionNames.GestionRegion, () => new ViewWithName(new RadButton()
            {
                Content = "Working days",
                Command = Container.Resolve<WorkingDaysController>().ShowInterfaceCommand
            }, $"RadButton_ShowWorkingDaysInterface_{new Random().Next()}"));
        }

        private void Configure()
        {
            var factory = Container.Resolve<IFactoryVM>();
            CheckTypePeriode(factory, TypePeriode.WORKINGDAY, "Jour ouvré");
            CheckTypePeriode(factory, TypePeriode.HALFWORKINGDAY, "Semi-jour ouvré");
            factory.SaveTypesPeriodes();
        }

        private void CheckTypePeriode(IFactoryVM factory, TypePeriode type, string libelle)
        {
            if (factory.TypesPeriodes.FirstOrDefault(x => x.Code == type.ToString()) == null)
            {
                var typePeriode = new VMTypePeriode()
                {
                    Designation = libelle,
                    Index = (int)type,
                    Code = type.ToString()
                };
                factory.TypesPeriodes.Add(typePeriode);
            }
        }
    }
}
