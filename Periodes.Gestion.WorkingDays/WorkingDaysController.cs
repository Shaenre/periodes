﻿using Periodes.Gestion.WorkingDays.View;
using Periodes.Global;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.WorkingDays
{ 
    public class WorkingDaysController
    {
        [Dependency]
        public IUnityContainer Container { get; set; }
        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public DelegateCommand ShowInterfaceCommand { get; set; }
        
        public WorkingDaysController()
        {
            ShowInterfaceCommand = new DelegateCommand(ShowInterfaceExecute);
        }
        private void ShowInterfaceExecute()
        {
            RegionManager.RequestNavigate(RegionNames.TypePeriodeRegion, nameof(LayoutTPWDView));
            RegionManager.RequestNavigate(RegionNames.SettingsRegion, nameof(PreferencesWDView));
            Container.Resolve<GestionController>().Try(x =>
            {
                x.ModuleActif = WorkingDaysModule.Name;
                x.Policy = Container.Resolve<PolicyWorkingDay>();
            });
        }
    }
}
