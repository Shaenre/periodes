﻿using ID3iHoliday.Models;
using Periodes.Configuration;
using Periodes.Preferences;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.WorkingDays
{
    public class Preferences : BindableBase, IPreferences
    {        
        [JsonIgnore()]
        [Dependency]
        public IHolidaySystem HolidaySystem { get; set; }

        [JsonIgnore()]
        public List<Country> Countries => HolidaySystem.CountriesAvailable;
        private string code;
        public string Code
        {
            get => code;
            set
            {
                code = value;
                RaisePropertyChanged(nameof(States));
                RaisePropertyChanged(nameof(Langues));
            }
        }

        [JsonIgnore()]
        public List<State> States
        {
            get
            {
                if (!string.IsNullOrEmpty(Code))
                    return Countries.FirstOrDefault(x => x.Code == Code).States;
                return null;
            }
        }
        private string state;
        public string State
        {
            get => state;
            set
            {
                state = value;
                RaisePropertyChanged(nameof(Region));
            }
        }

        [JsonIgnore()]
        public List<Region> Regions
        {
            get
            {
                if (!string.IsNullOrEmpty(Code) && !string.IsNullOrEmpty(State))
                    return Countries.FirstOrDefault(x => x.Code == Code).States.FirstOrDefault(x => x.Code == State).Regions;
                return null;
            }
        }
        private string region;
        public string Region
        {
            get => region;
            set => region = value;
        }

        [JsonIgnore()]
        public List<string> Langues
        {
            get
            {
                if (!string.IsNullOrEmpty(Code))
                    return Countries.FirstOrDefault(x => x.Code == Code).Langues.Select(x => x.ToString()).ToList();
                return null;
            }
        }
        public string Langue { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public bool Lundi { get; set; }
        public bool LundiMatin { get; set; }
        public bool LundiApresMidi { get; set; }

        public bool Mardi { get; set; }
        public bool MardiMatin { get; set; }
        public bool MardiApresMidi { get; set; }

        public bool Mercredi { get; set; }
        public bool MercrediMatin { get; set; }
        public bool MercrediApresMidi { get; set; }

        public bool Jeudi { get; set; }
        public bool JeudiMatin { get; set; }
        public bool JeudiApresMidi { get; set; }

        public bool Vendredi { get; set; }
        public bool VendrediMatin { get; set; }
        public bool VendrediApresMidi { get; set; }

        public bool Samedi { get; set; }
        public bool SamediMatin { get; set; }
        public bool SamediApresMidi { get; set; }

        public bool Dimanche { get; set; }
        public bool DimancheMatin { get; set; }
        public bool DimancheApresMidi { get; set; }

        [JsonIgnore()]
        public string FilePath
        {
            get
            {
                string directoryPath = Path.Combine(ConfigFile.Conf.ApplicationDirectory, "Modules", nameof(WorkingDaysModule));
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                return Path.Combine(directoryPath, "Preferences.json");
            }
        }

        public void Enregistrer()
        {
            JsonConvert.SerializeObject(this).SaveAs(FilePath);
        }

        public void Charger()
        {
            if (File.Exists(FilePath))
                using (TextReader reader = new StreamReader(FilePath))
                {
                    var pref = JsonConvert.DeserializeObject<Preferences>(reader.ReadToEnd());
                    Code = pref.Code;
                    State = pref.State;
                    Region = pref.Region;
                    Langue = pref.Langue;
                    StartDate = pref.StartDate;
                    EndDate = pref.EndDate;
                    Lundi = pref.Lundi;
                    LundiMatin = pref.LundiMatin;
                    LundiApresMidi = pref.LundiApresMidi;
                    Mardi = pref.Mardi;
                    MardiMatin = pref.MardiMatin;
                    MardiApresMidi = pref.MardiApresMidi;
                    Mercredi = pref.Mercredi;
                    MercrediMatin = pref.MercrediMatin;
                    MercrediApresMidi = pref.MercrediApresMidi;
                    Jeudi = pref.Jeudi;
                    JeudiMatin = pref.JeudiMatin;
                    JeudiApresMidi = pref.JeudiApresMidi;
                    Vendredi = pref.Vendredi;
                    VendrediMatin = pref.VendrediMatin;
                    VendrediApresMidi = pref.VendrediApresMidi;
                    Samedi = pref.Samedi;
                    SamediMatin = pref.SamediMatin;
                    SamediApresMidi = pref.SamediApresMidi;
                    Dimanche = pref.Dimanche;
                    DimancheMatin = pref.DimancheMatin;
                    DimancheApresMidi = pref.DimancheApresMidi;
                }
            else
            {
                StartDate = new DateTime(DateTime.Today.Year, 01, 01);
                EndDate = new DateTime(DateTime.Today.Year + 1, 01, 01);
            }
        }
    }
}
