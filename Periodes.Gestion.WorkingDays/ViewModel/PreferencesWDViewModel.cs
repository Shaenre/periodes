﻿using Periodes.Preferences;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Gestion.WorkingDays.ViewModel
{
    public class PreferencesWDViewModel
    {
        public Preferences Preferences { get; set; }
        public PreferencesWDViewModel(IUnityContainer container)
        {
            Preferences = container.Resolve<IPreferences>($"{WorkingDaysModule.Name}_{nameof(Preferences)}") as Preferences;
        }
    }
}
