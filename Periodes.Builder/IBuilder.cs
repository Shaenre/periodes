﻿using Periodes.BuilderConfig;
using Periodes.Data.ViewModel;
using Periodes.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder
{
    public interface IBuilder
    {
        IBuilder Setup(IBuilderConfig config, IPolicy policy);
        IBuilder Configure(VMTypePeriode typePeriode, DateTime startDate, DateTime endDate);
        void Build();
    }
}
