﻿using Periodes.BuilderConfig;
using Periodes.Data.ViewModel;
using Periodes.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Builder
{
    public abstract class Builder : IBuilder
    {
        protected VMTypePeriode TypePeriode { get; set; }
        protected DateTime StartDate { get; set; }
        protected DateTime EndDate { get; set; }
        protected IBuilderConfig Config { get; set; }
        protected IPolicy Policy { get; set; }
        public IBuilder Configure(VMTypePeriode typePeriode, DateTime startDate, DateTime endDate)
        {
            TypePeriode = typePeriode;
            StartDate = startDate;
            EndDate = endDate;
            return this;
        }
        public abstract void Build();

        public IBuilder Setup(IBuilderConfig config, IPolicy policy)
        {
            Config = config;
            Policy = policy;
            return this;
        }
    }    
}
