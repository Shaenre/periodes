﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Periodes.Authorisation.Data;

namespace Periodes.Authorisation
{
    public interface IAuthorisationViewModel
    {
        ICommand AuthorisationCommand { get; set; }
        ICommand AuthorisationInitializeCommand { get; set; }
        bool IsAutomatic { get; set; }
        string AuthorisationKey { get; set; }
        event EventHandler<UtilisateurProfil> AuthorisationEnded;
        event EventHandler<string> AuthorisationFailed;
    }
}
