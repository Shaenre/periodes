﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Periodes.Authorisation.Data;

namespace Periodes.Authorisation
{
    public abstract class AuthorisationViewModelBase : IAuthorisationViewModel
    {
        public ICommand AuthorisationCommand { get; set; }
        public ICommand AuthorisationInitializeCommand { get; set; }
        public string AuthorisationKey { get; set; }
        public abstract bool IsAutomatic { get; set; }

        public event EventHandler<UtilisateurProfil> AuthorisationEnded;
        protected void OnAuthorisationEnded(UtilisateurProfil utilisateur) => AuthorisationEnded?.Invoke(this, utilisateur);

        public event EventHandler<string> AuthorisationFailed;
        protected void OnAuthorisationFailed(string message) => AuthorisationFailed?.Invoke(this, message);
    }
}
