﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Data.ViewModel;

namespace Periodes.Authorisation.Data
{
    public class UtilisateurProfil
    {
        public VMUtilisateur Utilisateur { get; set; }
        public List<string> ModulesNames { get; set; } = new List<string>();
    }
}
