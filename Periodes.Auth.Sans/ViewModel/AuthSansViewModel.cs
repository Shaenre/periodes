﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;

namespace Periodes.Auth.Sans.ViewModel
{
    public class AuthSansViewModel : AuthentificationViewModelBase
    {
        public override bool IsAutomatic { get; set; } = true;
        public AuthSansViewModel()
        {
            AuthentificationInitializeCommand = new DelegateCommand(() => OnAuthentificationEnded(Environment.UserName));
        }
    }
}
