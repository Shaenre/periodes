﻿using Microsoft.Practices.Unity;
using Prism.Unity;
using ID3iShared.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Auth.Sans.ViewModel;
using Periodes.Auth.Sans.View;

namespace Periodes.Auth.Sans
{
    public class AuthSansModule : Module, IAuthentificationModule
    {
        public AuthSansModule() { }
        public override void Initialize()
        {
            Container.RegisterInstance<IAuthentificationViewModel>(Container.Resolve<AuthSansViewModel>(), new ContainerControlledLifetimeManager());

            Container.RegisterTypeForNavigation<AuthSansView>(Names.ViewName);
        }
    }
}
