﻿using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Periodes.Splash
{
    public class NavigateEvent : PubSubEvent<string>
    {
        public string ViewName { get; set; }
    }
    public class SplashController
    {
        [Dependency]
        public IRegionManager RegionManager { get; set; }
        [Dependency]
        public IUnityContainer Container { get; set; }
        [Dependency]
        public IModuleCatalog ModuleCatalog { get; set; }
        [Dependency]
        public IEventAggregator EventAggregator { get; set; }
        public SplashController() { }
        public SplashController Initialize()
        {
            EventAggregator.GetEvent<NavigateEvent>().Subscribe(navigatePath =>
            {
                RegionManager.RequestNavigate("SplashRegion", navigatePath);
            }, ThreadOption.UIThread, true);
            return this;
        }
    }
}
