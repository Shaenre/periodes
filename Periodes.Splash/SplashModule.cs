﻿using ID3iShared;
using ID3iShared.Module;
using Periodes.Splash.View;
using Periodes.Splash.ViewModel;
using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Modularity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Periodes.Splash
{
    public class ShowMainWindow : PubSubEvent<string> { }
    public class SplashModule : Module
    {
        [Dependency]
        public IModuleCatalog ModuleCatalog { get; set; }
        [Dependency]
        public IModuleManager ModuleManager { get; set; }
        private ManualResetEvent WaitForCreation { get; set; }

        public void LetsDoThis()
        {
            Container.Resolve<IEventAggregator>().GetEvent<ShowMainWindow>().Subscribe(x =>
            {
                System.Windows.Application.Current.Dispatcher.Invoke(Container.Resolve<IShell>().Show);
            }, ThreadOption.PublisherThread);
            Container.Resolve<SplashViewModel>().StartCommand.Execute();
        }

        public override void Initialize()
        {
            WaitForCreation = new ManualResetEvent(false);

            ThreadStart showSplash =
                () =>
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(
                        (Action)(() =>
                        {
                            Container.RegisterType<SplashViewModel>(new ContainerControlledLifetimeManager());
                            Container.RegisterType<SplashView>(new ContainerControlledLifetimeManager());
                            Container.RegisterInstance(Container.Resolve<SplashController>().Initialize(), new ContainerControlledLifetimeManager());
                            Container.RegisterTypeForNavigation<SplashMessageView>(nameof(SplashMessageView));
                            Container.RegisterTypeForNavigation<SplashErreurView>(nameof(SplashErreurView));
                            var splash = Container.Resolve<SplashView>();

                            Prism.Regions.RegionManager.SetRegionManager(splash, RegionManager);
                            Prism.Regions.RegionManager.UpdateRegions();

                            Container.Resolve<SplashViewModel>().SplashEnded += (sender, e) =>
                            {
                                splash.Dispatcher.BeginInvoke((Action)splash.Close);
                            };
                            splash.Show();
                            WaitForCreation.Set();
                        }));
                    Dispatcher.Run();
                };

            var thread = new Thread(showSplash) { Name = "Splash Thread", IsBackground = true };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            WaitForCreation.WaitOne();

            LetsDoThis();
        }
    }
}
