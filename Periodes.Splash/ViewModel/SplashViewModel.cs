﻿using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Unity;
using Prism.Regions;
using Microsoft.Practices.Unity;
using ID3iShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Periodes.Auth;
using Periodes.Authorisation;
using Periodes.Authorisation.Data;
using Periodes.Splash.View;
using System.Threading;

namespace Periodes.Splash.ViewModel
{
    public class SplashViewModel : NotifyPropertyChangedBase
    {
        public DelegateCommand StartCommand { get; set; }
        public DelegateCommand PreviousCommand { get; set; }
        public DelegateCommand CloseCommand { get; set; }
        public DelegateCommand QuitCommand { get; set; }
        [Dependency]
        public SplashController Controller { get; set; }
        [Dependency]
        public IRegionManager RegionManager { get; set; }
        [Dependency]
        public IEventAggregator EventAggregator { get; set; }

        private bool close = false;
        public bool Close { get => close; set => SetProperty(ref close, value); }
        public event EventHandler<bool> SplashEnded;

        private string message;
        public string Message { get => message; set => SetProperty(ref message, value); }

        private string state;
        public string State { get => state; set => SetProperty(ref state, value); }

        public SplashViewModel()
        {
            StartCommand = new DelegateCommand(StartExecute);
            PreviousCommand = new DelegateCommand(() =>
            {
                if (RegionManager.Regions["SplashRegion"].NavigationService.Journal.CanGoBack)
                    RegionManager.Regions["SplashRegion"].NavigationService.Journal.GoBack();
            });
            CloseCommand = new DelegateCommand(() => Close = true);
            QuitCommand = new DelegateCommand(() => Environment.Exit(-1));
        }
        private void StartExecute()
        {
            IAuthentificationViewModel auth = Controller.Container.TryResolve<IAuthentificationViewModel>();
            if (auth != null)
            {
                State = "Authentication phase ...";
                auth.AuthentificationEnded += SplashViewModel_AuthentificationEnded;
                auth.AuthentificationFailed += (sender, e) =>
                {
                    Message = e;
                    if ((sender as IAuthentificationViewModel).IsAutomatic)
                        EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashErreurView));
                    else
                        EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashMessageView));
                };
                EventAggregator.GetEvent<NavigateEvent>().Publish(Auth.Names.ViewName);
                auth.AuthentificationInitializeCommand?.Execute(null);
            }
            else SplashViewModel_AuthentificationEnded(this, null);
        }
        private void SplashViewModel_AuthentificationEnded(object sender, string e)
        {
            IAuthorisationViewModel cnx = Controller.Container.TryResolve<IAuthorisationViewModel>();
            if (cnx != null && e != null)
            {
                State = "Authorisation phase ...";
                cnx.AuthorisationKey = e;
                cnx.AuthorisationEnded += SplashViewModel_AuthorisationEnded;
                cnx.AuthorisationFailed += (x, y) =>
                {
                    Message = y;
                    if ((x as IAuthorisationViewModel).IsAutomatic)
                        EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashErreurView));
                    else
                        EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashMessageView));
                };
                EventAggregator.GetEvent<NavigateEvent>().Publish(Authorisation.Names.ViewName);
                cnx.AuthorisationInitializeCommand?.Execute(null);
            }
            else
            {
                Message = cnx == null ? "No connection module was found" : "Authentication refused";
                EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashMessageView));
            }
        }
        private void SplashViewModel_AuthorisationEnded(object sender, UtilisateurProfil e)
        {
            if (e == null)
            {
                Message = "Your profile wasn't found.";
                if ((sender as IAuthorisationViewModel).IsAutomatic)
                    EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashErreurView));
                else
                    EventAggregator.GetEvent<NavigateEvent>().Publish(nameof(SplashMessageView));
            }
            else
            {
                if (e.ModulesNames.FirstOrDefault() == "*")
                    Controller.ModuleCatalog.Modules.Where(x => x.State == ModuleState.NotStarted).ToList().ForEach(x => LoadModule(x));
                else
                    Controller.ModuleCatalog.Modules.Where(x => e.ModulesNames.Contains(x.ModuleName) && x.State == ModuleState.NotStarted).ToList().ForEach(x => Controller.Container.Resolve<IModuleManager>()?.LoadModule(x.ModuleName));
                State = "Loading finished.";
                SplashEnded?.Invoke(this, true);
                EventAggregator.GetEvent<ShowMainWindow>().Publish(null);
            }
        }
        private void LoadModule(ModuleInfo moduleInfo)
        {
            State = $"{moduleInfo.ModuleName}'s loading ...";
            Controller.Container.Resolve<IModuleManager>()?.LoadModule(moduleInfo.ModuleName);
        }
    }
}
